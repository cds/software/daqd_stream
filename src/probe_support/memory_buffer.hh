//
// Created by jonathan.hanks on 9/6/22.
//

#ifndef DAQD_STREAM_PROBE_MEMORY_BUFFER_HH
#define DAQD_STREAM_PROBE_MEMORY_BUFFER_HH

#include <memory>
#include <string>

#include "posix_mmap.hh"
#include "posix_shmem.hh"

class Memory_buffer
{
    struct Interface
    {
        virtual ~Interface( ) = default;
        virtual const void* get( ) const noexcept = 0;
        virtual void*       get( ) noexcept = 0;
    };

    template < typename B >
    struct Model : Interface
    {
        explicit Model( B&& b ) : b_{ std::move( b ) }
        {
        }
        ~Model( ) override = default;

        void*
        get( ) noexcept override
        {
            return b_.get( );
        }

        const void*
        get( ) const noexcept override
        {
            return b_.get( );
        }
        B b_;
    };

public:
    explicit Memory_buffer( const std::string& path ) : buffer_{ nullptr }
    {
        static const std::string mmap_str( "mmap://" );
        if ( path.find( mmap_str ) == 0 )
        {
            auto mmap_path = path.substr( mmap_str.size( ) );
            buffer_ =
                std::make_shared< Model< daqd_stream::detail::Posix_mmap > >(
                    daqd_stream::detail::Posix_mmap( mmap_path ) );
        }
        else
        {
            buffer_ = std::make_shared<
                Model< daqd_stream::detail::Posix_shared_memory > >(
                daqd_stream::detail::Posix_shared_memory( path ) );
        }
    }

    void*
    get( ) noexcept
    {
        return buffer_->get( );
    }

    const void*
    get( ) const noexcept
    {
        return buffer_->get( );
    }

    template < typename T >
    T*
    get_as( ) noexcept
    {
        return reinterpret_cast< T* >( get( ) );
    }

    template < typename T >
    const T*
    get_as( ) const noexcept
    {
        return reinterpret_cast< const T* >( get( ) );
    }

private:
    std::shared_ptr< Interface > buffer_;
};

#endif // DAQD_STREAM_PROBE_MEMORY_BUFFER_HH
