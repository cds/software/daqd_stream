//
// Created by jonathan.hanks on 9/13/22.
//

#ifndef DAQD_STREAM_SIMPLE_RANGE_HH
#define DAQD_STREAM_SIMPLE_RANGE_HH

template < typename It >
class Simple_range
{
public:
    Simple_range( It a, It b ) : start_{ a }, end_{ b }
    {
    }
    Simple_range( const Simple_range& ) = default;

    It
    begin( )
    {
        return start_;
    }

    It
    end( )
    {
        return end_;
    }

private:
    It start_;
    It end_;
};

#endif // DAQD_STREAM_SIMPLE_RANGE_HH
