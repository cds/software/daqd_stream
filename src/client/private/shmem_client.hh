//
// Created by jonathan.hanks on 11/5/21.
//

#ifndef DAQD_STREAM_SHMEM_CLIENT_HH
#define DAQD_STREAM_SHMEM_CLIENT_HH

#include <daqd_stream/daqd_stream.hh>
#include "client_base.hh"
#include "internal.hh"
#include "endian_utility.hh"
#include "load_data.hh"

#include "concatenate.hh"
#include "../../common/include/shared_structures.hh"

#include <iostream>
#include <fstream>

namespace daqd_stream
{
    namespace detail
    {
        class shmem_client : public detail::client_base
        {
        public:
            explicit shmem_client( client_intl&& internal )
                : client_base( ), p_{ std::move( internal ) }
            {
            }

            ~shmem_client( ) override = default;

            data_plan
            plan_request( const PLAN_TYPE          plan_type,
                          const channel_name_list& channel_names,
                          std::size_t              seconds_in_buffer ) const override
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );

                return detail::layout_data_plan( plan_type,
                                                 channel_names,
                                                 *( header->get_config( ) ),
                                                 seconds_in_buffer );
            }
            bool
            update_plan( data_plan& plan ) const override
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );
                auto* cur_config = header->get_config( );
                return detail::relayout_data_plan( plan, *cur_config );
            }
            checksum_t
            config_checksum( ) const override
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );
                auto* cfg = header->get_config( );
                return cfg->checksums.master_config;
            }

            std::vector< online_channel >
            channels( ) const override
            {
                std::vector< online_channel > results;
                auto                          header =
                    p_.shared_->get_as< detail::shared_mem_header >( );
                auto* config = header->get_config( );
                if ( config )
                {
                    results.reserve( config->channels.size( ) );
                    std::copy( config->channels.begin( ),
                               config->channels.end( ),
                               std::back_inserter( results ) );
                }
                return results;
            }
            std::vector< std::size_t >
            size_list( const channel_name_list& names ) const override
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );

                auto& channels = header->get_config( )->channels;
                return detail::build_size_list( names, channels );
            }

            void
            get_16th_data_core( const data_plan&  plan,
                                const time_point& last_time_seen,
                                void*             buffer,
                                std::size_t       buffer_size,
                                data_status&      status_buf ) const override
            {
                if ( !buffer || buffer_size < plan.required_size( ) )
                {
                    throw std::runtime_error(
                        "Invalid buffer passed to get_16th_data" );
                }
                if ( plan.type( ) != PLAN_TYPE::PLAN_16TH )
                {
                    throw std::runtime_error(
                        "Invalid plan type passed to get_16th_data" );
                }
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );

                unsigned int cycle = 0;
                unsigned int gps = 0;

                detail::data_block* db = nullptr;
                {
                    waiting_state waiter{};
                    waiter.api( p_.signal_block( ) )
                        .wait_16th( pred_16th( header, last_time_seen ) );
                    auto index =
                        get_next_block_index_16th( header, last_time_seen );
                    db = &( header->blocks( index ) );
                    cycle = header->blocks( index ).cycle;
                    gps = header->blocks( index ).gps_second;
                }
                detail::buffer_offset_helper offsets(
                    *db,
                    detail::plan_internals::get_locations( plan ),
                    header->get_config( )->checksums.master_config );
                load_data_16th(
                    plan, source_number( ), offsets, buffer, status_buf );

                status_buf.gps = detail::time_point_from_cycle( gps, cycle );
                status_buf.cycle = static_cast< int >( cycle );
            }
            void
            get_16th_data( const data_plan&  plan,
                           const time_point& last_time_seen,
                           void*             buffer,
                           std::size_t       buffer_size,
                           data_status&      status_buf,
                           byte_order        output_byte_order ) const override
            {
                status_buf.reset( plan.channel_names( ).size( ) );
                get_16th_data_core(
                    plan, last_time_seen, buffer, buffer_size, status_buf );
                detail::endian_convert_in_place(
                    buffer, plan, 1, output_byte_order );
            }
            void
            get_sec_data_core( const data_plan& plan,
                               std::int64_t     start_second,
                               std::size_t      stride,
                               void*            buffer,
                               std::size_t      buffer_size,
                               sec_data_status& status_buf,
                               uint64_t*        gps_loaded ) const override
            {
                if ( !buffer || buffer_size < plan.required_size( ) )
                {
                    throw std::runtime_error(
                        "Invalid buffer passed to get_sec_data" );
                }
                if ( plan.type( ) != PLAN_TYPE::PLAN_SEC )
                {
                    throw std::runtime_error(
                        "Invalid plan type passed to get_sec_data" );
                }
                if ( start_second < 0 || stride > plan.seconds_in_buffer( ) )
                {
                    throw std::range_error(
                        "Invalid second offset supplied to get_sec_data" );
                }

                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );

                if ( start_second == 0 )
                {
                    auto& block = header->blocks(header->get_cur_block());
                    start_second = block.gps_second;
                }

                waiting_state waiter{};
                for ( std::int64_t sec_index = 0; sec_index < stride;
                      ++sec_index )
                {
                    auto cur_sec = start_second + sec_index;
                    waiter.api( p_.signal_block( ) )
                        .wait_sec( pred_sec( header, cur_sec ) );

                    auto index = get_start_block_index_sec( header, cur_sec );
                    auto actual_sec = header->blocks( index ).gps_second;

//                    if ( start_second == 0 )
//                    {
//                        cur_sec = start_second = actual_sec;
//                    }

                    if ( actual_sec == cur_sec )
                    {
                        client::copy_sec_data(
                            header,
                            reinterpret_cast< char* >( buffer ),
                            index,
                            sec_index,
                            plan,
                            source_number( ),
                            status_buf );
                    }
                    else
                    {
                        client::zero_fill_second(
                            reinterpret_cast< char* >( buffer ),
                            sec_index,
                            detail::plan_internals::get_locations( plan ),
                            status_buf );
                    }
                }
                if ( gps_loaded )
                {
                    *gps_loaded = start_second;
                }
            }

            void
            get_sec_data( const data_plan& plan,
                          std::int64_t     start_second,
                          std::size_t      stride,
                          void*            buffer,
                          std::size_t      buffer_size,
                          sec_data_status& status_buf,
                          byte_order       output_byte_order ) const override
            {
                std::uint64_t actual_sec = 0;
                status_buf.reset( plan.channel_names( ).size( ) );
                get_sec_data_core( plan,
                                   start_second,
                                   stride,
                                   buffer,
                                   buffer_size,
                                   status_buf,
                                   &actual_sec );

                detail::endian_convert_in_place(
                    buffer,
                    buffer_size,
                    detail::plan_internals::get_locations( plan ),
                    stride * detail::cycles_per_sec( ),
                    output_byte_order );

                for ( auto& status_entry : status_buf.channel_status )
                {
                    std::transform( status_entry.begin( ),
                                    status_entry.end( ),
                                    status_entry.begin( ),
                                    []( unsigned int val ) -> unsigned int {
                                        return ( val == 2 ? 0 : val );
                                    } );
                }
                status_buf.gps = make_time_point( actual_sec, 0 );
                status_buf.cycle = 0;
            }
            void
            dump_16th( const std::string& filename ) const override
            {
                std::unique_ptr< detail::data_block > dblock(
                    new detail::data_block );

                waiting_state waiter{};

                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );
                detail::data_block* db = nullptr;
                {
                    //            interprocess::scoped_lock<
                    //            interprocess::interprocess_mutex > l_(
                    //                header->signal_16th_mutex );
                    //            header->signal_16th_condition.wait( l_ );
                    waiter.api( p_.signal_block( ) )
                        .wait_16th(
                            pred_16th( header, make_time_point( 0, 0 ) ) );
                    db = &( header->blocks( header->cur_block ) );
                }
                *dblock = *db;
                auto& shared_chans = header->get_config( )->channels;
                std::vector< online_channel > chans( shared_chans.begin( ),
                                                     shared_chans.end( ) );

                std::ofstream out( filename, std::ios::binary );
                out.exceptions( std::ios_base::failbit );
                out.exceptions( std::ios_base::badbit );
                out.write( reinterpret_cast< char* >( dblock.get( ) ),
                           sizeof( *dblock ) );
                if ( !out )
                {
                    std::cout << "output is bad" << std::endl;
                }
                std::cout << "Wrote " << sizeof( *dblock ) << " bytes"
                          << std::endl;

                std::ofstream out_chans( filename + ".chans",
                                         std::ios::binary );
                out.write( reinterpret_cast< char* >( chans.data( ) ),
                           sizeof( online_channel ) * chans.size( ) );
            }

            /////////////////////////////////////
            // internal calls
            ///////

            void
            get_internal_config( ifo_checksums&                 checksums,
                                 shared_span< online_channel >& channels )
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );
                auto* config = header->get_config( );
                if ( config )
                {
                    checksums = config->checksums;
                    channels = config->channels;
                }
            }

            void
            avail_info( detail::avail_info& info ) override
            {
                auto header =
                    p_.shared_->get_as< detail::shared_mem_header >( );

                auto safe = header->safe_data_blocks( );
                auto newest_index = header->get_cur_block( );
                auto oldest_index = newest_index - safe;
                if ( oldest_index < 0 )
                {
                    oldest_index += header->max_data_blocks( );
                }
                const auto& newest = header->blocks( newest_index );
                const auto& oldest = header->blocks( oldest_index );

                info = detail::avail_info( newest.gps_second,
                                           newest.cycle,
                                           oldest.gps_second,
                                           oldest.cycle );
            };

        private:
            detail::client_intl p_;
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_SHMEM_CLIENT_HH
