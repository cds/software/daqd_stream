//
// Created by jonathan.hanks on 11/15/21.
//

#ifndef DAQD_STREAM_LOAD_DATA_HH
#define DAQD_STREAM_LOAD_DATA_HH

#include "client_internal.hh"
#include <daqd_stream/daqd_stream.hh>
#include "endian_utility.hh"

namespace daqd_stream
{
    namespace detail
    {

        void load_data_16th( const data_plan&                    plan,
                             unsigned char                       current_source,
                             const detail::buffer_offset_helper& offsets,
                             void*                               dest_buffer,
                             data_status&                        status );
    }
} // namespace daqd_stream

#endif // DAQD_STREAM_LOAD_DATA_HH
