//
// Created by jonathan.hanks on 11/8/21.
//

#ifndef DAQD_STREAM_NULL_CLIENT_HH
#define DAQD_STREAM_NULL_CLIENT_HH

#include "client_base.hh"

namespace daqd_stream
{
    namespace detail
    {
        class null_client : public detail::client_base
        {
        public:
            ~null_client( ) override = default;
            data_plan
            plan_request( const PLAN_TYPE          plan_type,
                          const channel_name_list& channel_names,
                          std::size_t              seconds_in_buffer ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            bool
            update_plan( data_plan& plan ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            checksum_t
            config_checksum( ) const override
            {
                throw std::runtime_error( "not implemented" );
            }

            std::vector< online_channel >
            channels( ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            std::vector< std::size_t >
            size_list( const channel_name_list& names ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            void
            get_16th_data( const data_plan&  plan,
                           const time_point& last_time_seen,
                           void*             buffer,
                           std::size_t       buffer_size,
                           data_status&      status_buf,
                           byte_order        output_byte_order ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            void
            get_sec_data( const data_plan& plan,
                          std::int64_t     start_second,
                          std::size_t      stride,
                          void*            buffer,
                          std::size_t      buffer_size,
                          sec_data_status& status_buf,
                          byte_order       output_byte_order ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            void
            dump_16th( const std::string& filename ) const override
            {
                throw std::runtime_error( "not implemented" );
            }

            void
            get_16th_data_core( const data_plan&  plan,
                                const time_point& last_time_seen,
                                void*             buffer,
                                std::size_t       buffer_size,
                                data_status&      status_buf ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            void
            get_sec_data_core( const data_plan& plan,
                               std::int64_t     start_second,
                               std::size_t      stride,
                               void*            buffer,
                               std::size_t      buffer_size,
                               sec_data_status& status_buf,
                               std::uint64_t*   gps_loaded ) const override
            {
                throw std::runtime_error( "not implemented" );
            }

            /////////////////////////////////////
            // internal calls
            ///////

            virtual void
            get_internal_config( ifo_checksums&                 checksums,
                                 shared_span< online_channel >& channels )
            {
                throw std::runtime_error( "not implemented" );
            }

            void
            avail_info( detail::avail_info& info ) override
            {
                throw std::runtime_error( "not implemented" );
            };
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_NULL_CLIENT_HH
