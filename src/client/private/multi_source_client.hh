//
// Created by jonathan.hanks on 11/5/21.
//

#ifndef DAQD_STREAM_MULTI_SOURCE_CLIENT_HH
#define DAQD_STREAM_MULTI_SOURCE_CLIENT_HH

#include <algorithm>
#include <iterator>
#include <type_traits>
#include <vector>

#include <daqd_stream/daqd_stream.hh>

#include "crc.h"
#include "predicates.hh"
#include "shmem_client.hh"
#include "multi_source_info_block.hh"

// error fixme
// FIXME:
//  x Constrain sources to be shmem_clients
//  x - Put in a method to shmem_clients to extract the shared_ifo_config
//    - or the subset needed for planning
//  x - channels should have source # + dcu id encoded
//  x  - then data lookups become a tuple of (source, dcu, offset, length)
//    - just use the top 8 bits of the dcu?
//  - how do we deal with status in a multi source world?
//    - not just maxdcus array, need something smarter

namespace daqd_stream
{
    namespace detail
    {
        inline avail_info
        narrow_avail_to_sec( avail_info avail )
        {
            auto start_gps = avail.start_gps( );
            auto end_gps = avail.end_gps( );

            if ( avail.start_cycle( ) != 0 )
            {
                ++start_gps;
            }
            if ( avail.end_cycle( ) != 15 )
            {
                --end_gps;
            }
            if ( start_gps > end_gps )
            {
                return avail_info( 0, 0, 0, 0 );
            }
            return avail_info( end_gps, 15, start_gps, 0 );
        }

        inline time_point
        narrow_initial_time_range( const std::vector< avail_info >& avail )
        {
            time_point result{ };
            if ( avail.empty( ) )
            {
                return result;
            }
            avail_info cur_span{ avail.front( ) };
            for ( const auto& cur_avail : avail )
            {
                if ( overlaps( cur_avail, cur_span ) )
                {
                    cur_span = intersection( cur_avail, cur_span );
                }
                else
                {
                    // what here?
                }
            }
            result.seconds = cur_span.end_gps( );
            result.nano = detail::cycle_to_nano( cur_span.end_cycle( ) );
            return result;
        }

        /**
         * What are the reqauirements on MultiSourceClientType
         *  we have several clients to choose from
         *   * null test clients
         *   * daqd_stream
         *   * file stream
         * @tparam MultiSourceClientType
         */
        template < typename MultiSourceClientType >
        struct multi_source_client_internal
        {
            multi_source_client_internal( MultiSourceClientType& client )
                : client_{ client }
            {
            }

            std::vector< std::unique_ptr<
                typename MultiSourceClientType::client_type > >&
            sources( )
            {
                return client_.sources_;
            }

            MultiSourceClientType& client_;
        };

        template < typename ClientType >
        class generic_multi_source_client : public detail::client_base
        {
            static_assert(
                std::is_base_of< detail::client_base, ClientType >::value,
                "ClientType must be derived from client_base" );

            static void
            number_sources(
                std::vector< std::unique_ptr< ClientType > >& sources )
            {
                int i = 0;
                for ( auto& cur : sources )
                {
                    cur->set_source_number( i );
                    i++;
                }
            }

            static std::vector< std::unique_ptr< ClientType > >
            injest_sources(
                std::vector< std::unique_ptr< ClientType > > sources )
            {
                number_sources( sources );
                return std::move( sources );
            }

            static std::vector< std::unique_ptr< ClientType > >
            injest_sources(
                const std::vector< std::string >&   source_names,
                const detail::signaling_data_block* signal_block = nullptr )
            {
                std::vector< std::unique_ptr< ClientType > > sources{ };
                std::transform(
                    source_names.begin( ),
                    source_names.end( ),
                    std::back_inserter( sources ),
                    [ signal_block ]( const std::string& source )
                        -> std::unique_ptr< shmem_client > {
                        return std::make_unique< shmem_client >(
                            detail::client_intl(
                                source,
                                const_cast< detail::signaling_data_block* >(
                                    signal_block ) ) );
                    } );
                number_sources( sources );
                return std::move( sources );
            }

        public:
            using client_type = ClientType;

            template < typename T >
            friend struct multi_source_client_internal;

            explicit generic_multi_source_client(
                std::vector< std::unique_ptr< ClientType > > sources )
                : client_base( ), multi_header_shmem_( nullptr, 0 ), sources_{
                      injest_sources( std::move( sources ) )
                  }
            {
            }
            explicit generic_multi_source_client(
                const std::vector< std::string >& sources )
                : client_base( ), multi_header_shmem_( nullptr, 0 ), sources_{
                      injest_sources( sources )
                  }
            {
            }
            generic_multi_source_client(
                detail::Posix_shared_memory&&                shmem,
                const volatile detail::signaling_data_block* signal_block,
                const std::vector< std::string >&            sources )
                : client_base( ), multi_header_shmem_{ std::move( shmem ) },
                  multi_signal_block_{ signal_block }, sources_{
                      injest_sources(
                          sources,
                          const_cast< detail::signaling_data_block* >(
                              signal_block ) )
                  }
            {
            }

            ~generic_multi_source_client( ) override = default;

            data_plan
            plan_request( const PLAN_TYPE          plan_type,
                          const channel_name_list& channel_names,
                          std::size_t              seconds_in_buffer ) const override
            {
                auto config = build_ifo_config( );
                return detail::layout_data_plan(
                    plan_type, channel_names, config, seconds_in_buffer );
            }

            bool
            update_plan( data_plan& plan ) const override
            {
                auto config = build_ifo_config( );
                return detail::relayout_data_plan( plan, config );
            }

            checksum_t
            config_checksum( ) const override
            {
                checksum_t cur_sum{ 0 };
                for ( const auto& source : sources_ )
                {
                    auto input_sum = source->config_checksum( );
                    cur_sum = crc_ptr( reinterpret_cast< char* >( &input_sum ),
                                       sizeof( input_sum ),
                                       cur_sum );
                }
                return crc_len( sources_.size( ) * sizeof( checksum_t ),
                                cur_sum );
            }

            std::vector< online_channel >
            channels( ) const override
            {
                std::vector< online_channel > results{ };
                for ( const auto& source : sources_ )
                {
                    auto cur_channels = source->channels( );
                    std::move( cur_channels.begin( ),
                               cur_channels.end( ),
                               std::back_inserter( results ) );
                }
                std::sort( results.begin( ), results.end( ), less_by_name );
                return results;
            }

            std::vector< std::size_t >
            size_list( const channel_name_list& names ) const override
            {
                return detail::build_size_list( names, channels( ) );
            }

            void
            get_16th_data( const data_plan&  plan,
                           const time_point& last_time_seen,
                           void*             buffer,
                           std::size_t       buffer_size,
                           data_status&      status_buf,
                           byte_order        output_byte_order ) const override
            {
                const_plan_internals intl{ plan };
                const auto&          segments = intl.segments( );
                if ( !intl.multi_source( ) )
                {
                    auto source_number =
                        intl.segments( ).front( ).identifier( ).source;
                    sources_[ source_number ]->get_16th_data(
                        plan,
                        last_time_seen,
                        buffer,
                        buffer_size,
                        status_buf,
                        output_byte_order );
                    return;
                }
                status_buf.reset( plan.channel_names( ).size( ) );
                buffer_and_plan_get_data_checks< PLAN_TYPE::PLAN_16TH >(
                    plan, buffer, buffer_size );
                std::vector< bool > used_sources =
                    determine_used_sources( segments );

                auto last = determine_start_timepoint_16th( last_time_seen,
                                                            used_sources );

                // now that we have a concrete time
                // for each source see if the time is available
                // if so get the data
                // if not skip it

                for ( auto i = 0; i < sources_.size( ); ++i )
                {
                    if ( used_sources[ i ] )
                    {
                        sources_[ i ]->get_16th_data( plan,
                                                      last,
                                                      buffer,
                                                      buffer_size,
                                                      status_buf,
                                                      byte_order::NATIVE );
                    }
                }

                // for remaining sources, if the time is too old, mark the data
                // old else wait until data is available

                detail::endian_convert_in_place(
                    buffer, plan, 1, output_byte_order );

                // what about timeout ?
                status_buf.data_good = true;
            }

            void
            get_sec_data( const data_plan& plan,
                          std::int64_t     start_second,
                          std::size_t      stride,
                          void*            buffer,
                          std::size_t      buffer_size,
                          sec_data_status& status_buf,
                          byte_order       output_byte_order ) const override
            {
                const_plan_internals intl{ plan };
                const auto&          segments = intl.segments( );
                if ( !intl.multi_source( ) )
                {
                    auto source_number =
                        intl.segments( ).front( ).identifier( ).source;
                    sources_[ source_number ]->get_sec_data(
                        plan,
                        start_second,
                        stride,
                        buffer,
                        buffer_size,
                        status_buf,
                        output_byte_order );
                    return;
                }
                status_buf.reset( plan.channel_names( ).size( ) );
                buffer_and_plan_get_data_checks< PLAN_TYPE::PLAN_SEC >(
                    plan, buffer, buffer_size );
                std::vector< bool > used_sources =
                    determine_used_sources( segments );

                auto requested_gps =
                    determine_start_timepoint_sec( start_second, used_sources );

                for ( auto i = 0; i < sources_.size( ); ++i )
                {
                    if ( used_sources[ i ] )
                    {
                        sources_[ i ]->get_sec_data_core( plan,
                                                          requested_gps,
                                                          stride,
                                                          buffer,
                                                          buffer_size,
                                                          status_buf,
                                                          nullptr );
                    }
                }

                detail::endian_convert_in_place(
                    buffer,
                    buffer_size,
                    detail::plan_internals::get_locations( plan ),
                    stride * detail::cycles_per_sec( ),
                    output_byte_order );

                status_buf.gps = make_time_point( requested_gps, 0 );
                status_buf.cycle = 0;
                // what about timeout ?
            }

            void
            dump_16th( const std::string& filename ) const override
            {
                throw std::runtime_error(
                    "not implemented yet for multi source clients" );
            }
            void
            get_16th_data_core( const data_plan&  plan,
                                const time_point& last_time_seen,
                                void*             buffer,
                                std::size_t       buffer_size,
                                data_status&      status_buf ) const override
            {
                throw std::runtime_error( "not implemented" );
            }
            void
            get_sec_data_core( const data_plan& plan,
                               std::int64_t     start_second,
                               std::size_t      stride,
                               void*            buffer,
                               std::size_t      buffer_size,
                               sec_data_status& status_buf,
                               std::uint64_t*   gps_loaded ) const override
            {
                throw std::runtime_error( "not implemented" );
            }

        protected:
            template < PLAN_TYPE SPECIFIED_PLAN_TYPE >
            void
            buffer_and_plan_get_data_checks( const data_plan& plan,
                                             void*            buffer,
                                             std::size_t      buffer_size ) const
            {
                if ( !buffer || buffer_size < plan.required_size( ) )
                {
                    throw std::runtime_error(
                        "Invalid buffer passed (null or wrong size)" );
                }
                if ( plan.type( ) != SPECIFIED_PLAN_TYPE )
                {
                    throw std::runtime_error(
                        "Invalid plan type passed to get_sec_data" );
                }
            }

            std::vector< bool >
            determine_used_sources( const std::vector< dcu_span >& segments ) const
            {
                std::vector< bool > used_sources( sources_.size( ), false );
                auto                used_sources_count = 0;
                for ( auto i = 0; i < sources_.size( ); ++i )
                {
                    auto it = std::find_if(
                        segments.begin( ),
                        segments.end( ),
                        [ i ]( const dcu_span& cur_span ) -> bool {
                            return cur_span.source == i;
                        } );
                    auto used = ( it != segments.end( ) );
                    if ( used )
                    {
                        used_sources[ i ] = true;
                        ++used_sources_count;
                    }
                }
                return used_sources;
            }

            time_point
            determine_start_timepoint_16th(
                time_point                 last_time_seen,
                const std::vector< bool >& used_sources ) const
            {
                time_point last = last_time_seen;
                if ( last_time_seen.seconds == 0 && last_time_seen.nano == 0 )
                {
                    std::vector< detail::avail_info > availability{ };
                    for ( auto i = 0; i < sources_.size( ); ++i )
                    {
                        if ( used_sources[ i ] )
                        {
                            detail::avail_info info{ };
                            sources_[ i ]->avail_info( info );
                            availability.emplace_back( info );
                        }
                    }
                    last = detail::narrow_initial_time_range( availability );
                    if ( last.nano == 0 )
                    {
                        last.seconds--;
                        last.nano = 1000000000 - ( 1000000000 / 16 );
                    }
                    else
                    {
                        last.nano -= ( 1000000000 / 16 );
                    }
                }
                return last;
            }

            std::int64_t
            determine_start_timepoint_sec(
                std::int64_t               requested_gps,
                const std::vector< bool >& used_sources ) const
            {
                std::int64_t last = requested_gps;
                if ( requested_gps == 0 )
                {
                    std::vector< detail::avail_info > availability{ };
                    for ( auto i = 0; i < sources_.size( ); ++i )
                    {
                        if ( used_sources[ i ] )
                        {
                            detail::avail_info info{ };
                            sources_[ i ]->avail_info( info );
                            availability.emplace_back(
                                narrow_avail_to_sec( info ) );
                        }
                    }
                    last = static_cast< std::int64_t >(
                        detail::narrow_initial_time_range( availability )
                            .seconds );
                }
                return last;
            }

            ifo_config_base< multi_channel_list, sparse_ifo_checksums >
            build_ifo_config( ) const
            {
                ifo_config_base< multi_channel_list, sparse_ifo_checksums >
                              config;
                unsigned char source_num = 0;
                for ( const auto& source : sources_ )
                {
                    ifo_checksums                 source_config;
                    shared_span< online_channel > source_channels{ };
                    source->get_internal_config( source_config,
                                                 source_channels );
                    config.channels.add_source( source_channels );

                    for ( auto i = 0; i < max_dcu( ); ++i )
                    {
                        if ( source_config.dcu_config[ i ] != 0 )
                        {
                            config.checksums.dcu_config.insert( std::make_pair(
                                dcu_identifier( source_num, i ),
                                source_config.dcu_config[ i ] ) );
                        }
                    }
                    ++source_num;
                }
                config.checksums.recalculate_master_config( );
                //                std::sort( config.channels.begin( ),
                //                           config.channels.end( ),
                //                           detail::less_by_name );
                return config;
            }

            void
            avail_info( detail::avail_info& info ) override
            {
                throw std::runtime_error( "not implemented" );
            }

        private:
            Posix_shared_memory                          multi_header_shmem_;
            const volatile detail::signaling_data_block* multi_signal_block_{
                nullptr
            };
            std::vector< std::unique_ptr< ClientType > > sources_;
        };
        using multi_source_client = generic_multi_source_client< shmem_client >;
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_MULTI_SOURCE_CLIENT_HH
