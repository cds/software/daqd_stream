/*
Copyright 2022 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#ifndef DAQD_STREAM_TIME_POINT_HH
#define DAQD_STREAM_TIME_POINT_HH

#include <cstdint>

namespace daqd_stream
{
    /*!
     * @brief A point in time tracked as seconds and nanoseconds.
     * @noop tag time_point
     */
    struct time_point
    {
        uint64_t seconds{ 0 };
        uint64_t nano{ 0 };

        constexpr time_point() noexcept: seconds{0}, nano{0} {};
        constexpr time_point(uint64_t sec, uint64_t ns) noexcept: seconds{sec}, nano{ns} {};
        constexpr time_point(const time_point& other) noexcept: seconds{other.seconds}, nano{other.nano} {};

        time_point& operator=(const time_point& other) noexcept
        {
            seconds = other.seconds;
            nano = other.nano;
            return *this;
        }

        bool
        operator==( const time_point& other ) const noexcept
        {
            return seconds == other.seconds && nano == other.nano;
        }
        bool
        operator!=( const time_point& other ) const noexcept
        {
            return !( *this == other );
        }
        bool
        operator<( const time_point& other ) const noexcept
        {
            if ( seconds < other.seconds )
            {
                return true;
            }
            if ( seconds > other.seconds )
            {
                return false;
            }
            return nano < other.nano;
        }
        bool
        operator<=( const time_point& other ) const noexcept
        {
            if ( seconds < other.seconds )
            {
                return true;
            }
            if ( seconds > other.seconds )
            {
                return false;
            }
            return nano <= other.nano;
        }
        bool
        operator>( const time_point& other ) const noexcept
        {
            if ( seconds > other.seconds )
            {
                return true;
            }
            if ( seconds < other.seconds )
            {
                return false;
            }
            return nano > other.nano;
        }
        bool
        operator>=( const time_point& other ) const noexcept
        {
            if ( seconds > other.seconds )
            {
                return true;
            }
            if ( seconds < other.seconds )
            {
                return false;
            }
            return nano >= other.nano;
        }
    };

    /*!
     * @brief create a time point
     */
    constexpr time_point
    make_time_point( uint64_t seconds, uint64_t nano )
    {
        return time_point{ seconds, nano };
    }
} // namespace daqd_stream

#endif // DAQD_STREAM_TIME_POINT_HH
