/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_FIXED_STRING_HH
#define DAQD_STREAM_FIXED_STRING_HH

#include <cstdlib>
#include <cstring>

namespace daqd_stream
{
    namespace detail
    {
        inline void
        fixed_strncpy( char* dest, const char* src, std::size_t n )
        {
            strncpy( dest, src, n );
            dest[ n - 1 ] = '\0';
        }
    } // namespace detail

    template < std::size_t MAX_SIZE >
    class fixed_string
    {
    public:
        using size_type = std::size_t;

        fixed_string( ) : data_{ }
        {
        }

        fixed_string( const char* s ) : data_{ }
        {
            detail::fixed_strncpy( data_, s, MAX_SIZE );
        }
        fixed_string( const fixed_string& other ) = default;
        template < std::size_t OTHER_MAX >
        fixed_string( const fixed_string< OTHER_MAX >& other )
        {
            detail::fixed_strncpy( data_, other.data( ), MAX_SIZE );
        }

        fixed_string&
        operator=( const char* s )
        {
            detail::fixed_strncpy( data_, s, MAX_SIZE );
            return *this;
        }
        fixed_string& operator=( const fixed_string& other ) = default;
        template < std::size_t OTHER_MAX >
        fixed_string&
        operator=( const fixed_string< OTHER_MAX >& other )
        {
            detail::fixed_strncpy( data_, other.data( ), MAX_SIZE );
            return *this;
        }

        size_type
        size( ) const
        {
            return static_cast< size_type >( std::strlen( data_ ) );
        }
        constexpr size_type
        capacity( ) const
        {
            return MAX_SIZE;
        }

        constexpr size_type
        capacity( ) const volatile
        {
            return MAX_SIZE;
        }

        bool
        operator==( const char* s ) const
        {
            return strcmp( data( ), s ) == 0;
        }
        template < std::size_t OTHER_MAX >
        bool
        operator==( const fixed_string< OTHER_MAX >& other )
        {
            return *this == other.data( );
        }
        bool
        operator!=( const char* s ) const
        {
            return strcmp( data( ), s ) != 0;
        }
        template < std::size_t OTHER_MAX >
        bool
        operator!=( const fixed_string< OTHER_MAX >& other )
        {
            return *this != other.data( );
        }

        bool
        operator<( const char* s ) const
        {
            return strcmp( data( ), s ) < 0;
        }

        template < std::size_t OTHER_MAX >
        bool
        operator<( const fixed_string< OTHER_MAX >& other ) const
        {
            return *this < other.data( );
        }

        bool
        operator>( const char* s ) const
        {
            return strcmp( data( ), s ) > 0;
        }

        template < std::size_t OTHER_MAX >
        bool
        operator>( const fixed_string< OTHER_MAX >& other ) const
        {
            return *this > other.data( );
        }

        bool
        empty( ) const
        {
            return data_[ 0 ] == '\0';
        }

        char*
        data( )
        {
            return data_;
        }

        const char*
        data( ) const
        {
            return data_;
        }

        volatile const char*
        data( ) const volatile
        {
            return data_;
        }

        template < std::size_t N >
        void
        copy_to( char ( &dest )[ N ] ) const noexcept
        {
            static_assert( N >= MAX_SIZE, "destination too small for copy_to" );
            std::strcpy( dest, data_ );
        }

    private:
        char data_[ MAX_SIZE ];
    };
} // namespace daqd_stream

#endif // DAQD_STREAM_FIXED_STRING_HH
