/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_HH
#define DAQD_STREAM_HH

#include <array>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <typeindex>
#include <type_traits>
#include <vector>
#include <ostream>

#include <daqd_stream/fixed_string.hh>
#include <daqd_stream/basic_types.hh>
#include <daqd_stream/dcu_identifier.hh>
#include <daqd_stream/online_channel.hh>
#include <daqd_stream/time_point.hh>

namespace daqd_stream
{
    namespace detail
    {
        struct buffer_offset_helper;
        template < typename ChanList, typename ChecksumType >
        struct ifo_config_base;
        struct plan_internals;
        struct const_plan_internals;
        struct shared_mem_header;

        class avail_info;
    } // namespace detail

    /*!
     * @brief A subset of online channel used to help get output buffer offsets
     * for a plan.
     * @details This is enough information to help extract the data, type, rate,
     * ...
     * @noop tag output_location
     */
    struct output_location
    {
        output_location( ) = default;
        output_location( const online_channel& channel,
                         std::size_t           output_offset )
            : datatype{ channel.datatype }, datarate{ channel.datarate },
              bytes_per_16th{ channel.bytes_per_16th },
              data_offset{ output_offset }, signal_gain{ channel.signal_gain },
              signal_slope{ channel.signal_slope },
              signal_offset{ channel.signal_offset }, units{ channel.units }
        {
        }
        DATA_TYPE          datatype{ DATA_TYPE::UNDEFINED };
        int                datarate{ 0 };
        int                bytes_per_16th{ 0 };
        std::size_t        data_offset{ 0 };
        float              signal_gain{ 1.0 };
        float              signal_slope{ 1.0 };
        float              signal_offset{ 0.0 };
        fixed_string< 40 > units{ "counts" };
    };
    static_assert( std::is_same< decltype( online_channel::datatype ),
                                 decltype( output_location::datatype ) >::value,
                   "online_channel and output_metadata must have the same type "
                   "for their datatype fields" );
    static_assert( std::is_same< decltype( online_channel::datarate ),
                                 decltype( output_location::datarate ) >::value,
                   "online_channel and output_metadata must have the same type "
                   "for their datarate fields" );
    static_assert(
        std::is_same< decltype( online_channel::bytes_per_16th ),
                      decltype( output_location::bytes_per_16th ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "bytes_per_16th fields" );
    static_assert(
        std::is_same< decltype( online_channel::data_offset ),
                      decltype( output_location::data_offset ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "data_offset fields" );
    static_assert(
        std::is_same< decltype( online_channel::signal_slope ),
                      decltype( output_location::signal_slope ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "signal_slope fields" );
    static_assert(
        std::is_same< decltype( online_channel::signal_gain ),
                      decltype( output_location::signal_gain ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "signal_gain fields" );
    static_assert(
        std::is_same< decltype( online_channel::signal_offset ),
                      decltype( output_location::signal_offset ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "signal_offset fields" );
    static_assert(
        std::is_same< decltype( online_channel::units ),
                      decltype( output_location::units ) >::value,
        "online_channel and output_metadata must have the same type for their "
        "units fields" );
    static_assert( sizeof( online_channel ) > sizeof( output_location ),
                   "output_metadata is a subset of online_channel so it should "
                   "be strictly smaller" );

    /*!
     * @brief Name + output location in a buffer
     * @noop tag output_metadata
     */
    struct output_metadata
    {
        output_metadata( ) = default;
        output_metadata( std::string name, const output_location& location )
            : name{ std::move( name ) }, location{ location }
        {
        }
        std::string     name{ };
        output_location location{ };
    };

    /*!
     * @brief a list of channel names
     */
    using channel_name_list = std::vector< std::string >;
    /*!
     * @brief a list of channels
     */
    using channel_list = std::vector< online_channel >;

    struct dcu_span
    {
        dcu_span( ) : source{ 0 }, dcu{ 0 }, offset{ 0 }, length{ 0 }
        {
        }
        dcu_span( unsigned short Source,
                  unsigned short Dcu,
                  std::size_t    Offset,
                  std::size_t    Length )
            : source{ Source }, dcu{ Dcu }, offset{ Offset }, length{ Length }
        {
        }

        dcu_span( const dcu_span& other ) = default;
        dcu_span& operator=( const dcu_span& other ) = default;

        bool
        operator==( const dcu_span& other ) const
        {
            return dcu == other.dcu && offset == other.offset &&
                length == other.length;
        }

        bool
        can_extend_by( const dcu_span& other ) const
        {
            return ( source == other.source && dcu == other.dcu &&
                     other.offset == ( offset + length ) );
        }

        void
        extend( const std::size_t amount )
        {
            length += amount;
        }

        dcu_identifier
        identifier( ) const noexcept
        {
            return dcu_identifier( source, dcu );
        }

        unsigned short source;
        unsigned short dcu;
        std::size_t    offset;
        std::size_t    length;
    };

    /*!
     * @brief Enumeration of plan types
     * @noop tag PLAN_TYPE
     */
    enum class PLAN_TYPE
    {
        PLAN_16TH,
        PLAN_SEC,
    };

    struct data_status
    {
        data_status( )
            : gps{ make_time_point( 0, 0 ) }, cycle( 0 ),
              plan_status( PLAN_STATUS::OK ),
              data_good( true ), channel_status{ }
        {
        }
        explicit data_status( std::size_t channel_count )
            : gps{ make_time_point( 0, 0 ) }, cycle( 0 ),
              plan_status( PLAN_STATUS::OK ), data_good( true ),
              channel_status( channel_count, 0 )
        {
        }

        time_point                  gps;
        int                         cycle{ 0 };
        PLAN_STATUS                 plan_status;
        bool                        data_good{ true };
        std::vector< unsigned int > channel_status;

        void
        reset( std::size_t channel_count )
        {
            gps = make_time_point( 0, 0 );
            cycle = 0;
            plan_status = PLAN_STATUS::OK;
            data_good = true;
            channel_status.resize( channel_count );
            std::fill( channel_status.begin( ), channel_status.end( ), 0 );
        }
    };

    struct sec_data_status
    {
        sec_data_status( )
            : gps{ make_time_point( 0, 0 ) }, cycle( 0 ),
              plan_status( PLAN_STATUS::OK ), channel_status{ }
        {
        }
        explicit sec_data_status( std::size_t channel_count )
            : gps{ make_time_point( 0, 0 ) }, cycle( 0 ),
              plan_status( PLAN_STATUS::OK ), channel_status( channel_count )
        {
            for ( auto& cur : channel_status )
            {
                cur.fill( 0 );
            }
        }

        time_point                                    gps;
        int                                           cycle{ 0 };
        PLAN_STATUS                                   plan_status;
        std::vector< std::array< unsigned int, 16 > > channel_status;

        void
        reset( std::size_t channel_count )
        {
            gps = make_time_point( 0, 0 );
            cycle = 0;
            plan_status = PLAN_STATUS::OK;
            channel_status.resize( channel_count );
            for ( auto& cur : channel_status )
            {
                cur.fill( 0 );
            }
        }
    };

    /*!
     * @brief A plan or map of the gather operation to use when reading data
     * from the live data stream.
     * @noop tag data_plan
     */
    class data_plan
    {
        friend struct detail::plan_internals;
        friend struct detail::const_plan_internals;

    public:
        struct dcu_checksum
        {
            dcu_identifier identifier;
            checksum_t     config;
        };

    private:
        static std::size_t
        calculate_req_size( PLAN_TYPE                             plan_type,
                            const std::vector< output_location >& outputs,
                            std::size_t seconds_in_buffer )
        {
            if ( outputs.empty( ) )
            {
                return 0;
            }
            if ( plan_type == PLAN_TYPE::PLAN_16TH )
            {
                return outputs.back( ).data_offset +
                    outputs.back( ).bytes_per_16th;
            }
            return outputs.back( ).data_offset +
                ( outputs.back( ).bytes_per_16th * 16 * seconds_in_buffer );
        }

        template < typename Container >
        static std::vector< dcu_checksum >
        create_checksum_list( const Container& input )
        {
            using node_type = typename Container::value_type;
            std::vector< dcu_checksum > checksums( input.size( ) );
            std::transform( input.begin( ),
                            input.end( ),
                            checksums.begin( ),
                            []( const node_type& entry ) -> dcu_checksum {
                                dcu_checksum output{ };
                                output.identifier = entry.first;
                                output.config = entry.second;
                                return output;
                            } );
            std::sort(
                checksums.begin( ),
                checksums.end( ),
                []( const dcu_checksum& a, const dcu_checksum& b ) -> bool {
                    return a.identifier < b.identifier;
                } );
            return checksums;
        }

        static bool
        is_multi_source( const std::vector< dcu_span >& segments )
        {
            auto source = segments.front( ).source;
            auto it = std::find_if(
                segments.begin( ),
                segments.end( ),
                [ source ]( const dcu_span& cur_segment ) -> bool {
                    return cur_segment.source != source;
                } );
            return it != segments.end( );
        }

    public:
        data_plan( PLAN_TYPE                              type,
                   channel_name_list                      channels,
                   std::vector< dcu_span >                segments,
                   std::vector< output_location >         output_mapping,
                   checksum_t                             global_config,
                   std::map< dcu_identifier, checksum_t > dcu_configs,
                   std::size_t                            seconds_in_buffer )
            : type_{ type }, global_config_checksum_{ global_config },
              channels_{ std::move( channels ) }, segments_{ std::move(
                                                      segments ) },
              output_metadata_{ std::move( output_mapping ) },
              dcu_configs_{ create_checksum_list( dcu_configs ) },
              req_size_{ calculate_req_size(
                  type, output_metadata_, seconds_in_buffer ) },
              seconds_in_buffer_{ seconds_in_buffer }, multi_source_{
                  is_multi_source( segments_ )
              }
        {
            if ( channels_.size( ) != output_metadata_.size( ) )
            {
                throw std::runtime_error(
                    "Channel list and metadata do not match" );
            }
            if ( channels_.empty( ) || segments_.empty( ) )
            {
                throw std::runtime_error(
                    "Data plans must have channels and request segments" );
            }
        }

        data_plan( const data_plan& other ) = default;
        data_plan( data_plan&& other ) = default;

        data_plan& operator=( const data_plan& other ) = default;
        data_plan& operator=( data_plan&& other ) noexcept = default;

        /*!
         * @brief The required minimum buffer size to fulfil this request
         * @return buffer size
         * @noop tag data_plan::required_size
         */
        std::size_t
        required_size( ) const noexcept
        {
            return req_size_;
        }

        /*!
         * @brief return the plan type
         * @noop tag data_plan::type
         */
        PLAN_TYPE
        type( ) const noexcept
        {
            return type_;
        }

        /*!
         * @brief return the seconds in buffer value that was used when building
         * the plan
         * @noop tag data_plan::seconds_in_buffer
         */
        std::size_t
        seconds_in_buffer( ) const noexcept
        {
            return seconds_in_buffer_;
        }

        void load_data_in_sec( int                                 cycle,
                               const detail::buffer_offset_helper& offsets,
                               unsigned char    current_source,
                               void*            dest_buffer,
                               sec_data_status& status ) const;

        void zero_fill_16th( std::int64_t  cycle_offset,
                             unsigned char current_source,
                             void*         dest_buffer,
                             const std::vector< output_location >& locations,
                             data_status& status_buf ) const;

        void zero_fill_16th( std::int64_t  cycle_offset,
                             int           cycle,
                             unsigned char current_source,
                             void*         dest_buffer,
                             const std::vector< output_location >& locations,
                             sec_data_status& status_buf ) const;

        void
        dump( std::ostream& os )
        {
            for ( const auto& segment : segments_ )
            {
                os << "\tdcu: " << segment.dcu << " doffset: " << segment.offset
                   << " length: " << segment.length << "\n";
            }
        }

        data_status
        get_reference_status( ) const
        {
            return data_status( channels_.size( ) );
        }

        /*!
         * @brief Get the metadata needed to index the output buffer
         * @return a vector (a list in python) of output_metadata containing a
         * description of the output buffer.
         * @details This contains one entry per requested channel and provides
         * the information needed to decode the output buffer.
         * @noop tag data_plan::metadata
         */
        std::vector< output_metadata >
        metadata( ) const
        {
            std::vector< output_metadata > metadata;
            metadata.reserve( channels_.size( ) );

            for ( auto i = 0; i < channels_.size( ); ++i )
            {
                metadata.emplace_back( channels_[ i ], output_metadata_[ i ] );
            }
            return metadata;
        }

        template < typename F >
        void
        metadata( F&& f ) const
        {
            for ( auto i = 0; i < channels_.size( ); ++i )
            {
                f( channels_[ i ].c_str( ), output_metadata_[ i ] );
            }
        }

        const channel_name_list&
        channel_names( ) const
        {
            return channels_;
        }

        /*!
         * @brief Return if this plan requires multiple input streams
         * @return True if multi input streams are used, else false
         * @noop tag data_plan::multi_source
         */
        bool
        multi_source( ) const noexcept
        {
            return multi_source_;
        }

    private:
        PLAN_TYPE  type_;
        checksum_t global_config_checksum_;
        /* DOES this need to be full online_channel' structs */
        channel_name_list              channels_;
        std::vector< dcu_span >        segments_;
        std::vector< output_location > output_metadata_;
        std::vector< dcu_checksum >    dcu_configs_;
        std::size_t                    req_size_;
        std::size_t                    seconds_in_buffer_;
        bool                           multi_source_;
    };

    /*!
     * @brief An interface to read data from the live data stream, combining
     * access to channel metadata and data.
     * @noop tag client
     */
    class client
    {
    public:
        // explicit client( std::unique_ptr< detail::client_intl > p ) noexcept;
        /*!
         * @brief Construct a client by opening a named memory segment.
         * @param segment_name - the name of the shared memory segment.
         * @noop tag client::client
         */
        // explicit client( const std::string& segment_name );
        static std::unique_ptr< client >
        create( const std::string& conn_string );

        client( ) = default;
        virtual ~client( ) = default;

        //        client& operator=( const client& ) = delete;
        //        client& operator=( client&& ) noexcept;

        /*!
         * @brief Create a data plan to server the request.
         * @param plan_type - A PLAN_TYPE specifying the type of the plan.
         * @param channel_names - Channel names to be subscribed to
         * @param seconds_in_buffer - The size of the output buffer to be used.
         * @return a data plan
         *
         * @note seconds_in_buffer must be 0 for PLAN_16TH, and should be the
         * size in seconds of the target buffer for PLAN_SEC.  Seconds_in_buffer
         * and plan_type are used to help determine output data strides.
         * @noop tag client::plan_request
         */
        virtual data_plan
        plan_request( const PLAN_TYPE          plan_type,
                      const channel_name_list& channel_names,
                      std::size_t seconds_in_buffer = 0 ) const = 0;

        /*!
         * @brief Attempt to update a plan.
         * @param plan - the plan to update
         * @return true if the plan was updated.
         *
         * @note When a get data function returns that a plan needs update this
         * function should be called.  Note it is expected that the update may
         * fail if the metadata that is known about does not yet match the data.
         * So this function may need to be done multiple times
         * @noop tag client::update_plan
         */
        virtual bool update_plan( data_plan& plan ) const = 0;

        /*!
         * @brief Return a checksum of the current config.
         * @details this can be used to determine if the config of the client's
         * channel list has changed.
         * @return A checksum of the configuration.
         * @noop tag client::config_checksum
         */
        virtual checksum_t config_checksum( ) const = 0;

        /*!
         * @brief Return the current channel list that the client knows about.
         * @return The channels.
         * @noop tag client::channels
         */
        virtual std::vector< online_channel > channels( ) const = 0;

        virtual std::vector< std::size_t >
        size_list( const channel_name_list& names ) const = 0;

        /*!
         * @brief Retrieve a 1/16s block of data
         * @param plan - The data plan object describing the request
         * @param last_time_seen - The last timepoint that was seen
         * @param buffer - The output buffer
         * @param buffer_size - The size of the output buffer
         * @param status_buf - data_status buffer to store channel status values
         * @param output_byte_order - requested byte ordering for the data
         *
         * @details This function will serve the next buffer following the given
         * last_time_seen value.  For a new request the last_time_seen should be
         * set to a zero time.  In typical use cases it does return the next
         * value, however there may be gaps in the data if there was a
         * disruption
         * in the data stream, or it has been a significant amount of time since
         * the last call to get_16th_data and the requested data has aged out
         * of the buffers.
         */
        virtual void get_16th_data(
            const data_plan&  plan,
            const time_point& last_time_seen,
            void*             buffer,
            std::size_t       buffer_size,
            data_status&      status_buf,
            byte_order output_byte_order = byte_order::NATIVE ) const = 0;

        // FIXME: check on stride's usage
        /*!
         * @brief Retrieve one or more 1s blocks of data
         * @param plan - The data plan object describing the request
         * @param start_second - The second to retrieve
         * @param stride - the number of seconds of data to read (typically 1)
         * @param buffer - the output buffer
         * @param buffer_size - the size of the output buffer
         * @param status_buf - buffer for status of each channel
         * @param output_byte_order - requested byte ordering of the output data
         *
         * @note This is very different from get_16th_data.  This function
         * returns the requested time (or the latest if start_second is 0), NOT
         * the next second of data.  Put the requested time in, not the previous
         * time.
         */
        virtual void get_sec_data(
            const data_plan& plan,
            std::int64_t     start_second,
            std::size_t      stride,
            void*            buffer,
            std::size_t      buffer_size,
            sec_data_status& status_buf,
            byte_order       output_byte_order = byte_order::NATIVE ) const = 0;

        virtual void dump_16th( const std::string& filename ) const = 0;

    protected:
        // what does this return?
        virtual void get_avail( detail::avail_info& availability );

        static void copy_sec_data( detail::shared_mem_header* header,
                                   char*                      buffer,
                                   int                        block_index,
                                   std::int64_t               second_offset,
                                   const data_plan&           plan,
                                   unsigned char              current_source,
                                   sec_data_status&           status_buf );

        static void
        zero_fill_second( char*                                 buffer,
                          std::int64_t                          second_offset,
                          const std::vector< output_location >& locations,
                          sec_data_status&                      status_buf );

        // std::unique_ptr< detail::client_intl > p_;
    };

}; // namespace daqd_stream

template <>
struct std::hash< daqd_stream::dcu_identifier >
{
    std::size_t
    operator( )( const daqd_stream::dcu_identifier& input ) const
    {
        return input.hash( );
    }
};

#endif // DAQD_STREAM_HH
