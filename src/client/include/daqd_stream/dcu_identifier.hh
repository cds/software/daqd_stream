//
// Created by jonathan.hanks on 5/3/22.
//

#ifndef DAQD_STREAM_DCU_IDENTIFIER_HH
#define DAQD_STREAM_DCU_IDENTIFIER_HH

namespace daqd_stream
{
    /**
     * @brief dcu_identifier can identify a dcu block when multiple sources are
     * present.
     * @note essentially std::pair with appropriate member names.
     */
    struct dcu_identifier
    {
        dcu_identifier( ) = default;
        dcu_identifier( unsigned char Source, unsigned char Dcuid )
            : source{ Source }, dcuid{ Dcuid }
        {
        }
        dcu_identifier( const dcu_identifier& other ) noexcept = default;
        dcu_identifier&
        operator=( const dcu_identifier& other ) noexcept = default;

        bool
        operator==( const dcu_identifier& other ) const noexcept
        {
            return source == other.source && dcuid == other.dcuid;
        }
        bool
        operator!=( const dcu_identifier& other ) const noexcept
        {
            return source != other.source || dcuid != other.dcuid;
        }

        bool
        operator<( const dcu_identifier& other ) const noexcept
        {
            if ( source < other.source )
            {
                return true;
            }
            if ( source > other.source )
            {
                return false;
            }
            return dcuid < other.dcuid;
        }

        std::size_t
        hash( ) const noexcept
        {
            return ( ( static_cast< std::size_t >( source ) ) << 8 ) |
                static_cast< std::size_t >( dcuid );
        }

        unsigned char source{ 0 };
        unsigned char dcuid{ 0 };
    };
} // namespace daqd_stream

#endif // DAQD_STREAM_DCU_IDENTIFIER_HH
