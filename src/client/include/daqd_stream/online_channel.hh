/*
Copyright 2022 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#ifndef DAQD_STREAM_ONLINE_CHANNEL_HH
#define DAQD_STREAM_ONLINE_CHANNEL_HH

#include <type_traits>

#include <daqd_stream/basic_types.hh>
#include <daqd_stream/fixed_string.hh>

namespace daqd_stream
{
    /*!
     * @brief A named time series stream
     * @noop tag online_channel
     */
    struct online_channel
    {
        fixed_string< 60 > name;
        unsigned char      source_number;
        unsigned char      dcuid;
        DATA_TYPE          datatype;
        int                datarate;
        int                bytes_per_16th;
        std::size_t        data_offset;
        float              signal_gain;
        float              signal_slope;
        float              signal_offset;
        fixed_string< 40 > units;
    };
    static_assert( sizeof( online_channel::datatype ) == sizeof( short ),
                   "datatype must be the same as a short" );
} // namespace daqd_stream

#endif // DAQD_STREAM_ONLINE_CHANNEL_HH
