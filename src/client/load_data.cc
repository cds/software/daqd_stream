//
// Created by jonathan.hanks on 11/15/21.
//

#include "load_data.hh"

namespace daqd_stream
{
    namespace detail
    {
        void
        load_data_16th( const data_plan&                    plan,
                        unsigned char                       current_source,
                        const detail::buffer_offset_helper& offsets,
                        void*                               dest_buffer,
                        data_status&                        status )
        {
            detail::const_plan_internals intl{ plan };
            if ( !dest_buffer )
            {
                throw std::runtime_error(
                    "Invalid buffers passed to load_data" );
            }
            auto  status_it = status.channel_status.begin( );
            char* dest = reinterpret_cast< char* >( dest_buffer );

            std::array< std::uint32_t, detail::max_dcu( ) > dcu_status{ };
            dcu_status[ 0 ] = 0xbad;

            /**
             * FIXME: how do we handle this
             * remeber we only need to deal with the current source's entries
             */
            for ( const auto& config_entry : intl.dcu_configs( ) )
            {
                // FIXME: indexing
                auto cur_status =
                    offsets.status( config_entry.identifier.dcuid );
                if ( cur_status == 2 )
                {
                    cur_status = 0;
                }
                if ( cur_status != 0xbad )
                {
                    // FIXME: indexing
                    if ( config_entry.config !=
                         offsets.config( config_entry.identifier.dcuid ) )
                    {
                        status.plan_status = PLAN_STATUS::UPDATE_PLAN;
                        cur_status = 0x2000;
                    }
                }
                // FIXME: indexing
                dcu_status[ config_entry.identifier.dcuid ] = cur_status;
            }
            auto using_dcu0 = false;
            std::for_each(
                intl.segments( ).begin( ),
                intl.segments( ).end( ),
                [ current_source,
                  &dest,
                  &offsets,
                  &status_it,
                  &dcu_status,
                  &using_dcu0 ]( const dcu_span& span ) {
                    if ( span.source == current_source && span.dcu != 0 &&
                         dcu_status[ span.dcu ] != 0x2000 &&
                         dcu_status[ span.dcu ] != 0xbad )
                    {
                        const volatile char* start =
                            offsets.buffer( span.dcu ) + span.offset;
                        const volatile char* end = start + span.length;

                        // std::cout << "copying " << span.length << "
                        // bytes\n";
                        std::copy( start, end, dest );
                        dest += span.length;
                        *status_it = dcu_status[ span.dcu ];
                    }
                    else if ( span.source != current_source )
                    {
                        auto dest_end = dest + span.length;
                        dest = dest_end;
                    }
                    else
                    {
                        using_dcu0 = true;
                        auto dest_end = dest + span.length;
                        std::fill( dest, dest_end, 0 );
                        dest = dest_end;
                        *status_it = dcu_status[ span.dcu ];
                    }
                    ++status_it;
                } );

            if ( using_dcu0 && intl.global_config( ) != offsets.global_config )
            {
                status.plan_status = PLAN_STATUS::UPDATE_PLAN;
            }
        }

        /*
         // FIXME: how do we do this bit?
// globally, outside of this function likely.
detail::endian_convert_in_place( dest_buffer,
                        plan.required_size( ),
                        intl.output_metadata( ),
                        1,
                        output_byte_order );
         */
    } // namespace detail
} // namespace daqd_stream