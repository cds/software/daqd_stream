/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include <daqd_stream/daqd_stream.hh>

#include <boost/algorithm/string.hpp>

#include "internal.hh"
#include "multi_source_client.hh"
#include "shmem_client.hh"
#include "null_client.hh"
#include "posix_shmem.hh"

namespace daqd_stream
{
    void
    client::get_avail( detail::avail_info& availability )
    {
        throw std::runtime_error( "not implemented" );
    }

    void
    client::copy_sec_data( detail::shared_mem_header* header,
                           char*                      buffer,
                           int                        block_index,
                           std::int64_t               second_offset,
                           const data_plan&           plan,
                           unsigned char              current_source,
                           sec_data_status&           status_buf )
    {
        auto* cfg = header->get_config( );
        auto  cycle_offset = second_offset * detail::cycles_per_sec( );
        for ( auto cur_cycle = 0; cur_cycle < detail::cycles_per_sec( );
              ++cur_cycle )
        {
            detail::data_block&          db = header->blocks( block_index );
            detail::buffer_offset_helper offsets(
                db,
                detail::plan_internals::get_locations( plan ),
                cfg->checksums.master_config );
            if ( db.cycle == cur_cycle )
            {
                plan.load_data_in_sec( cycle_offset + cur_cycle,
                                       offsets,
                                       current_source,
                                       buffer,
                                       status_buf );
                block_index = header->next_block( block_index );
            }
            else
            {
                plan.zero_fill_16th( cur_cycle,
                                     cycle_offset + cur_cycle,
                                     current_source,
                                     buffer,
                                     *offsets.dest_information,
                                     status_buf );
            }
        }
    }

    void
    client::zero_fill_second( char*        buffer,
                              std::int64_t second_offset,
                              const std::vector< output_location >& locations,
                              sec_data_status&                      status_buf )
    {
        for ( const auto& cur_location : locations )
        {
            auto dest = buffer + cur_location.data_offset +
                ( second_offset * detail::cycles_per_sec( ) *
                  cur_location.bytes_per_16th );
            std::fill(
                dest,
                dest + cur_location.bytes_per_16th * detail::cycles_per_sec( ),
                0 );
        }
        for ( auto& status : status_buf.channel_status )
        {
            status.fill( 0xbad );
        }
    }

    std::unique_ptr< client >
    client::create( const std::string& conn_string )
    {
        static const std::string multi_prefix{ "multi://" };

        auto multi_prefix_handler =
            []( const std::string& conn_string ) -> std::unique_ptr< client > {
            auto name = conn_string.substr( multi_prefix.size( ) );
            detail::Posix_shared_memory shmem( name );
            auto*                       source_info = shmem.get_as<
                const volatile detail::multi_source_info_block >( );
            auto sources = get_configured_source_names( source_info );
            return std::make_unique< detail::multi_source_client >(
                std::move( shmem ), &source_info->signal_block, sources );
        };

        if ( conn_string.find( multi_prefix ) == 0 )
        {
            return multi_prefix_handler( conn_string );
        }
        else if ( conn_string.find( ',' ) != conn_string.npos )
        {
            std::vector< std::string > sources{ };
            boost::split( sources, conn_string, []( char ch ) -> bool {
                return ch == ',';
            } );
            return std::make_unique< detail::multi_source_client >( sources );
        }

        bool is_multi_source{ false };
        try
        {
            detail::Posix_shared_memory preview( conn_string );
            auto* header = preview.get_as< detail::multi_source_info_block >( );
            if ( header->magic == detail::multi_source_magic )
            {
                is_multi_source = true;
            }
        }
        catch ( ... )
        {
        }
        if ( is_multi_source )
        {
            return multi_prefix_handler( multi_prefix + conn_string );
        }

        return std::make_unique< detail::shmem_client >(
            detail::client_intl( conn_string ) );
    }
} // namespace daqd_stream
