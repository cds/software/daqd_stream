//
// Created by jonathan.hanks on 11/8/21.
//
#include <catch.hpp>

#include "multi_source_client.hh"

#include "plan_internals.hh"
#include "sample_channel_lists.hh"

#include "dummy_clients.hh"

#include <memory>

namespace
{
    std::vector< std::string >
    test_case1_channels( )
    {
        return { "sample_chan_6_4",
                 "sample_chan_5_4",
                 "sample_chan_6_2",
                 "sample_chan_5_3" };
    }

    void
    test_case1_initial_layout_verification( daqd_stream::data_plan& plan )
    {
        daqd_stream::detail::plan_internals internals{ plan };
        REQUIRE( plan.channel_names( ).size( ) == 4 );
        REQUIRE( internals.segments( ).size( ) == 4 );
        REQUIRE( internals.segments( )[ 0 ].source == 1 );
        REQUIRE( internals.segments( )[ 0 ].offset == 12 );
        REQUIRE( internals.segments( )[ 0 ].length == 4 );
        REQUIRE( internals.segments( )[ 0 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 1 ].source == 0 );
        REQUIRE( internals.segments( )[ 1 ].offset == 3 * ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 1 ].length == 4 );
        REQUIRE( internals.segments( )[ 1 ].dcu == 5 );

        REQUIRE( internals.segments( )[ 2 ].source == 1 );
        REQUIRE( internals.segments( )[ 2 ].offset == 4 );
        REQUIRE( internals.segments( )[ 2 ].length == 4 );
        REQUIRE( internals.segments( )[ 2 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 3 ].source == 0 );
        REQUIRE( internals.segments( )[ 3 ].offset == 2 * ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 3 ].length == ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 3 ].dcu == 5 );
    }

    void
    test_case1a_initial_layout_verification( daqd_stream::data_plan& plan )
    {
        daqd_stream::detail::plan_internals internals{ plan };
        REQUIRE( plan.channel_names( ).size( ) == 4 );
        REQUIRE( internals.segments( ).size( ) == 4 );
        REQUIRE( internals.segments( )[ 0 ].source == 1 );
        REQUIRE( internals.segments( )[ 0 ].offset == 12 );
        REQUIRE( internals.segments( )[ 0 ].length == 4 );
        REQUIRE( internals.segments( )[ 0 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 1 ].source == 0 );
        REQUIRE( internals.segments( )[ 1 ].offset == 2 * ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 1 ].length == 4 );
        REQUIRE( internals.segments( )[ 1 ].dcu == 5 );

        REQUIRE( internals.segments( )[ 2 ].source == 1 );
        REQUIRE( internals.segments( )[ 2 ].offset == 4 );
        REQUIRE( internals.segments( )[ 2 ].length == 4 );
        REQUIRE( internals.segments( )[ 2 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 3 ].source == 0 );
        REQUIRE( internals.segments( )[ 3 ].offset ==
                 2 * ( 256 * 4 / 16 ) + 4 );
        REQUIRE( internals.segments( )[ 3 ].length == ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 3 ].dcu == 5 );
    }

    void
    test_case1b_initial_layout_verification( daqd_stream::data_plan& plan )
    {
        daqd_stream::detail::plan_internals internals{ plan };
        REQUIRE( plan.channel_names( ).size( ) == 4 );
        REQUIRE( internals.segments( ).size( ) == 4 );
        REQUIRE( internals.segments( )[ 0 ].source == 1 );
        REQUIRE( internals.segments( )[ 0 ].offset == 12 );
        REQUIRE( internals.segments( )[ 0 ].length == 4 );
        REQUIRE( internals.segments( )[ 0 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 1 ].source == 0 );
        REQUIRE( internals.segments( )[ 1 ].dcu == 0 );

        REQUIRE( internals.segments( )[ 2 ].source == 1 );
        REQUIRE( internals.segments( )[ 2 ].offset == 4 );
        REQUIRE( internals.segments( )[ 2 ].length == 4 );
        REQUIRE( internals.segments( )[ 2 ].dcu == 6 );

        REQUIRE( internals.segments( )[ 3 ].source == 0 );
        REQUIRE( internals.segments( )[ 3 ].offset ==
                 2 * ( 256 * 4 / 16 ) + 4 );
        REQUIRE( internals.segments( )[ 3 ].length == ( 256 * 4 / 16 ) );
        REQUIRE( internals.segments( )[ 3 ].dcu == 5 );
    }

    std::ostream&
    operator<<( std::ostream& os, const daqd_stream::time_point& tp )
    {
        os << tp.seconds << ":" << tp.nano;
        return os;
    }
} // namespace

TEST_CASE( "You can initialize a avail_info a few ways" )
{
    using avail = daqd_stream::detail::avail_info;

    avail info1{ 1000000000, 0, 5 };
    REQUIRE( info1.end_gps( ) == 1000000000 );
    REQUIRE( info1.end_cycle( ) == 0 );
    REQUIRE( info1.end_time_value( ) ==
             ( static_cast< std::int64_t >( 1000000000 ) << 8 ) );
    REQUIRE( info1.start_gps( ) == 1000000000 - 5 );
    REQUIRE( info1.start_cycle( ) == 0 );
    REQUIRE( info1.start_time_value( ) ==
             ( static_cast< std::int64_t >( 1000000000 - 5 ) << 8 ) );
}

TEST_CASE( "Test the narrow_initial_time_range function" )
{
    using avail = daqd_stream::detail::avail_info;
    struct test_case_t
    {
        std::vector< avail >    input;
        daqd_stream::time_point output;
    };
    auto test_cases = std::vector< test_case_t >{
        test_case_t{ { avail( 0, 0, 5 ) }, { 0, 0 } },
        test_case_t{ std::vector< avail >{ avail( 1000000000, 0, 5 ) },
                     { 1000000000, 0 } },
    };
    for ( const auto& test_case : test_cases )
    {
        auto actual =
            daqd_stream::detail::narrow_initial_time_range( test_case.input );
        auto expected = test_case.output;
        std::cout << "want " << expected << " got " << actual << "\n";
        REQUIRE( expected == actual );
    }
}

TEST_CASE( "mutli_source_config_checksum should return a good checksum" )
{
    std::vector< std::unique_ptr< config_crc_test_client > > sources{ };
    sources.emplace_back( std::make_unique< config_crc_test_client >(
        static_cast< daqd_stream::checksum_t >( 42 ) ) );
    sources.emplace_back( std::make_unique< config_crc_test_client >(
        static_cast< daqd_stream::checksum_t >( 1024 ) ) );

    daqd_stream::detail::generic_multi_source_client< config_crc_test_client >
         client( std::move( sources ) );
    auto result = client.config_checksum( );
    REQUIRE( result != 0 );
    REQUIRE( result != 42 );
    REQUIRE( result != 1024 );
}

TEST_CASE( "multi_source_clients can make a plan spanning sources" )
{
    daqd_stream::detail::ifo_checksums mod5_cfg;
    mod5_cfg.dcu_config[ 5 ] = 42;
    daqd_stream::detail::ifo_checksums mod6_cfg;
    mod6_cfg.dcu_config[ 6 ] = 6;
    std::vector< std::unique_ptr< config_test_client > > sources{ };
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_1( ), mod5_cfg ) );
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_2( ), mod6_cfg ) );

    using test_client_t =
        daqd_stream::detail::generic_multi_source_client< config_test_client >;
    using test_internal_t =
        daqd_stream::detail::multi_source_client_internal< test_client_t >;

    test_client_t   client( std::move( sources ) );
    test_internal_t client_internals( client );

    daqd_stream::data_plan plan = client.plan_request(
        daqd_stream::PLAN_TYPE::PLAN_16TH, test_case1_channels( ), 0 );
    test_case1_initial_layout_verification( plan );

    mod5_cfg.dcu_config[ 5 ] = 43;
    client_internals.sources( )[ 0 ] = std::make_unique< config_test_client >(
        sample_channel_list_1a( ), mod5_cfg );

    REQUIRE( client.update_plan( plan ) );
    test_case1a_initial_layout_verification( plan );

    REQUIRE( !client.update_plan( plan ) );

    mod5_cfg.dcu_config[ 5 ] = 44;
    client_internals.sources( )[ 0 ] = std::make_unique< config_test_client >(
        sample_channel_list_1b( ), mod5_cfg );

    REQUIRE( client.update_plan( plan ) );
    test_case1b_initial_layout_verification( plan );

    mod5_cfg.dcu_config[ 5 ] = 42;
    client_internals.sources( )[ 0 ] = std::make_unique< config_test_client >(
        sample_channel_list_1( ), mod5_cfg );

    REQUIRE( client.update_plan( plan ) );
    test_case1_initial_layout_verification( plan );
}

TEST_CASE( "multi_source_clients can get a data size list" )
{
    daqd_stream::detail::ifo_checksums mod5_cfg;
    mod5_cfg.dcu_config[ 5 ] = 42;
    daqd_stream::detail::ifo_checksums mod6_cfg;
    mod6_cfg.dcu_config[ 6 ] = 6;
    std::vector< std::unique_ptr< config_test_client > > sources{ };
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_1( ), mod5_cfg ) );
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_2( ), mod6_cfg ) );

    using test_client_t =
        daqd_stream::detail::generic_multi_source_client< config_test_client >;

    test_client_t              client( std::move( sources ) );
    std::vector< std::string > channels = { };

    auto size_list = client.size_list( channels );
    REQUIRE( size_list.size( ) == channels.size( ) );

    size_list = client.size_list( test_case1_channels( ) );
    REQUIRE( size_list.size( ) == 4 );
    auto reference = { 4, 4, 4, 64 };
    REQUIRE( std::equal(
        size_list.begin( ), size_list.end( ), reference.begin( ) ) );
}