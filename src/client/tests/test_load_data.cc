//
// Created by jonathan.hanks on 11/16/21.
//
#include "catch.hpp"

#include "load_data.hh"

#include <algorithm>
#include <iterator>
#include <memory>

#include "dummy_clients.hh"
#include "multi_source_client.hh"
#include "sample_channel_lists.hh"

namespace
{
    std::unique_ptr< daqd_stream::detail::data_block >
    create_test_data_block1( )
    {
        auto db = std::make_unique< daqd_stream::detail::data_block >( );
        db->gps_second = 1000000000;
        db->cycle = 1;

        std::fill( db->dcu_offsets.begin( ), db->dcu_offsets.end( ), 0 );
        std::fill( db->dcu_status.begin( ), db->dcu_status.end( ), 0xbad );
        std::fill( db->dcu_config.begin( ), db->dcu_config.end( ), 0 );
        std::fill( std::begin( db->data ), std::end( db->data ), 0 );

        db->dcu_offsets[ 5 ] = 1024 * 512 / 16;
        db->dcu_status[ 5 ] = 2;
        db->dcu_config[ 5 ] = 42;

        auto* data_start =
            reinterpret_cast< float* >( &db->data[ db->dcu_offsets[ 5 ] ] );
        std::vector< std::pair< int, float > > layout{
            std::make_pair( 16, 5.1 ),
            std::make_pair( 16, 5.2 ),
            std::make_pair( 16, 5.3 ),
            std::make_pair( 1, 5.4 ),
        };
        for ( const auto cur_layout : layout )
        {
            auto data_end = data_start + cur_layout.first;

            std::fill( data_start, data_end, cur_layout.second );
            data_start += cur_layout.first;
        }
        return db;
    }

    std::unique_ptr< daqd_stream::detail::data_block >
    create_test_data_block2( )
    {
        auto db = std::make_unique< daqd_stream::detail::data_block >( );
        db->gps_second = 1000000000;
        db->cycle = 1;

        std::fill( db->dcu_offsets.begin( ), db->dcu_offsets.end( ), 0 );
        std::fill( db->dcu_status.begin( ), db->dcu_status.end( ), 0xbad );
        std::fill( db->dcu_config.begin( ), db->dcu_config.end( ), 0 );
        std::fill( std::begin( db->data ), std::end( db->data ), 0 );

        db->dcu_offsets[ 6 ] = 1024 * 1024 / 16;
        db->dcu_status[ 6 ] = 2;
        db->dcu_config[ 6 ] = 1024;

        auto* data_start =
            reinterpret_cast< float* >( &db->data[ db->dcu_offsets[ 6 ] ] );
        std::vector< std::pair< int, float > > layout{
            std::make_pair( 1, 6.1 ),
            std::make_pair( 1, 6.2 ),
            std::make_pair( 1, 6.3 ),
            std::make_pair( 1, 6.4 ),
        };
        for ( const auto cur_layout : layout )
        {
            auto data_end = data_start + cur_layout.first;

            std::fill( data_start, data_end, cur_layout.second );
            data_start += cur_layout.first;
        }
        return db;
    }
} // namespace

TEST_CASE( "Test loading a multi source 1/16s data slice one source at a time" )
{
    daqd_stream::detail::ifo_checksums mod5_cfg;
    mod5_cfg.dcu_config[ 5 ] = 42;
    daqd_stream::detail::ifo_checksums mod6_cfg;
    mod6_cfg.dcu_config[ 6 ] = 1024;
    std::vector< std::unique_ptr< config_test_client > > sources{ };
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_1( ), mod5_cfg ) );
    sources.emplace_back( std::make_unique< config_test_client >(
        sample_channel_list_2( ), mod6_cfg ) );

    using test_client_t =
        daqd_stream::detail::generic_multi_source_client< config_test_client >;

    test_client_t client( std::move( sources ) );

    std::vector< std::string > channels{
        "sample_chan_6_4",
        "sample_chan_5_4",
        "sample_chan_6_2",
        "sample_chan_5_3",
    };
    auto plan =
        client.plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH, channels, 0 );

    std::vector< char > dest{ };
    dest.resize( plan.required_size( ) );

    daqd_stream::data_status status{ };

    {
        auto block = create_test_data_block1( );
        std::vector< daqd_stream::output_location > dest_info{ };
        daqd_stream::detail::buffer_offset_helper   offsets(
            *block, dest_info, client.config_checksum( ) );

        status.reset( plan.channel_names( ).size( ) );
        daqd_stream::detail::load_data_16th(
            plan,
            0,
            offsets,
            reinterpret_cast< void* >( dest.data( ) ),
            status );
    }
    {
        auto block = create_test_data_block2( );
        std::vector< daqd_stream::output_location > dest_info{ };
        daqd_stream::detail::buffer_offset_helper   offsets(
            *block, dest_info, client.config_checksum( ) );
        status.reset( plan.channel_names( ).size( ) );
        daqd_stream::detail::load_data_16th(
            plan,
            1,
            offsets,
            reinterpret_cast< void* >( dest.data( ) ),
            status );
    }

    std::vector< float > expected_data{ };
    expected_data.push_back( 6.4 );
    expected_data.push_back( 5.4 );
    expected_data.push_back( 6.2 );
    std::fill_n( std::back_inserter( expected_data ), 16, 5.3 );

    auto* dest_as_float = reinterpret_cast< float* >( dest.data( ) );
    std::copy( dest_as_float,
               dest_as_float + expected_data.size( ),
               std::ostream_iterator< float >( std::cout, " " ) );
    std::cout << std::endl;
    std::copy( expected_data.begin( ),
               expected_data.end( ),
               std::ostream_iterator< float >( std::cout, " " ) );
    std::cout << std::endl;
    REQUIRE( std::equal(
        expected_data.begin( ), expected_data.end( ), dest_as_float ) );
}