//
// Created by jonathan.hanks on 2/4/22.
//

#ifndef DAQD_STREAM_TEST_HELPERS_HH
#define DAQD_STREAM_TEST_HELPERS_HH

#include <string>
#include <functional>
#include <utility>
#include <vector>

namespace helpers
{
    class cleanup_action
    {
    public:
        using action_type = std::function< void( void ) >;
        explicit cleanup_action( action_type&& action )
            : action_{ std::forward< action_type >( action ) }
        {
        }
        cleanup_action( const cleanup_action& ) = delete;
        cleanup_action( cleanup_action&& ) = delete;
        cleanup_action& operator=( const cleanup_action& ) = delete;
        cleanup_action& operator=( cleanup_action&& ) = delete;
        ~cleanup_action( )
        {
            action_( );
        }

    private:
        action_type action_;
    };

    extern std::function< void( void ) >
    cleanup_shmem_action( const std::vector< std::string >& names );

    extern std::string random_shm_name( );
} // namespace helpers

#endif // DAQD_STREAM_TEST_HELPERS_HH
