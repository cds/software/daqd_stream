//
// Created by jonathan.hanks on 3/2/22.
//
#include "catch.hpp"

#include <daqd_stream/daqd_stream.hh>

TEST_CASE( "dcu_identifiers have an ordering" )
{
    struct Test_case
    {
        unsigned char src1, dcuid1;
        unsigned char src2, dcuid2;
        bool          one_less_than_two;
    };
    std::vector< Test_case > test_cases{
        { 1, 1, 0, 5, false },
        { 0, 5, 1, 1, true },
    };
    for ( const auto& test : test_cases )
    {
        daqd_stream::dcu_identifier info1( test.src1, test.dcuid1 );
        daqd_stream::dcu_identifier info2( test.src2, test.dcuid2 );

        bool info1_less_than_info2{ info1 < info2 };
        REQUIRE( test.one_less_than_two == info1_less_than_info2 );
    }
}