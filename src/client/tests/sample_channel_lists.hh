//
// Created by jonathan.hanks on 11/9/21.
//

#ifndef DAQD_STREAM_SAMPLE_CHANNEL_LISTS_HH
#define DAQD_STREAM_SAMPLE_CHANNEL_LISTS_HH

#include <vector>

#include <daqd_stream/daqd_stream.hh>

extern std::vector< daqd_stream::online_channel > sample_channel_list_1( );
extern std::vector< daqd_stream::online_channel > sample_channel_list_1a( );
extern std::vector< daqd_stream::online_channel > sample_channel_list_1b( );

extern std::vector< daqd_stream::online_channel > sample_channel_list_2( );

#endif // DAQD_STREAM_SAMPLE_CHANNEL_LISTS_HH
