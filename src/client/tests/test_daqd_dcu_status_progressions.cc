//
// Created by jonathan.hanks on 6/4/21.
//
#include <chrono>
#include <cstring>
#include <deque>
#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "daqd_stream/daqd_stream.hh"

enum class TEST_TYPE
{
    TEST1S,
    TEST16Hz,
};

struct ConfigOptions
{
    std::string                 flag_dir{ "flags" };
    std::vector< unsigned int > status_codes{ };
    enum TEST_TYPE              test_type
    {
        TEST_TYPE::TEST16Hz
    };
};

std::string
make_flag( const std::string& prefix, int index, unsigned int status )
{
    std::ostringstream os;
    os << prefix << "_" << index << "_" << std::hex << status;
    return os.str( );
}

class Flag_writer
{
public:
    explicit Flag_writer( std::string dir ) : dir_{ std::move( dir ) }
    {
    }
    void
    set( const std::string& flag )
    {
        std::ofstream f( dir_ + "/" + flag );
    }

    void
    set_with_data( const std::string& flag, const std::string& data )
    {
        std::ofstream f( dir_ + "/" + flag );
        f << data;
    }

    bool
    is_set( const std::string& flag )
    {
        std::string fname = dir_ + "/" + flag;
        struct stat info
        {
        };
        return stat( fname.c_str( ), &info ) == 0;
    }

private:
    std::string dir_;
};

bool
starts_with( const daqd_stream::fixed_string< 60 >& s,
             const std::string&                     prefix )
{
    return std::strncmp( s.data( ), prefix.c_str( ), prefix.size( ) ) == 0;
}

void
do_basic_data_validation( const daqd_stream::data_plan&   plan,
                          const daqd_stream::data_status& status,
                          const std::vector< char >&      buffer )
{
    auto metadata = plan.metadata( );
    for ( auto i = 0; i < status.channel_status.size( ); ++i )
    {
        auto  cur_status = status.channel_status[ i ];
        auto& cur_metadata = metadata[ i ];

        auto start = buffer.data( ) + cur_metadata.location.data_offset;
        auto length = cur_metadata.location.bytes_per_16th;

        if ( cur_status == 0xbad || cur_status == 0x2000 )
        {
            if ( !std::all_of( start, start + length, []( char c ) -> bool {
                     return c == 0;
                 } ) )
            {
                throw std::runtime_error(
                    "channel marked bad or 2000 had non-zero data" );
            }
        }
        else
        {
            if ( !std::any_of( start, start + length, []( char c ) -> bool {
                     return c != 0;
                 } ) )
            {
                throw std::runtime_error(
                    "channel had unexpected all zero data" );
            }
        }
    }
}

std::vector< std::string >
select_channels( daqd_stream::client& client, Flag_writer& flags )
{
    std::vector< std::string > chans;

    auto full_chan_list = client.channels( );

    chans.emplace_back( full_chan_list[ 0 ].name.data( ) );
    chans.emplace_back( full_chan_list[ 1 ].name.data( ) );
    for ( const auto& cur_chan : full_chan_list )
    {
        if ( starts_with( cur_chan.name, "mod6" ) )
        {
            chans.emplace_back( cur_chan.name.data( ) );
            break;
        }
    }
    {
        std::ostringstream os{ };
        std::copy( chans.begin( ),
                   chans.end( ),
                   std::ostream_iterator< std::string >( os, "\n" ) );
        flags.set_with_data( "channels_selected", os.str( ) );
    }
    return chans;
}

void
wait_for_channels( daqd_stream::client& client )
{
    for ( auto iterations = 0; true; ++iterations )
    {
        auto chans = client.channels( );
        if ( !chans.empty( ) )
        {
            break;
        }
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
        if ( iterations > 20 )
        {
            throw std::runtime_error( "Unable to get channel list" );
        }
    }
}

void
client_loop( Flag_writer&                       flags,
             const std::vector< unsigned int >& status_codes )
{
    std::cout << "starting client loop" << std::endl;

    auto  client_ = daqd_stream::client::create( "daqd_stream" );
    auto& client = *client_;

    wait_for_channels( client );

    auto chans = select_channels( client, flags );

    auto plan = client.plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH, chans );
    std::vector< char > buffer( plan.required_size( ) );

    daqd_stream::time_point  last_time_seen{ };
    daqd_stream::data_status status{ };

    // run through each status
    auto         status_index = -1;
    unsigned int old_status{ 0 };
    for ( const auto expected_status : status_codes )
    {
        ++status_index;
        while ( true )
        {

            client.get_16th_data( plan,
                                  last_time_seen,
                                  reinterpret_cast< void* >( buffer.data( ) ),
                                  buffer.size( ),
                                  status );
            last_time_seen = status.gps;

            unsigned int s1, s2, s3;
            s1 = status.channel_status[ 0 ];
            s2 = status.channel_status[ 1 ];
            s3 = status.channel_status[ 2 ];

            std::cout << "got data " << std::hex << s1 << " " << s2 << " " << s3
                      << std::dec << std::endl;

            if ( status.plan_status == daqd_stream::PLAN_STATUS::UPDATE_PLAN )
            {
                if ( client.update_plan( plan ) )
                {
                    std::cout << "The data plan was updated" << std::endl;
                }
            }

            if ( s1 == expected_status && s2 == expected_status && s3 == 0 )
            {
                std::cout << "mod5 status changed to " << expected_status
                          << std::endl;
                flags.set( make_flag( "mod5", status_index, expected_status ) );
                old_status = expected_status;

                auto expected_plan_status =
                    ( expected_status == 0x2000
                          ? daqd_stream::PLAN_STATUS::UPDATE_PLAN
                          : daqd_stream::PLAN_STATUS::OK );
                if ( status.plan_status != expected_plan_status )
                {
                    std::cout << "plan status = " << (int)status.plan_status
                              << " expected " << (int)expected_status
                              << std::endl;
                    throw std::runtime_error( "Unexpected plan status found" );
                }

                break;
            }
            else if ( s1 == old_status && s2 == old_status && s3 == 0 )
            {
                // still waiting for a change in status
                // this check may not make sense
                //                auto expected_plan_status =
                //                    ( old_status == 0x2000
                //                          ?
                //                          daqd_stream::PLAN_STATUS::UPDATE_PLAN
                //                          : daqd_stream::PLAN_STATUS::OK );
                //                if ( status.plan_status !=
                //                expected_plan_status )
                //                {
                //                    std::cout << "plan status = " <<
                //                    (int)status.plan_status << " expected " <<
                //                    (int)expected_status << std::endl;
                //                    throw std::runtime_error( "Unexpected plan
                //                    status found" );
                //                }
            }
            else
            {
                std::cout << "unexpected state " << std::hex << s1 << " "
                          << std::hex << s2 << " " << std::hex << s3 << std::dec
                          << std::endl;
                std::cout << "expecting " << std::hex << expected_status
                          << " with prev status " << std::hex << old_status
                          << std::dec << std::endl;
                throw std::runtime_error(
                    "channel status in an unexpected state" );
            }
            do_basic_data_validation( plan, status, buffer );
        }
    }
}

template < typename Container >
typename Container::value_type
or_values( const Container& c )
{
    typename Container::value_type val{ 0 };
    for ( const auto& entry : c )
    {
        val |= entry;
    }
    return val;
}

[[noreturn]] void
client_loop_1s( Flag_writer&                       flags,
                const std::vector< unsigned int >& status_codes )
{
    std::cout << "Starting 1s client loop" << std::endl;

    auto  client_ = daqd_stream::client::create( "daqd_stream" );
    auto& client = *client_;

    wait_for_channels( client );

    auto chans = select_channels( client, flags );

    auto plan =
        client.plan_request( daqd_stream::PLAN_TYPE::PLAN_SEC, chans, 1 );
    std::vector< char > buffer( plan.required_size( ) );

    daqd_stream::time_point      last_time_seen{ };
    daqd_stream::sec_data_status status{ };

    while ( true )
    {
        auto stride = 1;
        client.get_sec_data( plan,
                             last_time_seen.seconds,
                             stride,
                             reinterpret_cast< void* >( buffer.data( ) ),
                             buffer.size( ),
                             status );

        last_time_seen.seconds = status.gps.seconds + stride;

        std::cout << "Got data " << std::dec << status.gps.seconds << " - "
                  << std::hex << or_values( status.channel_status[ 0 ] ) << " "
                  << std::hex << or_values( status.channel_status[ 1 ] ) << " "
                  << std::hex << or_values( status.channel_status[ 2 ] )
                  << std::endl;
        if ( status.plan_status == daqd_stream::PLAN_STATUS::UPDATE_PLAN )
        {
            std::cout << "need to update the plan\n";
            client.update_plan( plan );
        }
    }
}

void
run_client( const ConfigOptions& cfg )
{
    Flag_writer flags{ cfg.flag_dir };

    while ( flags.is_set( "pause" ) )
    {
        usleep( 250 * 1000 );
    }

    //    try
    //    {
    if ( cfg.test_type == TEST_TYPE::TEST16Hz )
    {
        client_loop( flags, cfg.status_codes );
    }
    else
    {
        client_loop_1s( flags, cfg.status_codes );
    }
    //    }
    //    catch ( ... )
    //    {
    //        flags.set( "client_aborted" );
    //        throw;
    //    }
}

/**
 * @brief find the next occurrence of a character in a NULL terminated string
 * @param it input
 * @param target character to look for
 * @return pointer to the occurrence of target in it or nullptr.
 */
const char*
find_next( const char* it, char target )
{
    if ( !it )
    {
        return nullptr;
    }
    for ( auto ch = *it; *it; ch = *( ++it ) )
    {
        if ( ch == target )
        {
            return it;
        }
    }
    return nullptr;
}

/**
 * @brief given a input string of hex numbers separated by ',' return
 * the hex numbers in a vector.
 */
std::vector< unsigned int >
parse_status_codes( const std::string& input )
{
    std::vector< unsigned int > codes{ };
    if ( input.empty( ) )
    {
        throw std::runtime_error( "invalid status code list" );
    }
    const char* input_ = input.data( );
    auto        cur = input_;
    auto        next = find_next( cur, ',' );
    while ( next != nullptr )
    {
        if ( next != cur )
        {
            auto entry = std::string( cur, next );
            if ( !entry.empty( ) )
            {
                unsigned int       status = 0;
                std::istringstream istream( entry );
                istream >> ( std::hex ) >> status;
                codes.push_back( status );
            }
        }
        cur = next + 1;
        next = find_next( cur, ',' );
    }
    {
        auto entry = std::string( cur );
        if ( !entry.empty( ) )
        {
            unsigned int       status = 0;
            std::istringstream istream( entry );
            istream >> ( std::hex ) >> status;
            codes.push_back( status );
        }
    }

    std::cout << "status codes:\n";
    for ( const auto& entry : codes )
    {
        std::cout << "\t" << std::hex << entry << "\n";
    }

    return codes;
}

void
usage( const char* progname )
{
    std::cout << "Usage:\n\t" << progname << " options\n";
    std::cout << "Where options are:\n";
    std::cout << "\t--flag-dir|-f <directory> - specify the flag directory\n";
    std::cout << "\t--status-codes|-s <int list> - list of comma separated hex "
                 "values of expected status codes\n";
    std::cout << "\t--one-sec - do a test on 1s requests (defaults to 16Hz)\n";
    std::cout << "\t--help|-h - this help\n";
}

ConfigOptions
parse_args( int argc, char* argv[] )
{
    ConfigOptions             cfg{ };
    std::deque< std::string > args{ };

    auto pop = [ &args ]( ) -> std::string {
        std::string results{ };
        if ( args.empty( ) )
        {
            throw std::runtime_error( "Insufficient arguments" );
        }
        results = args.front( );
        args.pop_front( );
        return results;
    };

    try
    {
        for ( int i = 1; i < argc; ++i )
        {
            args.emplace_back( argv[ i ] );
        }
        while ( !args.empty( ) )
        {
            auto arg = pop( );
            if ( arg == "--flag-dir" || arg == "-f" )
            {
                cfg.flag_dir = pop( );
            }
            else if ( arg == "--status-codes" || arg == "-s" )
            {
                cfg.status_codes = parse_status_codes( pop( ) );
            }
            else if ( arg == "--one-sec" )
            {
                cfg.test_type = TEST_TYPE::TEST1S;
            }
            else if ( arg == "--help" || arg == "-h" )
            {
                usage( argv[ 0 ] );
                exit( 0 );
            }
            else
            {
                throw std::runtime_error( "unknown argument" );
            }
        }
    }
    catch ( std::runtime_error& )
    {
        usage( argv[ 0 ] );
        exit( 1 );
    }
    return cfg;
}

int
main( int argc, char* argv[] )
{
    auto cfg = parse_args( argc, argv );
    if ( cfg.status_codes.empty( ) )
    {
        throw std::runtime_error(
            "At least one status code must be requested" );
    }

    run_client( cfg );

    std::cout << "test_dropping_data done" << std::endl;

    return 0;
}