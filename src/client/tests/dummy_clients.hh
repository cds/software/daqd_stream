//
// Created by jonathan.hanks on 11/16/21.
//

#ifndef DAQD_STREAM_DUMMY_CLIENTS_HH
#define DAQD_STREAM_DUMMY_CLIENTS_HH

#include "null_client.hh"

class config_crc_test_client : public daqd_stream::detail::null_client
{
public:
    explicit config_crc_test_client( daqd_stream::checksum_t value )
        : null_client( ), checksum{ value }
    {
    }
    ~config_crc_test_client( ) override = default;
    daqd_stream::checksum_t
    config_checksum( ) const override
    {
        return checksum;
    }

    daqd_stream::checksum_t checksum{ };
};

class config_test_client : public daqd_stream::detail::null_client
{
public:
    explicit config_test_client(
        std::vector< daqd_stream::online_channel > channels,
        const daqd_stream::detail::ifo_checksums&  checksums )
        : null_client( ), channels_{ std::move( channels ) }, checksums_{
              checksums
          }
    {
        checksums_.recalculate_master_config( );
    }
    ~config_test_client( ) override = default;
    daqd_stream::checksum_t
    config_checksum( ) const override
    {
        return checksums_.master_config;
    }
    void
    get_internal_config(
        daqd_stream::detail::ifo_checksums& checksums,
        daqd_stream::detail::shared_span< daqd_stream::online_channel >&
            channels ) override
    {
        channels =
            daqd_stream::detail::shared_span< daqd_stream::online_channel >(
                channels_.data( ), channels_.size( ) );
        checksums = checksums_;
    }
    std::vector< daqd_stream::online_channel >
    channels( ) const override
    {
        std::vector< daqd_stream::online_channel > result( channels_ );
        return result;
    }

    std::vector< daqd_stream::online_channel > channels_{ };
    daqd_stream::detail::ifo_checksums         checksums_{ };
};

#endif // DAQD_STREAM_DUMMY_CLIENTS_HH
