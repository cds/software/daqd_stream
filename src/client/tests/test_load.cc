//
// Created by jonathan.hanks on 6/15/21.
//
#include <array>
#include <atomic>
#include <condition_variable>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <deque>
#include <mutex>
#include <random>
#include <string>
#include <sstream>
#include <thread>

#include <daqd_stream/daqd_stream.hh>

class Random
{
    static auto
    get_seed( ) -> auto
    {
        static std::random_device rd;
        return rd( );
    }

public:
    Random( ) : r_{ get_seed( ) }
    {
    }
    explicit Random( unsigned int seed ) : r_{ seed }
    {
    }

    Random( const Random& ) = delete;
    Random( Random&& ) = delete;

    Random& operator=( const Random& ) = delete;
    Random& operator=( Random&& ) = delete;

    int
    intn( int max )
    {
        std::uniform_int_distribution< int > dist( 0, max - 1 );
        return dist( r_ );
    }

private:
    std::default_random_engine r_;
};

struct Stats
{
    std::atomic< int >                   running_threads{ 0 };
    std::atomic< int >                   failed_threads{ 0 };
    std::atomic< int >                   completed_threads{ 0 };
    std::atomic< int >                   slow_queries{ 0 };
    std::atomic< int >                   good_queries{ 0 };
    std::atomic< int >                   skipped_cycles{ 0 };
    std::array< std::atomic< int >, 10 > timing_bins{ };
};

Stats stats{ };

struct ConfigOptions
{
    int num_threads{ 0 };
    int num_channels{ 0 };
    int cycles{ 15 };
};

void
usage( char* progam_name, const char* msg = nullptr )
{
    if ( msg )
    {
        std::cout << msg << "\n";
    }
    std::cout << "Usage:\n" << progam_name << " options\n";
    std::cout << "Where options are:\n";
    std::cout << "\t--threads|-t <num> - Number of threads\n";
    std::cout << "\t--channels|-c <num> - Number of channels for each thread\n";
    std::cout
        << "\t--cycles <num> - Number of 2s cycles to run <0 is forever\n";
    std::cout << "\t--help|-h - this help\n";
}

int
parse_int( const std::string& input )
{
    int                result{ 0 };
    std::istringstream stream{ input };
    stream >> result;
    return result;
}

ConfigOptions
parse_args( int argc, char* argv[] )
{
    ConfigOptions             opts{ };
    std::deque< std::string > args{ };
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }
    auto pop = [ &args ]( ) -> std::string {
        std::string arg{ };
        if ( args.empty( ) )
        {
            throw std::runtime_error( "expected an argument" );
        }
        arg = args.front( );
        args.pop_front( );
        return arg;
    };
    try
    {
        while ( !args.empty( ) )
        {
            auto arg = pop( );
            if ( arg == "--threads" || arg == "-t" )
            {
                opts.num_threads = parse_int( pop( ) );
            }
            else if ( arg == "--channels" || arg == "-c" )
            {
                opts.num_channels = parse_int( pop( ) );
            }
            else if ( arg == "--cycles" )
            {
                opts.cycles = parse_int( pop( ) );
            }
            else if ( arg == "--help" || arg == "-h" )
            {
                usage( argv[ 0 ] );
                std::exit( 0 );
            }
            else
            {
                throw std::runtime_error( "Unexpected argument" );
            }
        }
        if ( opts.num_channels < 1 || opts.num_threads < 1 )
        {
            throw std::runtime_error( "parameters must be > 0" );
        }
    }
    catch ( std::runtime_error& err )
    {
        usage( argv[ 0 ], err.what( ) );
        std::exit( 1 );
    }
    return opts;
}

void
client_loop( daqd_stream::client&                              client,
             unsigned int                                      random_seed,
             const std::vector< daqd_stream::online_channel >& channel_list,
             int                                               num_channels,
             int                                               id,
             std::atomic< bool >*                              done )
{
    Random random( random_seed );
    stats.running_threads++;
    try
    {

        std::vector< std::string > channels;
        channels.reserve( num_channels );

        for ( int i = 0; i < num_channels; ++i )
        {
            std::string cur;
            do
            {
                cur = channel_list[ random.intn( num_channels ) ].name.data( );
            } while ( std::find( channels.begin( ), channels.end( ), cur ) !=
                      channels.end( ) );
            channels.emplace_back( std::move( cur ) );
        }
        auto plan =
            client.plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH, channels );

        std::vector< char > buffer( plan.required_size( ) );

        daqd_stream::time_point  last_seen{ };
        daqd_stream::data_status status{ };

        int long_periods = 0;
        int good_periods = 0;
        int bad_periods = 0;
        int prev_cycle = -1;
        while ( !*done )
        {
            auto start = std::chrono::steady_clock::now( );
            client.get_16th_data(
                plan, last_seen, buffer.data( ), buffer.size( ), status );
            last_seen = status.gps;
            auto end = std::chrono::steady_clock::now( );
            auto delta =
                std::chrono::duration_cast< std::chrono::milliseconds >( end -
                                                                         start )
                    .count( );
            if ( prev_cycle >= 0 )
            {
                if ( ( prev_cycle + 1 ) % 16 != status.cycle )
                {
                    ++bad_periods;
                }
            }
            prev_cycle = status.cycle;
            if ( delta > 85 )
            {
                ++long_periods;
            }
            else
            {
                ++good_periods;
            }
            int bin = delta / 10;
            if ( bin < 0 )
            {
                bin = 0;
            }
            else if ( bin >= stats.timing_bins.size( ) )
            {
                bin = stats.timing_bins.size( ) - 1;
            }
            stats.timing_bins[ bin ]++;
            if ( status.cycle == 15 )
            {
                stats.good_queries += good_periods;
                stats.slow_queries += long_periods;
                stats.skipped_cycles += bad_periods;
                bad_periods = good_periods = long_periods = 0;
            }
        }
        stats.completed_threads++;
    }
    catch ( ... )
    {
        stats.failed_threads++;
    }
    stats.running_threads--;
}

int
main( int argc, char* argv[] )
{
    std::random_device seed_source;
    auto               opts = parse_args( argc, argv );
    std::cout << "Opening daqd_stream" << std::endl;
    auto client = daqd_stream::client::create( "daqd_stream" );
    auto channels = client->channels( );
    std::cout << "There are " << channels.size( ) << " available channels"
              << std::endl;

    for ( auto& bin : stats.timing_bins )
    {
        bin = 0;
    }
    if ( opts.num_channels > channels.size( ) )
    {
        throw std::runtime_error( "requested too many channels" );
    }

    std::atomic< bool >        done{ false };
    std::vector< std::thread > threads;
    threads.reserve( opts.num_threads );

    for ( auto i = 0; i < opts.num_threads; ++i )
    {
        threads.emplace_back( client_loop,
                              std::ref( *client ),
                              seed_source( ),
                              std::ref( channels ),
                              opts.num_channels,
                              i,
                              &done );
    }

    for ( auto i = 0; i < opts.cycles || opts.cycles < 0; ++i )
    {
        std::this_thread::sleep_for( std::chrono::seconds( 2 ) );
        std::cout << i << ") r/c/f " << stats.running_threads << "/"
                  << stats.completed_threads << "/" << stats.failed_threads;
        std::cout << " long/good/skipped cycles = " << stats.slow_queries << "/"
                  << stats.good_queries << "/" << stats.skipped_cycles << "\n";
        std::cout << "bins ";

        std::array< int, stats.timing_bins.size( ) > bins{ };
        int                                          bin_total{ 0 };
        for ( auto j = 0; j < stats.timing_bins.size( ); ++j )
        {
            bins[ j ] = stats.timing_bins[ j ].exchange( 0 );
            bin_total += bins[ j ];
        }
        for ( auto j = 0; j < stats.timing_bins.size( ); ++j )
        {
            std::cout << "<" << ( j + 1 ) * 10 << " "
                      << ( bin_total > 0 ? bins[ j ] * 100 / bin_total : 0 )
                      << "% ";
        }
        std::cout << "\n";
    }
    done = true;
    for ( auto& cur : threads )
    {
        cur.join( );
    }
    return 0;
}