//
// Created by jonathan.hanks on 11/17/21.
//
#include "catch.hpp"

#include "endian_utility.hh"
#include "data_types.hh"
#include <boost/endian/conversion.hpp>

namespace
{
    float
    to_little( float f )
    {
        static_assert( sizeof( std::int32_t ) == sizeof( float ),
                       "int32_t and float must be equally sized types" );
        std::int32_t tmp = *reinterpret_cast< std::int32_t* >( &f );
        boost::endian::native_to_little_inplace( tmp );
        return *reinterpret_cast< float* >( &tmp );
        ;
    }

    float
    to_little( std::int16_t i )
    {
        return boost::endian::native_to_little( i );
    }

    float
    to_big( float f )
    {
        static_assert( sizeof( std::int32_t ) == sizeof( float ),
                       "int32_t and float must be equally sized types" );
        std::int32_t tmp = *reinterpret_cast< std::int32_t* >( &f );
        boost::endian::native_to_big_inplace( tmp );
        return *reinterpret_cast< float* >( &tmp );
        ;
    }

    float
    to_big( std::int16_t i )
    {
        return boost::endian::native_to_big( i );
    }
} // namespace

TEST_CASE( "Endian remapping from native to LITTLE endian works as expected" )
{
    struct test_struct
    {
        float a;
        float b;
        short c;
        short d;
    };
    test_struct t{ 1.1, 4.2, 1024, 5 };
    static_assert( sizeof( test_struct ) == 4 + 4 + 2 + 2,
                   "struct size must without padding" );

    using DT = daqd_stream::DATA_TYPE;

    auto to_chan = []( DT data_type, int rate ) -> daqd_stream::online_channel {
        daqd_stream::online_channel chan{ };
        chan.datatype = data_type;
        chan.datarate = rate;
        chan.bytes_per_16th =
            daqd_stream::detail::data_type_size( data_type ) * rate / 16;
        return chan;
    };

    std::vector< daqd_stream::output_location > locations{
        daqd_stream::output_location( to_chan( DT::FLOAT32, 32 ), 0 ),
        daqd_stream::output_location( to_chan( DT::INT16, 16 ), 0 ),
        daqd_stream::output_location( to_chan( DT::INT16, 16 ), 0 ),
    };

    daqd_stream::detail::endian_convert_in_place(
        reinterpret_cast< void* >( &t ),
        sizeof( t ),
        locations,
        1,
        daqd_stream::byte_order::LITTLE );

    REQUIRE( t.a == to_little( 1.1f ) );
    REQUIRE( t.b == to_little( 4.2f ) );
    REQUIRE( t.c == to_little( static_cast< std::int16_t >( 1024 ) ) );
    REQUIRE( t.d == to_little( static_cast< std::int16_t >( 5 ) ) );

    if ( boost::endian::order::native == boost::endian::order::little )
    {
        REQUIRE( t.a == 1.1f );
        REQUIRE( t.b == 4.2f );
        REQUIRE( t.c == 1024 );
        REQUIRE( t.d == 5 );
    }
}

TEST_CASE( "Endian remapping from native to BIG endian works as expected" )
{
    struct test_struct
    {
        float        a;
        float        b;
        std::int16_t c;
        std::int16_t d;
    };
    test_struct t{ 1.1, 4.2, 1024, 5 };
    static_assert( sizeof( test_struct ) == 4 + 4 + 2 + 2,
                   "struct size must without padding" );

    using DT = daqd_stream::DATA_TYPE;

    auto to_chan = []( DT data_type, int rate ) -> daqd_stream::online_channel {
        daqd_stream::online_channel chan{ };
        chan.datatype = data_type;
        chan.datarate = rate;
        chan.bytes_per_16th =
            daqd_stream::detail::data_type_size( data_type ) * rate / 16;
        return chan;
    };

    std::vector< daqd_stream::output_location > locations{
        daqd_stream::output_location( to_chan( DT::FLOAT32, 32 ), 0 ),
        daqd_stream::output_location( to_chan( DT::INT16, 16 ), 8 ),
        daqd_stream::output_location( to_chan( DT::INT16, 16 ), 10 ),
    };

    daqd_stream::detail::endian_convert_in_place(
        reinterpret_cast< void* >( &t ),
        sizeof( t ),
        locations,
        1,
        daqd_stream::byte_order::BIG );

    REQUIRE( t.a == to_big( 1.1f ) );
    REQUIRE( t.b == to_big( 4.2f ) );
    REQUIRE( t.c == to_big( static_cast< std::int16_t >( 1024 ) ) );
    REQUIRE( t.d == to_big( static_cast< std::int16_t >( 5 ) ) );

    if ( boost::endian::order::native == boost::endian::order::big )
    {
        REQUIRE( t.a == 1.1f );
        REQUIRE( t.b == 4.2f );
        REQUIRE( t.c == 1024 );
        REQUIRE( t.d == 5 );
    }
}