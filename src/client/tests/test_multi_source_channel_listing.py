import contextlib
import integration
import os.path
import time
import threading

import numpy
import requests
import sys
sys.path.insert(0, "/home/jonathan.hanks/Documents/Programming/scratch/daqd_stream/cmake-build-debug/python")

import daqd_stream
import os

print(daqd_stream.__file__)

integration.Executable(name="daqd_stream_srv", hints=['../apps/daqd_stream_server'], description="daqd stream server")
integration.Executable(name="fe_simulated_streams",
                       hints=["/usr/bin", ],
                       description="data server")
integration.Executable(name="cps_xmit",
                       hints=["/usr/bin", ],
                       description="data xmit")
integration.Executable(name="cds_metadata_server", hints=["/usr/bin"], description="channel metadata server")
#integration.Executable(name="test_daqd_dcu_status_progressions", hints=[], description="test program")

# flag file directory
# valid flags are "receiving_data", "mod5_missing", "mod5_restored" "client_aborted"
#flag_dir = integration.state.temp_dir("event_flags")

ini_dir0 = integration.state.temp_dir('inis0')
master_file0 = os.path.join(ini_dir0, 'master')

ini_dir1 = integration.state.temp_dir('inis1')
master_file1 = os.path.join(ini_dir1, 'master')

generator0 = integration.Process("fe_simulated_streams",
                                ["-b", "local_dc0",
                                 "-m", "100",
                                 "-k", "100",
                                 "-R", "5",
                                 "-i", ini_dir0,
                                 "-M", master_file0,
                                 "-admin", "127.0.0.1:10000",
                                 "-s", "shm://",
                                 ])
xmit0 = integration.Process("cps_xmit",
                           ["-b", "shm://local_dc0",
                            "-m", "100",
                            "-p", "tcp://127.0.0.1:9000"],)

generator1 = integration.Process("fe_simulated_streams",
                                 ["-b", "local_dc1",
                                  "-m", "100",
                                  "-k", "100",
                                  "-D", "20,21,22",
                                  "-i", ini_dir1,
                                  "-M", master_file1,
                                  "-admin", "127.0.0.1:10001",
                                  "-s", "shm://",
                                  ])
xmit1 = integration.Process("cps_xmit",
                            ["-b", "shm://local_dc1",
                             "-m", "100",
                             "-p", "tcp://127.0.0.1:9001"],)

metadata0 = integration.Process("cds_metadata_server",
                               ["-listen", "127.0.0.1:9100",
                                "-master", master_file0, ])
metadata1 = integration.Process("cds_metadata_server",
                                ["-listen", "127.0.0.1:9101",
                                 "-master", master_file1, ])

server0 = integration.Process("daqd_stream_srv", [
    "-d", "tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9100",
    "-n", "daqd_stream0",
])
server1 = integration.Process("daqd_stream_srv", [
    "-d", "tcp://127.0.0.1:9001|meta://cds://127.0.0.1:9101|delay://4",
    "-n", "daqd_stream1",
    "--additional-plugins", "delay",
])

def fail():
    raise RuntimeError("Fail here")


@contextlib.contextmanager
def cleanup_shmem(segments=('local_dc', 'daqd_stream0', 'daqd_stream1',)):
    yield
    for entry in segments:
        path = os.path.join('/dev/shm', entry)
        try:
            os.remove(path)
        except:
            pass


channel_lists = {}
config_separate = {}
channels = []
config_multi = 0


def get_channels_seperate(index):
    def wrapper():
        global channel_lists
        global config_separate

        client = daqd_stream.client("daqd_stream{0}".format(index))
        channel_lists[index] = client.channels()
        config_separate[index] = client.config_checksum()
        del client

    return wrapper


def get_channels():
    global channels
    global config_multi

    client = daqd_stream.client("daqd_stream0,daqd_stream1")
    channels = client.channels()
    config_multi = client.config_checksum()
    del client


def check_channels():
    global channels
    global channel_lists
    print("Separate channels {0} {1}".format(len(channel_lists[0]), len(channel_lists[1])))
    print("Combined channels {0}".format(len(channels)))
    assert(len(channels) == len(channel_lists[0]) + len(channel_lists[1]))


def check_config_checksum():
    global config_separate
    global config_multi
    assert (config_multi != 0)
    assert (config_separate[0] != config_multi)
    assert (config_separate[1] != config_multi)


def wait(delay=3.0):
    return integration.callable(time.sleep, delay)


def echo(msg:str):
    def do_echo():
        print(msg)
    return do_echo


with cleanup_shmem():
    integration.Sequence(
        [
            integration.state.preserve_files,
            generator0.run,
            generator1.run,
            integration.wait_tcp_server(port=10000, timeout=5),
            integration.wait_tcp_server(port=10001, timeout=5),
            xmit0.run,
            xmit1.run,
            integration.wait_tcp_server(port=9000, timeout=5),
            integration.wait_tcp_server(port=9001, timeout=5),
            metadata0.run,
            metadata1.run,
            integration.wait_tcp_server(port=9100, timeout=5),
            integration.wait_tcp_server(port=9101, timeout=5),
            echo("About to start server0"),
            server0.run,
            echo("About to start server0"),
            server1.run,
            echo("Wait for servers"),
            wait(1.0),
            echo("Starting tests"),

            get_channels_seperate(0),
            get_channels_seperate(1),
            get_channels,
            check_channels,
            check_config_checksum,

            server0.ignore,
            server1.ignore,
            metadata0.ignore,
            metadata1.ignore,
            xmit1.ignore,
            xmit0.ignore,
            generator0.ignore,
            generator0.ignore,
         ]
    )
print("Channel count {0} {1}".format(len(channel_lists[0]), len(channel_lists[1])))
print("done!")
