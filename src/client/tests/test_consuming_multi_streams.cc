//
// Created by jonathan.hanks on 2/3/22.
//
#include "catch.hpp"
#include "frame_utils.hh"
#include <atomic>
#include <iostream>
#include <functional>
#include <thread>

#include <cds-pubsub/pub.hh>
#include <daqd_stream/daqd_stream_server.hh>
#include "test_helpers.hh"
#include "simple_data.hh"
#include "endian_utility.hh"

/**
 * @brief A simple wrapper around std::thread that takes a job to run in a
 * background thread, and a stop mechanism. This ensures that the stop function
 * is called before the thread is joined.
 */
class Thread
{
public:
    using target_function_t = std::function< void( void ) >;
    using stop_function_t = std::function< void( void ) >;
    Thread( target_function_t&& target, stop_function_t&& stop )
        : t_( target ), stop_action_{ std::move( stop ) }
    {
    }
    Thread( const Thread& ) = delete;
    Thread( Thread&& ) = delete;
    Thread& operator=( const Thread& ) = delete;
    Thread& operator=( Thread&& ) = delete;

    ~Thread( )
    {
        stop_action_( );
        t_.join( );
    }

private:
    std::function< void( void ) > stop_action_;
    std::thread                   t_;
};

class LiveGenerator
{
public:
    LiveGenerator( std::vector< frame_utils::SimpleGenerator >& generators,
                   unsigned int                                 dcu_id,
                   pub_sub::Publisher&                          publisher )
        : generators_{ generators }, metadata_{ create_metadata_blob(
                                         generators, dcu_id ) },
          publisher_{ publisher }, dcu_id_{ dcu_id }
    {
    }

    void
    generate( bool force_metadata_push = false )
    {
        if ( cycle_ == 0 || force_metadata_push )
        {
            publisher_.publish( ( gps_ << 8 ) | ( cycle_ << 1 ) | 0x1,
                                metadata_ );
        }
        auto msg = create_data_blob( generators_, dcu_id_, gps_, cycle_ );
        publisher_.publish( ( gps_ << 8 ) | ( cycle_ << 1 ), msg );

        cycle_ = ( cycle_ + 1 ) % 16;
        gps_ = ( cycle_ == 0 ? gps_ + 1 : gps_ );
    }

private:
    std::vector< frame_utils::SimpleGenerator > generators_;
    pub_sub::Message                            metadata_;
    pub_sub::Publisher&                         publisher_;
    unsigned int                                dcu_id_;
    std::int64_t                                gps_{ 1000000000 };
    std::int64_t                                cycle_{ 0 };
};

void
wait_for_channels( daqd_stream::client& client, std::size_t min_channels = 0 )
{
    while ( client.channels( ).size( ) < min_channels )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }
}

std::vector< std::string >
pick_channels( const std::vector< frame_utils::SimpleGenerator >& channels1,
               const std::vector< frame_utils::SimpleGenerator >& channels2 )
{
    constexpr int              PICKS = 5;
    std::vector< std::string > picks{ };
    picks.reserve( 2 * PICKS );
    for ( auto i = 0; i < PICKS; ++i )
    {
        picks.emplace_back( channels1.at( i * 5 ).name( ) );
        picks.emplace_back( channels2.at( i * 5 ).name( ) );
    }
    return picks;
}

template < int N, typename F >
void
repeat_n_times( F&& f )
{
    for ( auto i = 0; i < N; ++i )
    {
        f( );
    }
}

template < int N, typename F >
void
repeat_n_times_index( F&& f )
{
    for ( auto i = 0; i < N; ++i )
    {
        f( i );
    }
}

TEST_CASE( "daqd_stream server can consume frames and serve its data" )
{
    std::atomic< bool >  done_{ false };
    frame_utils::TempDir tmp_dir{ };
    auto                 frame_generators =
        frame_utils::create_sample_channels( 100, "X1:CHAN-" );
    auto live_generators =
        frame_utils::create_sample_channels( 100, "X2:CHAN-" );

    std::map< std::string, frame_utils::SimpleGenerator* > generators{ };
    for ( auto i = 0; i < frame_generators.size( ); ++i )
    {
        generators.emplace( frame_generators[ i ].name( ),
                            &frame_generators[ i ] );
    }
    for ( auto i = 0; i < live_generators.size( ); ++i )
    {
        generators.emplace( live_generators[ i ].name( ),
                            &live_generators[ i ] );
    }

    auto channel_names = pick_channels( frame_generators, live_generators );

    frame_utils::frame_generator_config cfg{ };
    cfg.frame_root = tmp_dir.get( ).string( );
    cfg.verbose = false;
    frame_utils::frame_generator g( cfg, frame_generators );

    g.generate( );

    std::string frame_conn_str =
        std::string( "gwf://X1-TEST:" ) + tmp_dir.get( ).string( );
    daqd_stream::buffer_parameters params{ };
    params.seconds_in_buffer = 3;
    params.total_buffer_size = 400 * 1024 * 1024;

    auto frame_buffer_name = helpers::random_shm_name( );

    pub_sub::Publisher live_publisher;
    std::string        live_conn_str = "tcp://127.0.0.1:9020";
    live_publisher.add_destination( live_conn_str );

    auto live_buffer_name = helpers::random_shm_name( );

    LiveGenerator live_generator( live_generators, 42, live_publisher );

    helpers::cleanup_action  rm_shm( helpers::cleanup_shmem_action(
        { frame_buffer_name, live_buffer_name } ) );
    daqd_stream::daqd_stream frame_server(
        frame_conn_str, frame_buffer_name, params, { } );

    daqd_stream::daqd_stream live_server(
        live_conn_str, live_buffer_name, params, { } );

    Thread frame_server_loop_thread(
        [ &frame_server ]( ) { frame_server.run( ); },
        [ &frame_server ]( ) { frame_server.stop( ); } );
    Thread live_server_loop_thread(
        [ &live_server ]( ) { live_server.run( ); },
        [ &live_server ]( ) { live_server.stop( ); } );

    std::cerr << "about to get the client\n";
    auto client = daqd_stream::client::create( frame_buffer_name + "," +
                                               live_buffer_name );
    // wait to make sure some of the server is running, then start the live data
    wait_for_channels( *client, frame_generators.size( ) );
    // now publish 1s of data, forcing a lot of metadata pushes to ensure
    // the channel list gets picked up
    repeat_n_times< 16 >(
        [ &live_generator ]( ) { live_generator.generate( true ); } );
    wait_for_channels( *client,
                       frame_generators.size( ) + live_generators.size( ) );
    std::cerr << "got " << client->channels( ).size( ) << " channels\n";

    std::atomic< bool > wait{ false };

    // live_generator.generate(false);
    std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );
    while ( wait )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }

    auto plan = client->plan_request(
        daqd_stream::PLAN_TYPE::PLAN_16TH, channel_names, 0 );
    std::vector< char > buffer( plan.required_size( ) );

    daqd_stream::time_point  last_seen{ };
    daqd_stream::data_status status{ };
    // std::cerr << "about to get data\n";

    auto select_endianness = []( int cycle ) -> daqd_stream::byte_order {
        switch ( cycle % 3 )
        {
        case 0:
            return daqd_stream::byte_order::NATIVE;
        case 1:
            return daqd_stream::byte_order::BIG;
        case 2:
            return daqd_stream::byte_order::LITTLE;
        }
        return daqd_stream::byte_order::NATIVE;
    };

    auto expected_gps = 1000000000;
    auto expected_cycle = 15;

    auto                metadata = plan.metadata( );
    std::vector< char > test_buffer{ };
    for ( auto i = 0; i < 100; ++i )
    {
        auto byte_order = select_endianness( i );
        client->get_16th_data( plan,
                               last_seen,
                               buffer.data( ),
                               buffer.size( ),
                               status,
                               byte_order );
        //std::cout << i << " status.gps.seconds " << status.gps.seconds << ":" << status.gps.nano << ":" << status.cycle << "\n";
        last_seen = status.gps;
        REQUIRE( status.data_good );
        REQUIRE( status.gps.seconds == expected_gps );
        REQUIRE( status.cycle == expected_cycle );

        for ( const auto& cur_meta : metadata )
        {
            auto& generator = generators.at( cur_meta.name );
            auto  data_size = generator->bytes_per_16th( );
            test_buffer.resize( data_size );
            auto offset = cur_meta.location.data_offset;

            REQUIRE( generator->bytes_per_16th( ) ==
                     cur_meta.location.bytes_per_16th );
            generator->generate( (unsigned char*)test_buffer.data( ),
                                 expected_gps,
                                 expected_cycle );
            std::vector< daqd_stream::output_location > test_metadata{
                daqd_stream::output_location( generator->metadata( ), 0 )
            };
            daqd_stream::detail::endian_convert_in_place( test_buffer.data( ),
                                                          test_buffer.size( ),
                                                          test_metadata,
                                                          1,
                                                          byte_order );
            REQUIRE(
                std::equal( test_buffer.begin( ),
                            test_buffer.end( ),
                            buffer.data( ) + cur_meta.location.data_offset ) );
        }

        if ( status.cycle == 15 )
        {
            repeat_n_times< 16 >(
                [ &live_generator ]( ) { live_generator.generate( ); } );
            g.generate( );
        }
        expected_cycle = ( expected_cycle + 1 ) % 16;
        if ( expected_cycle == 0 )
        {
            ++expected_gps;
        }
    }

    plan = client->plan_request(
        daqd_stream::PLAN_TYPE::PLAN_SEC, channel_names, 1 );
    metadata = plan.metadata( );
    auto req_sec = 0;

    buffer.resize( plan.required_size( ) );

    daqd_stream::sec_data_status sec_status;

    for ( auto i = 0; i < 10; ++i )
    {
        auto byte_order = select_endianness( i );
        client->get_sec_data( plan,
                              req_sec,
                              1,
                              buffer.data( ),
                              buffer.size( ),
                              sec_status,
                              byte_order );
        // FIXME: how to deal with data_good
        // REQUIRE( sec_status.data_good );
        REQUIRE( sec_status.gps.seconds == expected_gps );

        for ( const auto& cur_meta : metadata )
        {
            auto& generator = generators.at( cur_meta.name );
            auto  data_size = generator->bytes_per_16th( ) * 16;
            test_buffer.resize( data_size );

            auto test_data = test_buffer.data( );
            repeat_n_times_index< 16 >(
                [ &test_data, &generator, expected_gps ]( int cycle ) {
                    test_data = (char*)generator->generate(
                        (unsigned char*)test_data, expected_gps, cycle );
                } );

            auto offset = cur_meta.location.data_offset;

            REQUIRE( generator->bytes_per_16th( ) ==
                     cur_meta.location.bytes_per_16th );

            std::vector< daqd_stream::output_location > test_buffer_shape{
                daqd_stream::output_location( generator->metadata( ), 0 )
            };
            daqd_stream::detail::endian_convert_in_place( test_buffer.data( ),
                                                          test_buffer.size( ),
                                                          test_buffer_shape,
                                                          16,
                                                          byte_order );

            REQUIRE(
                std::equal( test_buffer.begin( ),
                            test_buffer.end( ),
                            buffer.data( ) + cur_meta.location.data_offset ) );
        }

        ++expected_gps;
        req_sec = expected_gps;

        repeat_n_times< 16 >(
            [ &live_generator ]( ) { live_generator.generate( ); } );
        g.generate( );
    }
}