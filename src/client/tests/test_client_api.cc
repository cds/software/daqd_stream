//
// Created by jonathan.hanks on 6/14/21.
//
#include <daqd_stream/daqd_stream.hh>
#include "internal.hh"
#include "catch.hpp"

#include <memory>

#include "shmem_client.hh"

namespace
{
    void
    cleanup_dummy_shmem( daqd_stream::detail::Posix_shared_memory& dummy )
    {
        std::unique_ptr< char[] > data(
            reinterpret_cast< char* >( dummy.get( ) ) );
    }

    std::shared_ptr< daqd_stream::detail::Posix_shared_memory >
    create_simple_dummy_shmem( )
    {
        const std::size_t         buffer_size = 1024 * 1024 * 1024;
        std::unique_ptr< char[] > data( new char[ buffer_size ] );

        auto shmem =
            std::shared_ptr< daqd_stream::detail::Posix_shared_memory >(
                new daqd_stream::detail::Posix_shared_memory( data.get( ),
                                                              buffer_size ),
                []( daqd_stream::detail::Posix_shared_memory* p ) {
                    if ( p )
                    {
                        cleanup_dummy_shmem( *p );
                        delete p;
                    }
                } );

        auto* header =
            shmem->get_as< daqd_stream::detail::shared_mem_header >( );
        daqd_stream::detail::ifo_config tmp_config;
        tmp_config.channels.emplace_back(
            daqd_stream::online_channel{ "chan1",
                                         0,
                                         1,
                                         daqd_stream::DATA_TYPE::FLOAT32,
                                         16,
                                         4,
                                         0,
                                         1.0,
                                         1.0,
                                         0.0,
                                         "undef" } );
        tmp_config.channels.emplace_back(
            daqd_stream::online_channel{ "chan2",
                                         0,
                                         1,
                                         daqd_stream::DATA_TYPE::FLOAT32,
                                         16,
                                         4,
                                         4,
                                         1.0,
                                         1.0,
                                         0.0,
                                         "undef" } );
        std::fill( tmp_config.checksums.dcu_config.begin( ),
                   tmp_config.checksums.dcu_config.end( ),
                   0 );
        tmp_config.checksums.master_config = 42;
        tmp_config.checksums.dcu_config[ 1 ] = 6;

        header->set_config(
            header->construct< daqd_stream::detail::shared_ifo_config >(
                tmp_config,
                [ header ]( std::size_t channel_count )
                    -> daqd_stream::online_channel* {
                    return reinterpret_cast< daqd_stream::online_channel* >(
                        header->get_mem(
                            channel_count *
                            sizeof( daqd_stream::online_channel ) ) );
                } ) );
        data.release( );
        return shmem;
    }
} // namespace

TEST_CASE( "test client::config_hash()" )
{
    daqd_stream::detail::client_intl  intl( create_simple_dummy_shmem( ) );
    daqd_stream::detail::shmem_client client( std::move( intl ) );

    REQUIRE( client.config_checksum( ) == 42 );
}