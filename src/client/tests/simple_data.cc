//
// Created by jonathan.hanks on 2/7/22.
//
#include "simple_data.hh"
#include <advligorts/daq_metadata.h>
#include <advligorts/daq_core.h>
#include "crc.h"

pub_sub::Message
create_metadata_blob(
    const std::vector< frame_utils::SimpleGenerator >& generators,
    unsigned int                                       dcu_id )
{
    std::shared_ptr< unsigned char[] > blob{ nullptr };
    std::size_t req_size = sizeof( daq_multi_dcu_metadata_header_t ) +
        sizeof( daq_dcu_metadata_header_t ) +
        ( sizeof( daq_channel_metadata_t ) * generators.size( ) );
    auto data = new unsigned char[ req_size ];
    blob.reset( data );

    auto header = reinterpret_cast< daq_multi_dcu_metadata_header_t* >( data );
    header->magic = DAQ_METADATA_MULTI_DCU_MAGIC;
    header->version = 0;
    header->dcuCount = 1;
    header->dataBlockSize = sizeof( daq_dcu_metadata_header_t ) +
        ( generators.size( ) * sizeof( daq_channel_metadata_t ) );
    header->dataBlockChecksum = 0;

    auto dcu_header =
        reinterpret_cast< daq_dcu_metadata_header_t* >( header + 1 );
    dcu_header->dcuId = dcu_id;
    dcu_header->numChannels = generators.size( );
    dcu_header->checksum = 0;
    dcu_header->encodingMethod = METADATA_PLAIN;
    dcu_header->decodedSize =
        ( generators.size( ) * sizeof( daq_channel_metadata_t ) );
    dcu_header->encodedSize = dcu_header->decodedSize;
    dcu_header->encodedChecksum = 0;

    auto channels =
        reinterpret_cast< daq_channel_metadata_t* >( dcu_header + 1 );
    auto cur = channels;
    for ( const auto& cur_generator : generators )
    {
        auto chan = cur_generator.metadata( );
        chan.name.copy_to( cur->name );
        chan.units.copy_to( cur->units );
        cur->datatype = (int)chan.datatype;
        cur->datarate = chan.datarate;
        cur->test_point_number = 0;
        cur->signal_gain = chan.signal_gain;
        cur->signal_slope = chan.signal_slope;
        cur->signal_offset = chan.signal_offset;
        ++cur;
    }

    dcu_header->checksum = dcu_id;
    dcu_header->encodedChecksum =
        crc_len( crc_ptr( const_cast< char* >(
                              reinterpret_cast< const char* >( channels ) ),
                          dcu_header->encodedSize,
                          0 ),
                 dcu_header->encodedSize );

    return pub_sub::Message( std::move( blob ), req_size );
}

pub_sub::Message
create_data_blob( const std::vector< frame_utils::SimpleGenerator >& generators,
                  unsigned int                                       dcu_id,
                  std::int64_t                                       gps,
                  int                                                cycle )
{
    std::shared_ptr< unsigned char[] > blob{ nullptr };
    if ( generators.empty( ) )
    {
        throw std::runtime_error( "Empty generator list" );
    }
    auto data_size = generators.back( ).data_offset( ) +
        generators.back( ).bytes_per_16th( );
    auto req_size = sizeof( daq_multi_dcu_header_t ) + data_size;
    auto data = new unsigned char[ req_size ];
    blob.reset( data );

    auto header = reinterpret_cast< daq_multi_dcu_header_t* >( data );
    header->dcuTotalModels = 1;
    header->fullDataBlockSize = data_size;

    auto dcu_header = header->dcuheader;
    dcu_header->dcuId = dcu_id;
    dcu_header->fileCrc = dcu_id;
    dcu_header->status = 0;
    dcu_header->cycle = cycle;
    dcu_header->timeSec = static_cast< unsigned int >( gps );
    dcu_header->timeNSec = 62500000 * cycle;
    dcu_header->dataCrc = 0;
    dcu_header->dataBlockSize = data_size;
    dcu_header->tpBlockSize = 0;
    dcu_header->tpCount = 0;

    auto data_start = reinterpret_cast< unsigned char* >( header + 1 );
    auto cur = data_start;
    for ( const auto& cur_generator : generators )
    {
        cur = cur_generator.generate( cur, gps, cycle );
    }
    dcu_header->dataCrc =
        crc_len( crc_ptr( reinterpret_cast< char* >( data_start ),
                          dcu_header->dataBlockSize,
                          0 ),
                 dcu_header->dataBlockSize );
    // std::cerr << "publishing msg @ " << (void*)(blob.get()) << " dcuid " <<
    // dcu_header->dcuId << " " << dcu_header->timeNSec << ":" <<
    // dcu_header->cycle << "\n";
    return pub_sub::Message( std::move( blob ), req_size );
}