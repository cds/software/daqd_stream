/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include <daqd_stream/daqd_stream.hh>
#include "load_data.hh"
//#include "internal.hh"
//#include "endian_utility.hh"
//#include <algorithm>
//#include <iostream>

namespace daqd_stream
{
    void
    data_plan::load_data_in_sec( int                                 cycle,
                                 const detail::buffer_offset_helper& offsets,
                                 unsigned char    current_source,
                                 void*            dest_buffer,
                                 sec_data_status& status ) const
    {
        if ( status.channel_status.size( ) < channels_.size( ) )
        {
            throw std::runtime_error( "status buffer not initialized" );
        }
        if ( !dest_buffer )
        {
            throw std::runtime_error( "Invalid buffers passed to load_data" );
        }
        if ( cycle < 0 ||
             cycle > seconds_in_buffer_ * detail::cycles_per_sec( ) )
        {
            throw std::range_error( "Loading into an invalid cycle" );
        }

        auto  status_it = status.channel_status.begin( );
        char* dest = reinterpret_cast< char* >( dest_buffer );

        std::array< std::uint32_t, detail::max_dcu( ) > dcu_status{ };
        dcu_status[ 0 ] = 0xbad;
        for ( const auto& config_entry : dcu_configs_ )
        {
            // FIXME: indexing
            auto cur_status = offsets.status( config_entry.identifier.dcuid );
            if ( cur_status != 0xbad )
            {
                // FIXME: indexing
                if ( config_entry.config !=
                     offsets.config( config_entry.identifier.dcuid ) )
                {
                    status.plan_status = PLAN_STATUS::UPDATE_PLAN;
                    cur_status = 0x2000;
                }
            }
            // FIXME: indexing
            dcu_status[ config_entry.identifier.dcuid ] = cur_status;
        }

        auto i = 0;
        auto using_dcu0 = false;
        std::for_each(
            segments_.begin( ),
            segments_.end( ),
            [ &i,
              &dest,
              &offsets,
              &status_it,
              cycle,
              &dcu_status,
              &using_dcu0,
              current_source,
              this ]( const dcu_span& span ) {
                if ( span.source != current_source )
                {
                    ++i;
                    ++status_it;
                    return;
                }
                auto                          segment_output_offset =
                    offsets.dest_information->operator[]( i ).data_offset;
                auto output = dest + segment_output_offset;

                output += cycle * span.length;
                if ( span.dcu != 0 && dcu_status[ span.dcu ] != 0x2000 &&
                     dcu_status[ span.dcu ] != 0xbad )
                {
                    const volatile char* start =
                        offsets.buffer( span.dcu ) + span.offset;
                    const volatile char* end = start + span.length;
                    std::copy( start, end, output );
                }
                else
                {
                    using_dcu0 = true;
                    std::fill( output, output + span.length, 0 );
                }
                ( *status_it )[ cycle ] = offsets.status( span.dcu );
                ++status_it;
                ++i;
            } );
        if ( using_dcu0 && global_config_checksum_ != offsets.global_config )
        {
            status.plan_status = PLAN_STATUS::UPDATE_PLAN;
        }
    }

    void
    data_plan::zero_fill_16th( std::int64_t  cycle_offset,
                               unsigned char current_source,
                               void*         dest_buffer,
                               const std::vector< output_location >& locations,
                               data_status& status_buf ) const
    {
        for ( auto i = 0; i < segments_.size( ); ++i )
        {
            const auto& cur_span = segments_[ i ];

            if ( cur_span.source == current_source )
            {
                const auto& cur_location = locations[ i ];
                auto dest = reinterpret_cast< unsigned char* >( dest_buffer ) +
                    cur_location.data_offset +
                    ( cycle_offset * cur_location.bytes_per_16th );
                std::fill( dest,
                           dest +
                               cur_location.bytes_per_16th *
                                   detail::cycles_per_sec( ),
                           0 );
                status_buf.channel_status[ i ] |= 0xbad;
            }
        }
    }

    void
    data_plan::zero_fill_16th( std::int64_t  cycle_offset,
                               int           cycle,
                               unsigned char current_source,
                               void*         dest_buffer,
                               const std::vector< output_location >& locations,
                               sec_data_status& status_buf ) const
    {
        for ( auto i = 0; i < segments_.size( ); ++i )
        {
            const auto& cur_span = segments_[ i ];

            if ( cur_span.source == current_source )
            {
                const auto& cur_location = locations[ i ];
                auto dest = reinterpret_cast< unsigned char* >( dest_buffer ) +
                    cur_location.data_offset +
                    ( cycle_offset * cur_location.bytes_per_16th );
                std::fill( dest,
                           dest +
                               cur_location.bytes_per_16th *
                                   detail::cycles_per_sec( ),
                           0 );
                status_buf.channel_status[ i ][ cycle ] |= 0xbad;
            }
        }
    }
} // namespace daqd_stream
