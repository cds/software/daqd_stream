//
// Created by jonathan.hanks on 10/11/21.
//

#ifndef DAQD_STREAM_DAQ_METADATA_HH
#define DAQD_STREAM_DAQ_METADATA_HH

#ifdef __cplusplus

#include <type_traits>

extern "C" {
#endif

#define DAQ_METADATA_MULTI_DCU_MAGIC 0xffeeddcc

/**
 * @brief Enum to define how the channel data is encoded
 */
enum DAQ_METADATA_ENCODING
{
    METADATA_PLAIN = 0,
    METADATA_ZSTD = 1,
};

/**
 * @brief Metadata for a channel.
 * @note This does not include dcu ids and data block offsets
 * this is provided either in higher level structures or
 * is implicit in ordering.
 */
typedef struct daq_channel_metadata_t
{
    char         name[ 60 ];
    char         units[ 40 ];
    int          datatype;
    int          datarate;
    unsigned int test_point_number;
    float        signal_gain;
    float        signal_slope;
    float        signal_offset;
} daq_channel_metadata_t;

/**
 * @brief Metadata for a dcu.
 * @brief this if followed by a datablock of encodedSize bytes.  This holds
 * the channel list.
 */
typedef struct daq_dcu_metadata_header_t
{
    unsigned int dcuId;
    unsigned int numChannels;
    unsigned int checksum;
    unsigned int encodingMethod;
    unsigned int decodedSize;
    unsigned int encodedSize;
    unsigned int encodedChecksum;
} daq_dcu_metadata_header_t;

/**
 * @brief Metadata for a group of 1 or more DCUs
 */
typedef struct daq_multi_dcu_metadata_header_t
{
    unsigned int magic; /// DAQ_METADATA_MULTI_DCU_MAGIC
    unsigned int version; /// 0
    unsigned int dcuCount;
    unsigned int dataBlockSize; /// size of the associated datablock
    unsigned int dataBlockChecksum;
    // unsigned int totalDataSize;
} daq_multi_dcu_metadata_header_t;

/**
 * @brief Metadata header for 1 or more DCUs + associated channel lists
 * @note The actual size of the block is sizeof(header) + header.dataBlockSize
 * @note Data is arranged as follows
 *
 * header
 *  for each dcu
 *    daq_dcu_metadata_header_t
 *    data block
 */
typedef struct daq_multi_dcu_metadata_t
{
    daq_multi_dcu_metadata_header_t header;
    char                            data[ 100 * 1024 * 1024 ];
} daq_multi_dcu_metadata_t;

#ifdef __cplusplus
static_assert( sizeof( daq_dcu_metadata_header_t ) == 28, "size mismatch" );
static_assert( sizeof( daq_channel_metadata_t ) == 60 + 40 + ( 6 * 4 ),
               "size mismatch" );
static_assert( sizeof( daq_channel_metadata_t[ 2 ] ) ==
                   2 * ( 60 + 40 + ( 6 * 4 ) ),
               "size mismatch" );
}
#endif

#endif // DAQD_STREAM_DAQ_METADATA_HH
