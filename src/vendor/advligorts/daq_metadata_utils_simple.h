//
// Created by jonathan.hanks on 11/1/21.
//

#ifndef DAQD_STREAM_DAQ_METADATA_UTILS_SIMPLE_H
#define DAQD_STREAM_DAQ_METADATA_UTILS_SIMPLE_H

#include "daq_metadata.h"

#ifdef __cplusplus
extern "C" {
#endif

static inline daq_multi_dcu_metadata_t*
daq_metadata_simple_builder( int          dcuid,
                             int          config_crc,
                             unsigned int num_channels,
                             void*        user_data,
                             void ( *channel_cb )( int,
                                                   daq_channel_metadata_t*,
                                                   void* ),
                             void* ( *allocator )( size_t ),
                             long ( *crc_func )( void*, size_t ) )
{
    daq_multi_dcu_metadata_t*  metadata = 0;
    daq_dcu_metadata_header_t* dcu_header = 0;
    daq_channel_metadata_t*    cur_channel = 0;
    int                        i = 0;
    size_t data_size = ( sizeof( daq_channel_metadata_t ) * num_channels );
    size_t data_block_size = sizeof( daq_dcu_metadata_header_t ) + data_size;

    void* data =
        allocator( sizeof( daq_multi_dcu_metadata_t ) + data_block_size );
    if ( !data )
    {
        return (daq_multi_dcu_metadata_t*)0;
    }
    metadata = (daq_multi_dcu_metadata_t*)data;
    metadata->header.dcuCount = 1;
    metadata->header.dataBlockSize = data_block_size;
    metadata->header.version = 0;
    metadata->header.magic = DAQ_METADATA_MULTI_DCU_MAGIC;
    dcu_header = (daq_dcu_metadata_header_t*)( &metadata->data[ 0 ] );
    dcu_header->dcuId = dcuid;
    dcu_header->checksum = config_crc;
    dcu_header->encodedSize = dcu_header->decodedSize = data_size;
    dcu_header->numChannels = num_channels;
    dcu_header->encodingMethod = METADATA_PLAIN;
    dcu_header->encodedChecksum = 0;
    cur_channel = (daq_channel_metadata_t*)( dcu_header + 1 );
    for ( i = 0; i < num_channels; ++i )
    {
        channel_cb( i, cur_channel, user_data );
        ++cur_channel;
    }
    if ( crc_func )
    {
        dcu_header->encodedChecksum = crc_func(
            (void*)( (daq_channel_metadata_t*)( dcu_header + 1 ) ), data_size );
    }
    return metadata;
}

#ifdef __cplusplus
}
#endif

#endif // DAQD_STREAM_DAQ_METADATA_UTILS_SIMPLE_H
