//
// Created by jonathan.hanks on 9/23/21.
//

#ifndef DAQD_STREAM_FRAME_PLUGIN_INTERNAL_HH
#define DAQD_STREAM_FRAME_PLUGIN_INTERNAL_HH

#include <cstdint>
#include <string>
#include <type_traits>

#include <boost/filesystem.hpp>
#include <boost/optional.hpp>

#include <cds-pubsub/sub_plugin.hh>

#include "data_types.hh"

namespace daqd_stream
{
    namespace plugins
    {
        namespace frame_sub
        {

            struct frame_info
            {
                std::int64_t gps_start{ 0 };
                std::int64_t gps_stop{ 0 };
                std::string  frame_type{ };
            };

            struct info_and_path
            {
                frame_info              info;
                boost::filesystem::path path;
            };

            template < typename Result, typename StringLike >
            Result
            to_pos_int( const StringLike& input )
            {
                static_assert( std::is_integral< Result >::value,
                               "Type must be an integral type" );
                Result val{ 0 };
                for ( const auto ch : input )
                {
                    if ( ch < '0' || ch > '9' )
                    {
                        return 0;
                    }
                    val = ( val * 10 ) + ( ch - '0' );
                }
                return val;
            }

            boost::optional< frame_info >
            parse_frame_name( const std::string& name );

            void process_frame( const info_and_path& info,
                                pub_sub::SubHandler& handler,
                                pub_sub::SubId       sub_id );

            info_and_path
            find_next_frame( std::int64_t last_seen, const boost::filesystem::path& frame_path, const std::string& frame_type );
        } // namespace frame_sub
    } // namespace plugins
} // namespace daqd_stream

#endif // DAQD_STREAM_FRAME_PLUGIN_INTERNAL_HH
