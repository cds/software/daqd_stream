//
// Created by jonathan.hanks on 4/12/23.
//
#include "frame_plugin_internal.hh"
#include "frame_utils.hh"
#include <sstream>

#include "catch.hpp"

void
create_file(const boost::filesystem::path& dir, const std::string& name)
{
    auto fname = dir / name;
    boost::filesystem::ofstream out(fname);
    out << "hello";
}

TEST_CASE( "test find_next_frame" )
{
    using namespace daqd_stream::plugins::frame_sub;
    frame_utils::TempDir temp_dir{};

    auto root_path = temp_dir.get();

    auto touch = [&root_path](const std::string gps) {
        std::ostringstream os{};
        os << "X-X1_R-" << gps << "-1.gwf";
        create_file(root_path, os.str());
    };

    auto info = find_next_frame(0, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 0 );

    touch("1000000000");
    info = find_next_frame(0, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000000);

    touch("1000000001");
    touch("1000000002");
    touch("1000000003");
    info = find_next_frame(1000000000, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000001);
    info = find_next_frame(1000000001, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000002);
    info = find_next_frame(1000000002, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000003);

    info = find_next_frame(0, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000003);

    info = find_next_frame(1000000003, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 0);

    touch("1000000004");
    touch("1000000005");
    touch("1000000006");

    info = find_next_frame(1000000003, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000004);
    info = find_next_frame(1000000004, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000005);
    info = find_next_frame(1000000005, root_path, "X-X1_R");
    REQUIRE( info.info.gps_start == 1000000006);
}