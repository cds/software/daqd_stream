//
// Created by jonathan.hanks on 2/8/22.
//
#include "frame_utils.hh"

namespace frame_utils
{
    std::vector< SimpleGenerator >
    create_sample_channels( int count, std::string channel_prefix )
    {
        std::vector< SimpleGenerator > generators{ };
        generators.reserve( count );

        OffsetMap offsets{ };
        for ( auto i = 0; i < count; ++i )
        {
            std::ostringstream os;
            os << channel_prefix << i;
            generators.emplace_back( os.str( ), 1, i, offsets );
        }
        return generators;
    }

    std::vector< daqd_stream::online_channel >
    to_channels( const std::vector< SimpleGenerator >& input )
    {
        std::vector< daqd_stream::online_channel > output{ };
        output.reserve( input.size( ) );
        std::transform(
            input.begin( ),
            input.end( ),
            std::back_inserter( output ),
            []( const SimpleGenerator& gen ) -> daqd_stream::online_channel {
                return gen.metadata( );
            } );
        return output;
    }

    void
    generate_one_second( const std::vector< SimpleGenerator >& generators,
                         std::vector< DataPtr >&               outputs,
                         std::int64_t                          gps_s )
    {
        if ( generators.size( ) != outputs.size( ) )
        {
            throw std::runtime_error( "generator -> output count mismatch" );
        }
        for ( auto i = 0; i < generators.size( ); ++i )
        {
            auto* cur = outputs[ i ].first;
            for ( auto cycle = 0; cycle < 16; ++cycle )
            {
                cur = generators[ i ].generate( cur, gps_s, cycle );
            }
        }
    }

    void
    generate_16th( const std::vector< SimpleGenerator >& generators,
                   std::int64_t                          gps_s,
                   int                                   cycle,
                   unsigned char*                        dest,
                   std::size_t                           dest_size )
    {
        if ( generators.empty( ) )
        {
            return;
        }
        if ( ( generators.back( ).data_offset( ) +
               generators.back( ).bytes_per_16th( ) ) > dest_size )
        {
            throw std::runtime_error( "the destination buffer is too small" );
        }
        for ( const auto& cur_generator : generators )
        {
            dest = cur_generator.generate( dest, gps_s, cycle );
        }
    }

    std::size_t
    data_size( daqd_stream::DATA_TYPE data_type )
    {
        using DT = daqd_stream::DATA_TYPE;
        switch ( data_type )
        {
        case DT::UNDEFINED:
        default:
            return 0;
        case DT::INT16:
            return 2;
        case DT::INT32:
        case DT::FLOAT32:
        case DT::UINT32:
            return 4;
        case DT::INT64:
        case DT::FLOAT64:
        case DT::COMPLEX32:
            return 8;
        }
    }

    std::string
    create_path( const std::string& directory,
                 const std::string& prefix,
                 std::uint64_t      start,
                 std::uint64_t      period )
    {
        std::ostringstream os{ };
        os << directory << "/" << prefix << "-" << start << "-" << period
           << ".gwf";
        return os.str( );
    }

    FrameHPtr
    CreateFrameH( int frame_period )
    {
        auto frame = boost::make_shared< FrameCPP::Version::FrameH >(
            "LIGO",
            0, // configuration/run number
            1, // frame number,
            FrameCPP::Version::GPSTime( 0, 0 ),
            0,
            frame_period );

        frame->SetRawData(
            boost::make_shared< FrameCPP::Version::FrRawData >( ) );
        frame->RefHistory( ).append( FrameCPP::Version::FrHistory(
            "",
            0,
            "framebuilder, framecpp-" + std::string( FRAMECPP_VERSION ) ) );

        return frame;
    }

    std::vector< DataPtr >
    AssociateDataPtrs(
        FrameCPP::Version::FrameH&                        frame,
        int                                               frame_period,
        const std::vector< daqd_stream::online_channel >& channels )
    {
        using DT = daqd_stream::DATA_TYPE;

        std::vector< DataPtr > ptrs{ };
        ptrs.reserve( channels.size( ) );

        std::vector< INT_2S > aux_data_tmp{ };
        aux_data_tmp.resize( 16 * frame_period );

        auto i = 0;
        for ( const auto& cur_channel : channels )
        {
            FrameCPP::Version::FrAdcData adc(
                cur_channel.name.data( ),
                0 /* group number */,
                i /* channel number */,
                data_size( cur_channel.datatype ) * CHAR_BIT /* num bits */,
                cur_channel.datarate,
                cur_channel.signal_offset,
                cur_channel.signal_slope,
                cur_channel.units.data( ),
                /* from daqd.cc */
                ( cur_channel.datatype == daqd_stream::DATA_TYPE::COMPLEX32
                      ? cur_channel.signal_gain
                      : 0. ) /* freq shift */,
                0 /* time offset */,
                0 /* data valid */,
                .0 /* heterodyning phase in radians */
            );
            if ( cur_channel.datarate > 16 )
            {
                FrameCPP::Version::Dimension aux_dims(
                    16 * frame_period, 1. / 16, "" );
                FrameCPP::Version::FrVect aux_vect(
                    "dataValid",
                    1,
                    &aux_dims,
                    /* this is copied, we just need something */
                    aux_data_tmp.data( ),
                    "" );
                adc.RefAux( ).append( aux_vect );
            }

            std::unique_ptr< FrameCPP::Version::FrVect > vect{ nullptr };
            switch ( cur_channel.datatype )
            {
            case DT::COMPLEX32:
                vect = NewFrVect< COMPLEX_8 >( cur_channel, frame_period );
                break;
            case DT::FLOAT64:
                vect = NewFrVect< REAL_8 >( cur_channel, frame_period );
                break;
            case DT::FLOAT32:
                vect = NewFrVect< REAL_4 >( cur_channel, frame_period );
                break;
            case DT::INT32:
                vect = NewFrVect< INT_4S >( cur_channel, frame_period );
                break;
            case DT::UINT32:
                vect = NewFrVect< INT_4U >( cur_channel, frame_period );
                break;
            case DT::INT64:
                vect = NewFrVect< INT_8S >( cur_channel, frame_period );
                break;
            case DT::INT16:
                vect = NewFrVect< INT_2S >( cur_channel, frame_period );
                break;
            default:
                throw std::runtime_error( "Unknown data type" );
            }
            adc.RefData( ).append( *vect );
            frame.GetRawData( )->RefFirstAdc( ).append( adc );

            /* now extract the data vector for quick access */
            boost::shared_ptr< FrameCPP::Version::FrAdcData > curAdc =
                frame.GetRawData( )->RefFirstAdc( )[ i ];
            unsigned char* data_ptr =
                curAdc->RefData( )[ 0 ]->GetData( ).get( );
            INT_2U* aux_ptr = nullptr;
            if ( cur_channel.datarate > 16 )
            {
                aux_ptr =
                    (INT_2U*)( curAdc->RefAux( )[ 0 ]->GetData( ).get( ) );
            }
            ptrs.emplace_back( std::make_pair( data_ptr, aux_ptr ) );
            ++i;
        }
        return ptrs;
    }
} // namespace frame_utils