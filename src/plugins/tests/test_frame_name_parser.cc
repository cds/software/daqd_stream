//
// Created by jonathan.hanks on 9/23/21.
//
#include <iostream>
#include "frame_plugin_internal.hh"
#include "catch.hpp"

TEST_CASE( "Test parsing ints" )
{
    struct TestCase
    {
        std::string  input;
        std::int64_t output;
    };
    TestCase test_cases[] = {
        { "1", 1 },       { "0", 0 },     { "42", 42 },
        { "a42", 0 },     { "", 0 },      { "1235a", 0 },
        { "1235", 1235 }, { "-1235", 0 }, { "+1235", 0 },
    };
    for ( const auto test_case : test_cases )
    {
        REQUIRE( daqd_stream::plugins::frame_sub::to_pos_int< std::int64_t >(
                     test_case.input ) == test_case.output );
    }
}

TEST_CASE( "Test parsing frame filenames" )
{
    struct GoodTestCase
    {
        std::string                                 input;
        daqd_stream::plugins::frame_sub::frame_info info;
    };

    struct BadTestCase
    {
        std::string input;
    };

    GoodTestCase good_test_cases[] = {
        { "H-H1_R-1000000000-1.gwf", { 1000000000, 1000000001, "H-H1_R" } },
        { "H-DASH-DASH-H1_R-1300000000-42.gwf",
          { 1300000000, 1300000042, "H-DASH-DASH-H1_R" } },
        { "L-L1_TST-1-1024.gwf", { 1, 1025, "L-L1_TST" } },
    };

    for ( const auto& test_case : good_test_cases )
    {
        auto out = daqd_stream::plugins::frame_sub::parse_frame_name(
            test_case.input );
        REQUIRE( static_cast< bool >( out ) );
        daqd_stream::plugins::frame_sub::frame_info info = out.value( );
        REQUIRE( info.frame_type == test_case.info.frame_type );
        REQUIRE( info.gps_start == test_case.info.gps_start );
        REQUIRE( info.gps_stop == test_case.info.gps_stop );
    }

    BadTestCase bad_test_cases[] = {
        "abcdefg",
        "",
        "H-H1_R-1000a999-5.gwf",
        "H-H1_R-1000999-a5.gwf",
        "H-H1_R-1000999-5a.gwf",
        "H-H1_R-1000000000-4",
        "H-H1_R-1000000000-0.gwf",
        "H-H1_R-0-0.gwf",
        "H-H1_R-0-4.gwf",
        "H-H1_R-0.gwf",
        "H-H1_R.gwf",
    };
    for ( const auto& test_case : bad_test_cases )
    {
        auto out = daqd_stream::plugins::frame_sub::parse_frame_name(
            test_case.input );
        REQUIRE_FALSE( static_cast< bool >( out ) );
    }
}