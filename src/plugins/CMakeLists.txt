add_library(daqd_stream_plugins SHARED
        frame_plugin_internal.cc
        sub_frame_plugin.cc
        sub_delay_filter_plugin.cc
        sub_dump_filter_plugin.cc)
target_link_libraries(daqd_stream_plugins PRIVATE
        daqd_stream_common
        vendor)
target_link_libraries(daqd_stream_plugins PUBLIC
        cds::pub_sub
        daqd_stream_core
        Boost::filesystem
        Boost::boost
        ldastools::framecpp)
target_include_directories(daqd_stream_plugins PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/>)
target_include_directories(daqd_stream_plugins PRIVATE
        "${CMAKE_CURRENT_SOURCE_DIR}/private")

install(TARGETS daqd_stream_plugins
        EXPORT daqd_stream_install
        DESTINATION lib)

install(FILES
        include/daqd_stream/plugins/sub_delay_filter_plugin.hh
        include/daqd_stream/plugins/sub_frame_plugin.hh
        include/daqd_stream/plugins/sub_dump_filter_plugin.hh
        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/daqd_stream/plugins")

add_library(frame_utils SHARED
        tests/frame_utils.cc)
target_include_directories(frame_utils PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/tests")
target_link_libraries(frame_utils PUBLIC
        daqd_stream_client
        ldastools::framecpp
        Boost::boost
        Boost::filesystem)

add_executable(test_frame_plugin_unittests
        tests/test_main.cc
        tests/create_test_frame.cc
        tests/test_frame_name_parser.cc
        tests/test_find_next_frame.cc)
target_include_directories(test_frame_plugin_unittests PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/private)
target_link_libraries(test_frame_plugin_unittests PRIVATE
        vendor
        frame_utils
        daqd_stream_plugins
        daqd_stream_client
        daqd_stream_common
        catch2)
add_test(NAME "test_frame_plugin_unittests"
        COMMAND test_frame_plugin_unittests
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

add_executable(simple_frame_generator
        tests/simple_frame_generator.cc)
target_include_directories(simple_frame_generator PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/tests")
target_link_libraries(simple_frame_generator PRIVATE
        frame_utils)