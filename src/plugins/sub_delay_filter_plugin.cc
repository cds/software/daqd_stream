//
// Created by jonathan.hanks on 11/4/21.
//
#include <daqd_stream/plugins/sub_delay_filter_plugin.hh>

#include <deque>
#include <sstream>

namespace plugins
{
    namespace delay_filter
    {
        class DelayFilter : public pub_sub::plugins::Subscription
        {
        public:
            DelayFilter( unsigned int delay, pub_sub::SubHandler handler )
                : pub_sub::plugins::Subscription( ), queue_{ },
                  handler_{ std::move( handler ) }, delay_{ delay }
            {
            }
            ~DelayFilter( ) override = default;

            void
            filter( pub_sub::SubMessage msg ) override
            {
                queue_.push_front( std::move( msg ) );
                if ( queue_.size( ) >= delay_ )
                {
                    msg = queue_.back( );
                    queue_.pop_back( );
                    handler_( std::move( msg ) );
                }
            }

        private:
            std::deque< pub_sub::SubMessage > queue_;
            unsigned int                      delay_;
            pub_sub::SubHandler               handler_;
        };

        class DelayFilterSubPlugin
            : public pub_sub::plugins::SubscriptionPluginApi
        {
        public:
            ~DelayFilterSubPlugin( ) override = default;
            const std::string&
            prefix( ) const override
            {
                static const std::string my_prefix{ "delay://" };
                return my_prefix;
            }
            const std::string&
            version( ) const override
            {
                static const std::string my_version{ "0" };
                return my_version;
            }
            const std::string&
            name( ) const override
            {
                static const std::string my_name{ "Delay filter" };
                return my_name;
            }
            bool
            is_source( ) const override
            {
                return false;
            }
            std::shared_ptr< pub_sub::plugins::Subscription >
            subscribe( const std::string&        conn_str,
                       pub_sub::SubDebugNotices& debug_hooks,
                       pub_sub::SubHandler       handler ) override
            {
                if ( conn_str.find( prefix( ) ) != 0 )
                {
                    throw std::runtime_error( "Invalid subscription type "
                                              "passed to the delay filter" );
                }
                std::istringstream is( conn_str.substr( prefix( ).size( ) ) );
                unsigned int       delay{ 0 };
                is >> delay;
                return std::make_shared< DelayFilter >( delay,
                                                        std::move( handler ) );
            }
        };
    } // namespace delay_filter

    std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
    get_delay_filter_sub_plugin( )
    {
        return std::make_shared<
            plugins::delay_filter::DelayFilterSubPlugin >( );
    }
} // namespace plugins
