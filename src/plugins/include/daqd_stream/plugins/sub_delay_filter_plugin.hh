//
// Created by jonathan.hanks on 11/4/21.
//

#ifndef DAQD_STREAM_SUB_DELAY_FILTER_PLUGIN_HH
#define DAQD_STREAM_SUB_DELAY_FILTER_PLUGIN_HH

#include <cds-pubsub/sub_plugin.hh>

namespace plugins
{
    std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
    get_delay_filter_sub_plugin( );
}

#endif // DAQD_STREAM_SUB_DELAY_FILTER_PLUGIN_HH
