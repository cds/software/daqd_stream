//
// Created by jonathan.hanks on 10/3/22.
//
#include <cstdio>
#include <deque>
#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <sys/types.h>
#include <unistd.h>

#include <daqd_stream/plugins/sub_dump_filter_plugin.hh>
#include <advligorts/daq_core.h>

namespace plugins
{
    namespace dump_filter
    {

        class DumpFilter : public pub_sub::plugins::Subscription
        {
        public:
            DumpFilter( std::string root_path, pub_sub::SubHandler handler ): path_{std::move(root_path)}, handler_{std::move(handler)}
            {
                std::cout << "Creating dump filter with output to " << path_ << "\n";
            }
            ~DumpFilter() override = default;

            void
            filter ( pub_sub::SubMessage msg ) override
            {
                handler_( msg );

                if ( msg.size( ) < sizeof( daq_multi_dcu_header_t ) )
                {
                    return;
                }
                auto* header = reinterpret_cast<const daq_multi_dcu_header_t*>(msg.data());
                if (header->dcuTotalModels == 0)
                {
                    return;
                }
                auto gps = header->dcuheader[0].timeSec;
                auto cycle = header->dcuheader[0].cycle;

                std::ostringstream os{};
                os << path_ << gps << "_" << cycle << ".bin";
                std::string fname = os.str();

                dump_files_.push_back(fname);

                std::ofstream out(fname, std::ios::binary);
                out.write(reinterpret_cast<const char*>(msg.data()), msg.size());

                while (dump_files_.size() > 5*16)
                {
                    std::string old_fname = dump_files_.front();
                    dump_files_.pop_front();
                    std::remove(old_fname.c_str());
                }
            }
        private:
            pub_sub::SubHandler handler_;
            std::string path_;
            std::deque<std::string> dump_files_{};
        };

        class DumpFilterSubPlugin
            : public pub_sub::plugins::SubscriptionPluginApi
        {
        public:
            ~DumpFilterSubPlugin( ) override = default;
            const std::string&
            prefix( ) const override
            {
                static const std::string my_prefix{ "dump://" };
                return my_prefix;
            }
            const std::string&
            version( ) const override
            {
                static const std::string my_version{ "0" };
                return my_version;
            }
            const std::string&
            name( ) const override
            {
                static const std::string my_name{ "Dump filter" };
                return my_name;
            }
            bool
            is_source( ) const override
            {
                return false;
            }
            std::shared_ptr< pub_sub::plugins::Subscription >
            subscribe( const std::string&        conn_str,
                       pub_sub::SubDebugNotices& debug_hooks,
                       pub_sub::SubHandler       handler ) override
            {
                if ( conn_str.find( prefix( ) ) != 0 )
                {
                    throw std::runtime_error( "Invalid subscription type "
                                              "passed to the dump filter" );
                }
                auto root = conn_str.substr( prefix( ).size( ) );
                check_for_valid_root(root);
                return std::make_shared< DumpFilter >( root,
                                                        std::move( handler ) );
            }

            void
            check_for_valid_root(const std::string& root)
            {
                if (root.empty() || root.front() != '/' || root.back() != '/')
                {
                    throw std::runtime_error("Invalid dump root, it must be non empty and start and end with '/");
                }
                if (root.find("..") != std::string::npos)
                {
                    throw std::runtime_error("Invalid dump root, no \"..\" allowed");
                }
                auto valid_prefixes = std::vector<std::string>{"/dev/shm/", "/tmp/"};
                {
                    std::stringstream os{};
                    os << "/var/run/user/" << getpid() << "/";
                    valid_prefixes.emplace_back(os.str());
                }
                for (const auto& prefix:valid_prefixes)
                {
                    if (root.find(prefix) == 0 && root.size() > prefix.size())
                    {
                        return;
                    }
                }
                std::cout << root << "\n" << valid_prefixes.back() << "\n";
                throw std::runtime_error("Dumps must be a directory under /dev/shm/ or /tmp/ or your /var/run/.../ dir");
            }
        };
    }

    std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
    get_dump_filter_sub_plugin( )
    {
        return std::make_shared<
            plugins::dump_filter::DumpFilterSubPlugin >( );
    }
}