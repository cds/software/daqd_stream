/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include "daqd_stream_intl.hh"

#include <chrono>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <thread>
#include <utility>

#include <daqd_stream/plugins/sub_frame_plugin.hh>

#include <advligorts/daq_metadata.h>
#include <advligorts/daq_core.h>

#include "internal.hh"
#include "ini_parser.hh"
#include "remote_meta_data.hh"
#include "message_queue.hh"
#include "msg_to_ifo_config.hh"
#include "sub_cds_metadata_plugin.hh"
#include "multi_source_info_block.hh"
#include "wait_group.hh"

#include "shared_types.hh"

namespace daqd_stream
{
    namespace detail
    {
        static_assert( max_dcu( ) == DAQ_TRANSIT_MAX_DCU,
                       "detail::max_dcu() must match DAQ_TRANSIT_MAX_DCU!" );

        /**
         * @brief Check to see if the requested input data is in the input
         * buffer
         * @param header pointer to the input block header
         * @param the cycle you are checking data for
         * @param max_data_size the max size of the input buffer
         * @return 0 if safe, != 0 if overflow
         */
        int
        data_will_overflow( volatile daq_multi_cycle_header_t* header,
                            int                                cycle,
                            int                                max_data_size )
        {
            return ( ( ( cycle + 1 ) * header->cycleDataSize ) >
                     max_data_size );
        }

        bool
        retrieve_meta_data( const std::string& meta_data_source,
                            dcu_metadata_cb    per_dcu_callback )
        {
            if ( meta_data_source.find( "ini://" ) == 0 )
            {
                return parse_master_file( meta_data_source.substr( 6 ),
                                          per_dcu_callback );
            }
            if ( meta_data_source.find( "cds://" ) == 0 )
            {
                auto        source = meta_data_source.substr( 6 );
                int         port = 9010;
                std::string hostname;
                auto        idx = source.find( ':' );
                if ( idx == std::string::npos )
                {
                    hostname = source;
                }
                else
                {
                    hostname = source.substr( 0, idx );
                    std::istringstream port_stream( source.substr( idx + 1 ) );
                    port_stream >> port;
                }
                return retrieve_remote_meta_data(
                    hostname, port, per_dcu_callback );
            }
            throw std::runtime_error( "unknown metadata source" );
        }

        void
        load_meta_data( shared_mem_header* header,
                        const ifo_config&  new_ifo_config )
        {
            auto required_size =
                sizeof( online_channel ) * new_ifo_config.channels.size( ) +
                sizeof( shared_ifo_config );

            if ( header->remaining_memory( ) < required_size )
            {
                header->reset_dynamic_ram( );
            }

            header->set_config( header->construct< shared_ifo_config >(
                new_ifo_config,
                [ header ]( std::size_t channel_count ) -> online_channel* {
                    return reinterpret_cast< online_channel* >( header->get_mem(
                        channel_count * sizeof( online_channel ) ) );
                } ) );
        }

        daqd_stream_instance::daqd_stream_instance( // std::string
                                                    // meta_data_source,
            std::string              data_source,
            const std::string&       shmem_name,
            buffer_parameters        parameters,
            daqd_stream::plugin_list additional_plugins )
            : output_{ create_shared_memory(
                  shmem_name,
                  validate_shmem_size( parameters.total_buffer_size,
                                       parameters.seconds_in_buffer ),
                  parameters.permissions ) },
              // meta_data_source_{ std::move( meta_data_source ) },
              data_source_str_{ std::move( data_source ) },
              data_shmem_name_{ shmem_name }, additional_plugins_{ std::move(
                                                  additional_plugins ) }
        //,chan_allocator( output_.get_segment_manager( ) )
        {
            unsigned long config_crc = 0;

            auto header = new ( output_->get( ) ) shared_mem_header(
                output_->size( ), parameters.seconds_in_buffer );

            // load_meta_data( header, meta_data_source_ );
        }

        void
        daqd_stream_instance::injest_data_message(
            shared_mem_header*    header,
            detail::signaler_api& signals,
            locking_signaler&     global_signals,
            pub_sub::SubMessage   msg )
        {
            auto dc_data = reinterpret_cast< daq_dc_data_t* >( msg.data( ) );
            unsigned int num_dcus = dc_data->header.dcuTotalModels;

            // std::cerr << "receiving msg @ " << (void*)(msg.data()) << " dcus
            // " << dc_data->header.dcuTotalModels;
            //            if (num_dcus > 0)
            //            {
            //                std::cerr << " dcuid " <<
            //                dc_data->header.dcuheader[0].dcuId
            //                    << " " << dc_data->header.dcuheader[0].timeSec
            //                    << ":"
            //                    << dc_data->header.dcuheader[0].cycle;
            //            }
            //            std::cerr << "  source " << this->data_shmem_name_ <<
            //            "\n";

            if ( num_dcus == 0 )
            {
                return;
            }
            auto&        input_header = dc_data->header;
            unsigned int cycle = input_header.dcuheader[ 0 ].cycle;
            char*        input_data = &( dc_data->dataBlock[ 0 ] );

            int dest_block_index = header->next_block();
            data_block& cur_block = header->blocks( dest_block_index );
            std::fill( cur_block.dcu_offsets.begin( ),
                       cur_block.dcu_offsets.end( ),
                       0 );
            std::fill( cur_block.dcu_status.begin( ),
                       cur_block.dcu_status.end( ),
                       0xbad );

            std::size_t cur_offset = 0;
            for ( unsigned int i = 0; i < num_dcus; ++i )
            {
                auto&        dcu_header = input_header.dcuheader[ i ];
                unsigned int cur_dcu = dcu_header.dcuId;
                cur_block.dcu_offsets[ cur_dcu ] = cur_offset;
                cur_block.dcu_status[ cur_dcu ] = dcu_header.status;
                cur_block.dcu_config[ cur_dcu ] = dcu_header.fileCrc;

                std::copy( input_data,
                           input_data + dcu_header.dataBlockSize,
                           &( cur_block.data[ cur_offset ] ) );
                // skip TP for now
                input_data += dcu_header.dataBlockSize + dcu_header.tpBlockSize;
                cur_offset += dcu_header.dataBlockSize;
            }

            cur_block.gps_second = input_header.dcuheader[ 0 ].timeSec;
            cur_block.cycle = cycle;

            __sync_synchronize();

            header->set_cur_block( dest_block_index );

            signals.signal_16th( );
            global_signals.signal_16th( );
            if ( cycle == 15 )
            {
                signals.signal_sec( );
                global_signals.signal_sec( );
                // std::cout << sec_count++ << "\n";
            }
        }

        void
        daqd_stream_instance::run( locking_signaler& global_signals )
        {
            auto header = output_->get_as< shared_mem_header >( );

            detail::signaler_api signals{ header->signal_block };

            //            auto cfg = header->get_config();
            //            ifo_checksums checksums{ cfg->checksums };

            Message_queue< pub_sub::Message, 5 > metadata_queue{ };

            pub_sub::Subscriber input;
            input.load_plugin( plugins::get_frame_sub_plugin( ) );
            input.load_plugin( plugins::get_cds_metadata_sub_plugin( ) );
            for ( const auto& plugin : additional_plugins_ )
            {
                if ( plugin )
                {
                    input.load_plugin( plugin );
                }
            }
            input.subscribe(
                data_source_str_,
                [ &metadata_queue, header, &signals, &global_signals, this ](
                    pub_sub::SubMessage msg ) {
                    // std::cout << "received message " << msg.key() << "\n";

                    if ( msg.data( ) == nullptr || msg.size( ) == 0 )
                    {
                        return;
                    }
                    if ( msg.key( ) & 0x01 ||
                         *reinterpret_cast< const std::uint32_t* >(
                             msg.data( ) ) == 0xffeeddcc )
                    {
                        //                        std::cout << "queuing metadata
                        //                        message " << msg.key( )
                        //                                  << "\n";
                        metadata_queue.emplace_with_timeout(
                            std::chrono::milliseconds( 0 ), msg.message( ) );
                        return;
                    }
                    injest_data_message( header, signals, global_signals, msg );
                } );
            {
                pub_sub::Message metadata( nullptr, 0 );
                while ( true )
                {
                    boost::optional< pub_sub::Message > new_msg(
                        metadata_queue.pop(
                            std::chrono::milliseconds( 250 ) ) );
                    if ( stop_ )
                    {
                        break;
                    }
                    if ( !new_msg )
                    {
                        continue;
                    }
                    // std::cout << "dequeued metadata message\n";
                    ifo_config new_ifo_config{ };
                    if ( !msg_to_ifo_config( new_msg.get( ), &new_ifo_config ) )
                    {
                        std::cerr << "could not build ifo config from metadata "
                                     "message\n";
                        continue;
                    }
                    if ( !new_ifo_config.channels.empty( ) )
                    {
                        //                        std::cout << "loading metadata
                        //                        - "
                        //                                  <<
                        //                                  new_ifo_config.checksums.master_config
                        //                                  << "\n";
                        load_meta_data( header, new_ifo_config );
                    }
                }
            }
        }

        void
        daqd_stream_instance::stop( )
        {
            stop_ = true;
        }

        daqd_stream_intl::daqd_stream_intl(
            const std::string&       data_source,
            const std::string&       shmem_name,
            buffer_parameters        parameters,
            daqd_stream::plugin_list additional_plugins )
            : header_block_( nullptr, 0 ), instances_{ }, data_source_str_{
                  shmem_name
              }
        {
            instances_.emplace_back( std::make_unique< daqd_stream_instance >(
                data_source, shmem_name, parameters, additional_plugins ) );
        }

        daqd_stream_intl::daqd_stream_intl(
            const std::string&                    buffer_name,
            std::vector< source_parameters >      source_list,
            daqd_stream::daqd_stream::plugin_list additional_plugins )
            : header_block_( buffer_name, 1024 * 1024 ), instances_{ },
              data_source_str_{ "multi://" + buffer_name }
        {
            {
                if ( source_list.empty( ) )
                {
                    throw std::runtime_error( "empty source list" );
                }
                if ( source_list.size( ) >= multi_source_max_sources )
                {
                    throw std::runtime_error( "too many sources" );
                }
                std::set< std::string > names;
                names.insert( buffer_name );
                for ( const auto& cur_source : source_list )
                {
                    if ( names.find( cur_source.label( ) ) != names.end( ) )
                    {
                        throw std::runtime_error(
                            "label and buffer names must be unique" );
                    }
                    if ( cur_source.label( ).size( ) >=
                         multi_source_max_source_length )
                    {
                        throw std::runtime_error(
                            "source label length too long" );
                    }
                    names.insert( cur_source.label( ) );
                }
            }
            auto* header = header_block_.get_as< multi_source_info_block >( );
            initialize_source_info_block( header );

            instances_.reserve( source_list.size( ) );
            std::transform( source_list.begin( ),
                            source_list.end( ),
                            std::back_inserter( instances_ ),
                            [ &additional_plugins ](
                                const source_parameters& source_params )
                                -> std::unique_ptr< daqd_stream_instance > {
                                return std::make_unique< daqd_stream_instance >(
                                    source_params.source( ),
                                    source_params.label( ),
                                    source_params.parameters( ),
                                    additional_plugins );
                            } );
        }

        void
        daqd_stream_intl::run( )
        {
            auto* header = header_block_.get_as< multi_source_info_block >( );
            signaler_api*         shared_signals = nullptr;
            signaling_data_block  dummy_block{ };
            signaling_data_block* shared_block =
                ( header ? &header->signal_block : &dummy_block );
            signaler_api     multi_signal_api( *shared_block );
            locking_signaler global_signals(
                ( header ? &multi_signal_api : nullptr ) );

            std::vector< std::thread > threads{ };
            threads.reserve( instances_.size( ) );
            wait_group wg{ };
            for ( auto& cur_instance : instances_ )
            {
                wg.add( 1 );
                auto* instance = cur_instance.get( );
                threads.emplace_back( [ instance, &wg, &global_signals ]( ) {
                    wait_group_cleanup w( wg );
                    instance->run( global_signals );
                } );
            }
            write_source_list( );
            wg.wait( );
            // so many things can go wrong here
            for ( auto& cur : threads )
            {
                cur.join( );
            }
        }

        void
        daqd_stream_intl::write_source_list( )
        {
            auto* header = header_block_.get_as< multi_source_info_block >( );
            if ( !header )
            {
                return;
            }
            auto end = decode_block_range_end( header->block_range );
            auto cur = end;
            for ( const auto& instance : instances_ )
            {
                header->sources[ cur ] = instance->data_shmem_name_.c_str( );
                cur = ( cur + 1 ) % multi_source_max_sources;
            }
            auto* range_dest =
                reinterpret_cast< std::atomic< std::uint64_t >* >(
                    &( header->block_range ) );
            *range_dest = encode_block_range( end, cur );
        }
    } // namespace detail
} // namespace daqd_stream