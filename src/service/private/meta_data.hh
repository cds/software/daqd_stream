//
// Created by jonathan.hanks on 3/11/21.
//

#ifndef DAQD_STREAM_META_DATA_HH
#define DAQD_STREAM_META_DATA_HH

#include <algorithm>
#include <array>
#include <functional>
#include <map>
#include <string>
#include <type_traits>
#include <vector>

#include <iostream>

#include <daqd_stream/daqd_stream.hh>

#include "checksum.hh"
#include "constants.hh"
#include "predicates.hh"
#include "shared_structures.hh"

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief Internal structure to track ini file processing.
         */
        struct ini_parser_intl
        {
            ini_parser_intl( ) = default;

            void
            reset_dcu( )
            {
                current_dcuid = -1;
                ending_offset = 0;
            }

            std::array< checksum_generator, max_dcu( ) > dcu_checksums{ };
            std::vector< online_channel >                channels{ };
            std::map< std::string, int >                 channel_lookup{ };

            std::string  err_msg{ };
            std::int32_t ending_offset{ 0 };
            int          current_dcuid{ -1 };
        };

        using dcu_metadata_cb = std::function< bool(
            int,
            std::uint32_t,
            std::vector< ::daqd_stream::online_channel > ) >;

        /*!
         * @brief Parse an ini file
         * @param filename The path to the master file
         * @return
         */
        ifo_config parse_master_file( const std::string& filename );

        bool parse_master_file( const std::string& filename,
                                dcu_metadata_cb    per_dcu_callback );

        ifo_config retrieve_meta_data( const std::string& meta_data_source );

        bool retrieve_meta_data( const std::string& meta_data_source,
                                 dcu_metadata_cb    per_dcu_callback );
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_META_DATA_HH
