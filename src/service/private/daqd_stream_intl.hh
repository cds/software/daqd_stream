//
// Created by jonathan.hanks on 2/17/22.
//

#ifndef DAQD_STREAM_DAQD_STREAM_INTL_HH
#define DAQD_STREAM_DAQD_STREAM_INTL_HH

#include <algorithm>
#include <atomic>
#include <string>
#include <thread>
#include <vector>

#include <daqd_stream/daqd_stream_server.hh>

#include "internal.hh"
#include "posix_shmem.hh"
#include "signaling.hh"

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief All the state that the main/server code needs
         */
        struct daqd_stream_instance
        {
            daqd_stream_instance(
                std::string                           data_source,
                const std::string&                    shmem_name,
                buffer_parameters                     parameters,
                daqd_stream::daqd_stream::plugin_list additional_plugins );

            void run( locking_signaler& global_signals );

            void stop( );

            void injest_data_message( shared_mem_header*    header,
                                      detail::signaler_api& signals,
                                      locking_signaler&     global_signals,
                                      pub_sub::SubMessage   msg );

            std::shared_ptr< Posix_shared_memory > output_;

            std::string         data_source_str_;
            std::string         data_shmem_name_;
            std::atomic< bool > stop_{ false };

            daqd_stream::daqd_stream::plugin_list additional_plugins_;

            /* allocators */
            // shmem_allocator< online_channel > chan_allocator;
        };

        struct daqd_stream_intl
        {

            daqd_stream_intl( const std::string&       data_source,
                              const std::string&       shmem_name,
                              buffer_parameters        parameters,
                              daqd_stream::plugin_list additional_plugins );

            daqd_stream_intl(
                const std::string&                    buffer_name,
                std::vector< source_parameters >      source_list,
                daqd_stream::daqd_stream::plugin_list additional_plugins );

            void write_source_list( );

            void run( );

            void
            stop( )
            {
                std::for_each(
                    instances_.begin( ),
                    instances_.end( ),
                    []( std::unique_ptr< daqd_stream_instance >& instance ) {
                        instance->stop( );
                    } );
            }

            Posix_shared_memory header_block_;
            std::vector< std::unique_ptr< daqd_stream_instance > > instances_;
            std::string data_source_str_;
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_DAQD_STREAM_INTL_HH
