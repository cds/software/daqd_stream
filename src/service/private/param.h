#ifndef PARAM_H
#define PARAM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CHAN_PARAM
{
    int   dcuid;
    int   datarate;
    int   acquire;
    int   ifoid;
    int   rmid;
    int   datatype;
    int   chnnum;
    int   testpoint;
    float gain;
    float slope;
    float offset;
    char  units[ 40 ];
    char  system[ 40 ];
} CHAN_PARAM;

int parseConfigFile( char*,
                     unsigned long*,
                     int ( *callback )( char*, struct CHAN_PARAM*, void* ),
                     int,
                     char*,
                     void* );

#ifdef __cplusplus
}
#endif

#endif
