//
// Created by jonathan.hanks on 6/21/21.
//
#include <daqd_stream/daqd_stream_server.hh>

#include "catch.hpp"

#include <utility>
#include <vector>

#include "internal.hh"

TEST_CASE( "The system should reject bad sizes for lookback/buffers" )
{
    REQUIRE_THROWS( daqd_stream::daqd_stream(
        "tcp://127.0.0.1:9998",
        "daqd_stream",
        daqd_stream::buffer_parameters( 20, 1024 * 1024 ) ) );
}

TEST_CASE( "Test various buffer sizes/vs lookbacks" )
{
    std::vector< std::pair< daqd_stream::buffer_parameters, bool > >
        test_cases = {
            std::make_pair(
                daqd_stream::buffer_parameters( 20, 1024 * 1024 * 1024 ),
                false ),
            std::make_pair(
                daqd_stream::buffer_parameters( 5, 1024 * 1024 * 1024 ), true ),
            std::make_pair( daqd_stream::buffer_parameters(
                                5, 1024 * 1024 * 1024 + 1024 * 1024 * 800 ),
                            true ),
            std::make_pair( daqd_stream::buffer_parameters(
                                12, 1024 * 1024 * 1024 + 1024 * 1024 * 800 ),
                            true ),
        };

    for ( const auto& test_case : test_cases )
    {
        auto f = []( const daqd_stream::buffer_parameters& params ) {
            daqd_stream::detail::validate_shmem_size(
                params.total_buffer_size, params.seconds_in_buffer );
        };
        if ( test_case.second )
        {
            REQUIRE_NOTHROW( f( test_case.first ) );
        }
        else
        {
            REQUIRE_THROWS( f( test_case.first ) );
        }
    }
}