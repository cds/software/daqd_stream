/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include "ini_parser.hh"
#include <daqd_stream/daqd_stream.hh>
#include "internal.hh"
#include "concatenate.hh"

#include <algorithm>
#include <fstream>

#include <advligorts/daq_data_types.h>

#include "param.h"

namespace daqd_stream
{
    static const int MIN_DATA_TYPE = _16bit_integer;
    static const int MAX_DATA_TYPE = _32bit_uint;
    namespace detail
    {
        int
        ini_parser_cb( char*              channel_name,
                       struct CHAN_PARAM* params,
                       void*              user )
        {
            if ( !channel_name || !params || !user )
            {
                return 0;
            }
            auto user_ = reinterpret_cast< ini_parser_intl* >( user );
            if ( params->datatype < MIN_DATA_TYPE ||
                 params->datatype > MAX_DATA_TYPE )
            {
                user_->err_msg =
                    concatenate( "Invalid data type found on ", channel_name );
                return 0;
            }
            if ( user_->current_dcuid < 0 )
            {
                user_->current_dcuid = params->dcuid;
            }
            if ( user_->current_dcuid != params->dcuid )
            {
                user_->err_msg =
                    concatenate( "Unexpected DCU change on ", channel_name );
                return 0;
            }
            if ( params->dcuid < 0 || params->dcuid >= max_dcu( ) )
            {
                user_->err_msg = concatenate( "Invalid DCU on ", channel_name );
                return 0;
            }
            auto it = user_->channel_lookup.find( channel_name );
            if ( it != user_->channel_lookup.end( ) )
            {
                user_->err_msg =
                    concatenate( "Duplicate channel ", channel_name );
                return 0;
            }
            online_channel chan{ };
            chan.name = channel_name;
            chan.dcuid = static_cast< short >( params->dcuid );
            chan.datarate = params->datarate;
            chan.datatype = static_cast< DATA_TYPE >( params->datatype );
            chan.bytes_per_16th =
                ( detail::data_type_size( chan.datatype ) * params->datarate ) /
                16;
            chan.data_offset = user_->ending_offset;
            chan.signal_gain = params->gain;
            chan.signal_slope = params->slope;
            chan.signal_offset = params->offset;
            chan.units = params->units;

            user_->dcu_checksums[ chan.dcuid ].hash( chan );

            user_->channel_lookup.insert( std::make_pair(
                std::string( channel_name ), user_->channels.size( ) ) );
            user_->channels.push_back( chan );
            user_->ending_offset += chan.bytes_per_16th;
            return 1;
        }

        ifo_config
        parse_master_file( const std::string& filename )
        {
            ifo_config      final_config;
            std::ifstream   f( filename );
            std::string     line;
            ini_parser_intl intl_state{ };
            unsigned long   dummy_crc = 0;
            while ( std::getline( f, line, '\n' ) )
            {
                auto start = line.find_first_not_of( " \t" );
                if ( start == std::string::npos )
                {
                    continue;
                }
                if ( line[ start ] == '#' )
                {
                    continue;
                }
                auto end = line.find_first_of( " \t#", start );
                if ( start != 0 || end != std::string::npos )
                {
                    line = line.substr( start, end );
                }
                if ( line.find( ".par" ) != std::string::npos )
                {
                    continue;
                }
                if ( !parseConfigFile( const_cast< char* >( line.c_str( ) ),
                                       &dummy_crc,
                                       ini_parser_cb,
                                       0,
                                       nullptr,
                                       &intl_state ) )
                {
                    throw std::runtime_error(
                        concatenate( "Unable to parse the master file. ",
                                     intl_state.err_msg ) );
                }
                intl_state.reset_dcu( );
            }

            std::sort( intl_state.channels.begin( ),
                       intl_state.channels.end( ),
                       less_by_name );
            final_config = std::move( intl_state );
            return final_config;
        }

        bool
        parse_master_file( const std::string& filename,
                           dcu_metadata_cb    per_dcu_callback )
        {
            std::ifstream f( filename );
            std::string   line;
            unsigned long file_crc = 0;
            while ( std::getline( f, line, '\n' ) )
            {
                auto start = line.find_first_not_of( " \t" );
                if ( start == std::string::npos )
                {
                    continue;
                }
                if ( line[ start ] == '#' )
                {
                    continue;
                }
                auto end = line.find_first_of( " \t#", start );
                if ( start != 0 || end != std::string::npos )
                {
                    line = line.substr( start, end );
                }
                if ( line.find( ".par" ) != std::string::npos )
                {
                    continue;
                }
                ini_parser_intl intl_state{ };
                if ( !parseConfigFile( const_cast< char* >( line.c_str( ) ),
                                       &file_crc,
                                       ini_parser_cb,
                                       0,
                                       nullptr,
                                       &intl_state ) )
                {
                    return false;
                }
                per_dcu_callback( intl_state.current_dcuid,
                                  file_crc,
                                  std::move( intl_state.channels ) );
            }
            return true;
        }
    } // namespace detail
} // namespace daqd_stream