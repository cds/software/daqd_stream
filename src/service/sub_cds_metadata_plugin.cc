#include "sub_cds_metadata_plugin.hh"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <mutex>

#include <thread>
#include <advligorts/daq_core.h>

#include "meta_data.hh"
#include <advligorts/daq_metadata.h>
#include <advligorts/daq_metadata_utils.hh>

namespace daqd_stream
{
    namespace plugins
    {
        namespace cds_metadata
        {

            struct MessageBlob
            {
                using value_type = pub_sub::Message;

                value_type
                allocate( std::size_t size )
                {
                    auto ptr = std::shared_ptr< unsigned char[] >(
                        new unsigned char[ size ], []( unsigned char* p ) {
                            if ( p )
                            {
                                delete[] p;
                            }
                        } );
                    return pub_sub::Message( std::move( ptr ), size );
                }

                void*
                data_pointer( const value_type& blob )
                {
                    return blob.data( );
                }
            };

            daq_channel_metadata_t
            to_daq_channel_metadata_t( const online_channel& input )
            {
                daq_channel_metadata_t output{ };
                input.name.copy_to( output.name );
                input.units.copy_to( output.units );
                output.datatype = static_cast< int >( input.datatype );
                output.datarate = input.datarate;
                output.signal_gain = input.signal_gain;
                output.signal_slope = input.signal_slope;
                output.signal_offset = input.signal_offset;
                return output;
            }

            class CdsMetadataSubFilter : public pub_sub::plugins::Subscription
            {
            public:
                explicit CdsMetadataSubFilter( std::string         meta_path,
                                               pub_sub::SubHandler handler )
                    : pub_sub::plugins::Subscription( ), handler_{ std::move(
                                                             handler ) },
                      meta_path_{ std::move( meta_path ) }, worker_{ }
                {
                    worker_ =
                        std::thread( [ this ]( ) { background_loop( ); } );
                }
                ~CdsMetadataSubFilter( ) override
                {
                    stopping_ = true;
                    worker_.join( );
                }

                void
                filter( pub_sub::SubMessage msg ) override
                {
                    auto daq = reinterpret_cast< const daq_dc_data_t* >(
                        msg.message( ).data( ) );
                    if ( daq->header.dcuTotalModels <= 0 )
                    {
                        return;
                    }

                    auto       old_key = msg.key( );
                    const auto CYCLE_MASK =
                        static_cast< pub_sub::KeyType >( 0x0ff );
                    const auto MAIN_MASK =
                        ~static_cast< pub_sub::KeyType >( 0x0ff );
                    auto new_key = ( old_key & MAIN_MASK ) |
                        ( ( old_key & CYCLE_MASK ) << 1 );
                    pub_sub::SubMessage new_msg(
                        msg.sub_id( ), new_key, msg.message( ) );
                    send_message( std::move( new_msg ) );

                    auto cur_config = std::atomic_load( &cur_config_ );
                    if ( !cur_config )
                    {
                        request_new_metadata_ = true;
                    }
                    else
                    {
                        for ( auto i = 0; i < daq->header.dcuTotalModels; i++ )
                        {
                            auto cur_dcu = daq->header.dcuheader[ i ].dcuId;
                            if ( daq->header.dcuheader[ i ].fileCrc !=
                                 cur_config->dcu_config[ cur_dcu ] )
                            {
                                request_new_metadata_ = true;
                                return;
                            }
                        }
                        // do we clear this????
                        request_new_metadata_ = false;
                    }
                }

            private:
                void
                send_message( pub_sub::SubMessage msg )
                {
                    std::unique_lock< std::mutex > l_{ handler_mutex_ };

                    last_key_ = msg.key( );
                    handler_( std::move( msg ) );
                }

                void
                inject_message( pub_sub::Message msg )
                {
                    pub_sub::KeyType new_key = 0;
                    {
                        std::unique_lock< std::mutex > l_{ handler_mutex_ };

                        if ( last_key_ == 0 || ( last_key_ & 0x01 ) )
                        {
                            return;
                        }
                        ++last_key_;
                        new_key = last_key_;
                        handler_( pub_sub::SubMessage(
                            sub_id( ), new_key, std::move( msg ) ) );
                    }
                    std::cout << "Metadata injected into message stream as "
                              << new_key << "\n";
                }

                void
                background_loop( )
                {
                    int iterations = 0;
                    while ( !stopping_ )
                    {
                        std::this_thread::sleep_for(
                            std::chrono::milliseconds( 250 ) );
                        if ( iterations % 4 == 0 && request_new_metadata_ )
                        {
                            daq_metadata::MetadataBuilder builder{ };
                            std::cout << "Requesting metadata from "
                                      << meta_path_ << "\n";
                            auto good_data = detail::retrieve_meta_data(
                                meta_path_,
                                [ &builder ](
                                    int                           dcu,
                                    std::uint32_t                 checksum,
                                    std::vector< online_channel > channels )
                                    -> bool {
                                    return builder.append(
                                        dcu,
                                        checksum,
                                        channels,
                                        to_daq_channel_metadata_t );
                                } );
                            if ( good_data )
                            {
                                std::cout << "Got good metadata\n";
                                inject_message(
                                    builder.to_binary_blob( MessageBlob( ) ) );
                                request_new_metadata_ = false;
                                auto config = std::make_shared<
                                    detail::ifo_checksums >( );
                                for ( const auto& cur_header : builder.headers )
                                {
                                    config->dcu_config[ cur_header.dcuId ] =
                                        cur_header.checksum;
                                }
                                config->recalculate_master_config( );
                                std::atomic_store( &cur_config_, config );
                            }
                            else
                            {
                                std::cout << "failed on metadata\n";
                            }
                        }
                        ++iterations;
                    }
                }

                std::mutex          handler_mutex_{ };
                pub_sub::SubHandler handler_;
                pub_sub::KeyType    last_key_{ 0 };

                std::atomic< bool > request_new_metadata_{ true };
                std::shared_ptr< detail::ifo_checksums > cur_config_;

                std::atomic< bool > stopping_{ false };
                std::string         meta_path_;

                std::thread worker_;
            };

            class CdsMetadataSubPlugin
                : public pub_sub::plugins::SubscriptionPluginApi
            {
            public:
                ~CdsMetadataSubPlugin( ) override = default;
                const std::string&
                prefix( ) const override
                {
                    static const std::string my_prefix{ "meta://" };
                    return my_prefix;
                }

                const std::string&
                version( ) const override
                {
                    static const std::string my_version{ "0" };
                    return my_version;
                }
                const std::string&
                name( ) const override
                {
                    static const std::string my_name{ "CDS metadata injector" };
                    return my_name;
                }
                bool
                is_source( ) const override
                {
                    return false;
                }
                std::shared_ptr< pub_sub::plugins::Subscription >
                subscribe( const std::string&        conn_str,
                           pub_sub::SubDebugNotices& debug_hooks,
                           pub_sub::SubHandler       handler ) override
                {
                    if ( conn_str.find( prefix( ) ) != 0 )
                    {
                        throw std::runtime_error(
                            "Invalid subscription type passed to the"
                            "cds metadata filter" );
                    }
                    auto meta_data_path = conn_str.substr( prefix( ).size( ) );
                    return std::make_shared< CdsMetadataSubFilter >(
                        meta_data_path, std::move( handler ) );
                }
            };

        } // namespace cds_metadata

        std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
        get_cds_metadata_sub_plugin( )
        {
            return std::make_shared<
                plugins::cds_metadata::CdsMetadataSubPlugin >( );
        }
    } // namespace plugins
} // namespace daqd_stream