//
// Created by jonathan.hanks on 3/11/21.
//
#include "remote_meta_data.hh"

#include <map>
#include <stdexcept>
#include <sstream>
#include <string>

#include <curl/curl.h>
#include <rapidjson/document.h>

namespace
{
    template < typename T >
    void
    req_object( const T& obj )
    {
        if ( !obj.IsObject( ) )
        {
            throw std::runtime_error( "json error, expected object" );
        }
    }
    void
    req_integer( const rapidjson::Value& val )
    {
        if ( !val.IsInt64( ) )
        {
            throw std::runtime_error( "json error, expected a number" );
        }
    }
    void
    req_number( const rapidjson::Value& val )
    {
        if ( !val.IsNumber( ) )
        {
            throw std::runtime_error( "json error, expected a number" );
        }
    }
    void
    req_array( const rapidjson::Value& val )
    {
        if ( !val.IsArray( ) )
        {
            throw std::runtime_error( "json error, expected an array" );
        }
    }

    void
    req_string( const rapidjson::Value& val )
    {
        if ( !val.IsString( ) )
        {
            throw std::runtime_error( "json error, expecting a string" );
        }
    }

    void
    validate_channel( const rapidjson::Value& channel )
    {
        req_object( channel );
        std::map< const char*, void ( * )( const rapidjson::Value& ) > requires{
            { "Name", req_string },          { "DcuId", req_integer },
            { "DataType", req_integer },     { "DataRate", req_integer },
            { "BytesPer16th", req_integer }, { "DataOffset", req_integer },
            { "SignalGain", req_number },    { "SignalSlope", req_number },
            { "SignalOffset", req_number },  { "Units", req_string },
        };
        for ( const auto& req : requires )
        {
            if ( !channel.HasMember( req.first ) )
            {
                throw std::runtime_error( "Dcu missing a field" );
            }
            const rapidjson::Value& val = channel[ req.first ];
            req.second( val );
        }
    }

    void
    validate_channels( const rapidjson::Value& channels )
    {
        req_array( channels );
        for ( auto i = 0; i < channels.Size( ); ++i )
        {
            validate_channel( channels[ i ] );
        }
    }

    void
    validate_dcu( const rapidjson::Value& dcu )
    {
        req_object( dcu );
        std::map< const char*, void ( * )( const rapidjson::Value& ) > requires{
            { "DcuId", req_integer },
            { "ChannelHash", req_integer },
            { "ConfigHash", req_integer },
            { "Channels", validate_channels },
        };
        for ( const auto& req : requires )
        {
            if ( !dcu.HasMember( req.first ) )
            {
                throw std::runtime_error( "Dcu missing a field" );
            }
            const rapidjson::Value& val = dcu[ req.first ];
            req.second( val );
        }
    }

    void
    validate_dcus( const rapidjson::Value& dcus )
    {
        req_object( dcus );
        for ( auto cur = dcus.MemberBegin( ); cur != dcus.MemberEnd( ); ++cur )
        {
            const auto& childVal = cur->value;
            validate_dcu( childVal );
        }
    }

    void
    validate_document( const rapidjson::Document& doc )
    {
        req_object( doc );
        for ( const auto& key : { "Dcus", "ConfigHash" } )
        {
            if ( !doc.HasMember( key ) )
            {
                throw std::runtime_error(
                    "Top level document missing required key" );
            }
        }
        const rapidjson::Value& dcus = doc[ "Dcus" ];
        validate_dcus( dcus );
    }
} // namespace

namespace daqd_stream
{
    namespace detail
    {
        struct response
        {
            int                 status{ 200 };
            std::string         headers{ };
            std::vector< char > data{ };
        };

        static size_t
        easy_write_cb( void* data, size_t size, size_t nmemb, void* user )
        try
        {
            if ( !data || !user )
            {
                return 0;
            }
            auto  total_size = size * nmemb;
            auto& write_buffer =
                *reinterpret_cast< std::vector< char >* >( user );
            auto char_data = reinterpret_cast< char* >( data );

            write_buffer.reserve( write_buffer.size( ) + total_size );
            std::copy( char_data,
                       char_data + total_size,
                       std::back_inserter( write_buffer ) );

            return total_size;
        }
        catch ( ... )
        {
            return 0;
        }

        static size_t
        easy_header_cb( char* buffer, size_t size, size_t nitems, void* user )
        try
        {
            if ( !buffer || !user )
            {
                return 0;
            }
            auto  total_size = size * nitems;
            auto& os = *reinterpret_cast< std::ostream* >( user );

            os << std::string( buffer, buffer + total_size );

            return total_size;
        }
        catch ( ... )
        {
            return 0;
        }

        response
        easy_get( const std::string& url )
        {
            response           resp;
            std::ostringstream header_os;

            auto req = std::unique_ptr< CURL, void ( * )( CURL* ) >(
                curl_easy_init( ), curl_easy_cleanup );
            if ( !req )
            {
                throw std::runtime_error(
                    "Unable to initialize http client interface" );
            }

            curl_easy_setopt(
                req.get( ), CURLOPT_WRITEFUNCTION, easy_write_cb );
            curl_easy_setopt( req.get( ), CURLOPT_WRITEDATA, &resp.data );

            curl_easy_setopt(
                req.get( ), CURLOPT_HEADERFUNCTION, easy_header_cb );
            curl_easy_setopt( req.get( ), CURLOPT_HEADERDATA, &header_os );

            curl_easy_setopt( req.get( ), CURLOPT_URL, url.c_str( ) );
            CURLcode res = curl_easy_perform( req.get( ) );
            if ( res != CURLE_OK )
            {
                throw std::runtime_error( "Error with http request" );
            }
            long code = 0;
            if ( curl_easy_getinfo(
                     req.get( ), CURLINFO_RESPONSE_CODE, &code ) != CURLE_OK )
            {
                throw std::runtime_error(
                    "Unable to determine http status code" );
            }
            resp.status = static_cast< int >( code );
            resp.headers = header_os.str( );

            return resp;
        }

        template < typename It >
        void
        parse_channels( const rapidjson::Value& channels, It out )
        {
            std::transform(
                channels.Begin( ),
                channels.End( ),
                out,
                []( const rapidjson::Value& channel ) -> online_channel {
                    online_channel chan;

                    chan.name = channel[ "Name" ].GetString( );
                    chan.dcuid =
                        static_cast< short >( channel[ "DcuId" ].GetInt( ) );
                    chan.datatype = static_cast< DATA_TYPE >(
                        channel[ "DataType" ].GetInt( ) );
                    chan.datarate = channel[ "DataRate" ].GetInt( );
                    chan.bytes_per_16th = channel[ "BytesPer16th" ].GetInt( );
                    chan.data_offset = channel[ "DataOffset" ].GetInt( );
                    chan.signal_gain = channel[ "SignalGain" ].GetFloat( );
                    chan.signal_slope = channel[ "SignalSlope" ].GetFloat( );
                    chan.signal_offset = channel[ "SignalOffset" ].GetFloat( );
                    chan.units = channel[ "Units" ].GetString( );
                    return chan;
                } );
        }

        ifo_config
        parse_remote_meta_data_json( const std::vector< char >& data )
        {
            ifo_config cfg;

            rapidjson::Document document;
            document.Parse( data.data( ), data.size( ) );
            validate_document( document );

            cfg.checksums.master_config = document[ "ConfigHash" ].GetUint( );
            rapidjson::Value& dcus = document[ "Dcus" ];

            for ( auto it = dcus.MemberBegin( ); it != dcus.MemberEnd( ); ++it )
            {
                rapidjson::Value& curDcu = it->value;
                int               id = curDcu[ "DcuId" ].GetInt( );

                std::uint32_t config_hash = curDcu[ "ConfigHash" ].GetUint( );
                cfg.checksums.dcu_config[ id ] = config_hash;

                parse_channels( curDcu[ "Channels" ],
                                std::back_inserter( cfg.channels ) );
            }
            cfg.checksums.recalculate_master_config( );
            std::sort(
                cfg.channels.begin( ),
                cfg.channels.end( ),
                []( const online_channel& a, const online_channel& b ) -> bool {
                    return a.name < b.name;
                } );
            return cfg;
        }

        bool
        parse_remote_meta_data_json( const std::vector< char >& data,
                                     dcu_metadata_cb& per_dcu_callback )
        {
            rapidjson::Document document;
            document.Parse( data.data( ), data.size( ) );
            validate_document( document );

            rapidjson::Value& dcus = document[ "Dcus" ];

            for ( auto it = dcus.MemberBegin( ); it != dcus.MemberEnd( ); ++it )
            {
                rapidjson::Value& curDcu = it->value;
                int               id = curDcu[ "DcuId" ].GetInt( );

                std::uint32_t config_hash = curDcu[ "ConfigHash" ].GetUint( );

                std::vector< online_channel > channels{ };

                parse_channels( curDcu[ "Channels" ],
                                std::back_inserter( channels ) );

                if ( !per_dcu_callback(
                         id, config_hash, std::move( channels ) ) )
                {
                    return false;
                }
            }
            return true;
        }

        /*!
         * @brief Parse an ini file
         * @param hostname The path to the master file
         * @return
         */
        ifo_config
        retrieve_remote_meta_data( const std::string& hostname, int port )
        {
            std::ostringstream os;
            os << "http://" << hostname << ":" << port << "/";
            auto resp = easy_get( os.str( ) );
            if ( resp.status != 200 )
            {
                throw std::runtime_error( "Error status on the http request" );
            }
            return parse_remote_meta_data_json( resp.data );
        }

        /*!
         * @brief Parse an ini file
         * @param hostname The path to the master file
         * @param per_dcu_callback callback to use when each dcu is parsed
         * @return
         */
        bool
        retrieve_remote_meta_data( const std::string& hostname,
                                   int                port,
                                   dcu_metadata_cb    per_dcu_callback )
        {
            std::ostringstream os;
            os << "http://" << hostname << ":" << port << "/";
            auto resp = easy_get( os.str( ) );
            if ( resp.status != 200 )
            {
                throw std::runtime_error( "Error status on the http request" );
            }
            return parse_remote_meta_data_json( resp.data, per_dcu_callback );
        }
    } // namespace detail
} // namespace daqd_stream