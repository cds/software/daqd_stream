#include "compare_data.hh"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>

#include "internal.hh"

namespace
{
    struct dcu_size_conf_t
    {
        std::size_t             dcu_size{ 0 };
        daqd_stream::checksum_t dcu_config{ 0 };
    };

    using time_point_t = std::pair< std::uint64_t, std::uint64_t >;

    daqd_stream::detail::data_block&
    find_time_point( daqd_stream::detail::shared_mem_header& header,
                     time_point_t                            t )
    {
        for ( auto it = header.blocks_begin( ); it != header.blocks_end( );
              ++it )
        {
            if ( ( it->gps_second == t.first ) && ( it->cycle == t.second ) )
            {
                return *it;
            }
        }
        throw std::runtime_error(
            "Unable to find requested time point in shared mem header blocks" );
    }

    std::set< time_point_t >
    get_available_times( daqd_stream::detail::shared_mem_header& header )
    {
        std::set< time_point_t > points{ };
        auto exclude_block = header.next_block( header.get_cur_block( ) );
        auto max = header.max_data_blocks( );
        for ( auto i = 0; i < max; ++i )
        {
            if ( i == exclude_block )
            {
                continue;
            }
            const daqd_stream::detail::data_block& cur_block =
                header.blocks( i );
            points.insert(
                time_point_t( cur_block.gps_second, cur_block.cycle ) );
        }
        return points;
    }

    std::array< dcu_size_conf_t, daqd_stream::detail::max_dcu( ) >
    build_config_map( daqd_stream::detail::shared_mem_header& header )
    {
        std::array< dcu_size_conf_t, daqd_stream::detail::max_dcu( ) > cur{ };
        auto& checksums = header.config_->checksums;
        auto& channels = header.config_->channels;

        for ( const auto& chan : channels )
        {
            auto end = chan.data_offset + chan.bytes_per_16th;
            if ( end > cur[ chan.dcuid ].dcu_size )
            {
                cur[ chan.dcuid ].dcu_size = end;
            }
        }

        for ( auto i = 0; i < daqd_stream::detail::max_dcu( ); ++i )
        {
            cur[ i ].dcu_config = checksums.dcu_config[ i ];
        }
        return cur;
    }
} // namespace

void
handle_compare_data( Memory_buffer& a, Memory_buffer& b )
{
    if ( !a.get( ) || !b.get( ) )
    {
        throw std::runtime_error( "Invalid memory buffer passed in" );
    }
    auto* header_a = a.get_as< daqd_stream::detail::shared_mem_header >( );
    auto* header_b = b.get_as< daqd_stream::detail::shared_mem_header >( );
    auto  segments_a = get_available_times( *header_a );
    auto  segments_b = get_available_times( *header_b );
    std::cout << "a has << " << segments_a.size( ) << " time slices\n";
    std::cout << "b has << " << segments_b.size( ) << " time slices\n";
    std::set< time_point_t > overlap{ };
    std::set_intersection( segments_a.begin( ),
                           segments_a.end( ),
                           segments_b.begin( ),
                           segments_b.end( ),
                           std::inserter( overlap, overlap.end( ) ) );
    std::cout << "Found " << overlap.size( ) << " common time slices\n";

    auto config = build_config_map( *header_a );

    for ( const auto& time_point : overlap )
    {
        auto& db_a = find_time_point( *header_a, time_point );
        auto& db_b = find_time_point( *header_b, time_point );

        if ( !std::equal( std::begin( db_a.dcu_config ),
                          std::end( db_a.dcu_config ),
                          std::begin( db_b.dcu_config ) ) ||
             !std::equal( std::begin( db_a.dcu_status ),
                          std::end( db_a.dcu_status ),
                          std::begin( db_b.dcu_status ) ) )
        {
            std::cout << "config or status difference at " << time_point.first
                      << ":" << time_point.second << "\n";
            continue;
        }

        for ( auto i = 0; i < daqd_stream::detail::max_dcu( ); ++i )
        {
            if ( db_a.dcu_config[ i ] != config[ i ].dcu_config )
            {
                std::cout << "skipping due to config mismatch\n";
                continue;
            }
            if ( config[ i ].dcu_size == 0 )
            {
                continue;
            }
            auto* start_a = &db_a.data[ db_a.dcu_offsets[ i ] ];
            auto* end_a = start_a + config[ i ].dcu_size;
            auto* start_b = &db_b.data[ db_b.dcu_offsets[ i ] ];
            if ( !std::equal( start_a, end_a, start_b ) )
            {
                std::ostringstream os{ };
                os << "Data mismatch found " << time_point.first << ":"
                   << time_point.second;
                os << " (" << i << ")\n";
                throw std::runtime_error( os.str( ) );
            }
        }
    }
    std::cout << "no data mismatches found\n";
}