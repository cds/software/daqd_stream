//
// Created by jonathan.hanks on 2/4/21.
//
#include "nds_stream.hh"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <nds.hh>
#include <stdexcept>

namespace
{

    ComparisonType
    to_comp_type( NDS::channel::data_type input )
    {
        switch ( input )
        {
        case NDS::channel::DATA_TYPE_INT16:
            return ComparisonType::INT16;
        case NDS::channel::DATA_TYPE_INT32:
            return ComparisonType::INT32;
        case NDS::channel::DATA_TYPE_INT64:
            return ComparisonType::INT64;
        case NDS::channel::DATA_TYPE_FLOAT32:
            return ComparisonType::FLOAT32;
        case NDS::channel::DATA_TYPE_FLOAT64:
            return ComparisonType::FLOAT64;
        case NDS::channel::DATA_TYPE_UINT32:
            return ComparisonType::UINT32;
        default:
            throw std::runtime_error( "Unknown channel type found" );
        }
    }

    std::vector< char >
    to_char_vector( const NDS::buffer& buf, int sec_offset )
    {
        std::vector< char > out;
        auto begin = reinterpret_cast< const char* >( buf.cbegin< void >( ) );
        auto end = reinterpret_cast< const char* >( buf.cend< void >( ) );
        if ( sec_offset > 0 )
        {
            auto samples_per_sec = buf.SampleRate( );
            auto bytes_per_sec = buf.samples_to_bytes( samples_per_sec );

            auto start = begin + bytes_per_sec * sec_offset;
            auto finish = start + bytes_per_sec;

            end = std::min( end, finish );
            begin = std::min( end, start );
        }

        out.resize( std::distance( begin, end ) );
        std::copy( begin, end, out.begin( ) );
        return out;
    }

    std::vector< ComparisonBuffer >
    to_comp_buffers( const NDS::buffers_type& input, int sec_offset )
    {
        std::vector< ComparisonBuffer > out{ };
        out.reserve( input.size( ) );
        std::transform(
            input.begin( ),
            input.end( ),
            std::back_inserter( out ),
            [ sec_offset ]( const NDS::buffer& buf ) -> ComparisonBuffer {
                return ComparisonBuffer{ to_comp_type( buf.DataType( ) ),
                                         true,
                                         to_char_vector( buf, sec_offset ) };
            } );
        return out;
    }
} // namespace

bool debug_nds = false;

void
nds_stream_main_loop( Comparator&                       comp,
                      int                               slot,
                      std::atomic< bool >&              finished,
                      const std::string&                hostname,
                      int                               port,
                      const std::vector< std::string >& selections,
                      int                               stride )
{
    while ( !finished )
    {
        auto params = NDS::parameters( hostname, port );
        auto stream =
            NDS::iterate( params, NDS::request_period( stride ), selections );
        for ( const auto& bufs : stream )
        {
            if ( finished )
            {
                stream.abort( );
                break;
            }

            int64_t gps = ( *bufs ).front( ).Start( );
            int64_t nano = ( *bufs ).front( ).StartNano( );
            if ( debug_nds )
            {
                std::cout << "n" << slot << " " << gps << ":" << nano << "\n";
            }
            if ( stride > 1 )
            {
                for ( int sec = 0; sec < stride; ++sec )
                {
                    comp.input_cycle(
                        slot, gps + sec, nano, to_comp_buffers( *bufs, sec ) );
                }
            }
            else
            {
                comp.input_cycle(
                    slot, gps, nano, to_comp_buffers( *bufs, -1 ) );
            }
        }
    }
}