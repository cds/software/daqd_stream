//
// Created by jonathan.hanks on 2/4/21.
//

#ifndef DAQD_STREAM_TEST_NDS_STREAM_HH
#define DAQD_STREAM_TEST_NDS_STREAM_HH

#include <atomic>
#include <string>
#include <vector>

#include "compare.hh"

extern bool debug_nds;

void nds_stream_main_loop( Comparator&                       comp,
                           int                               slot,
                           std::atomic< bool >&              finished,
                           const std::string&                hostname,
                           int                               port,
                           const std::vector< std::string >& selections,
                           int                               stride );

#endif // DAQD_STREAM_TEST_NDS_STREAM_HH
