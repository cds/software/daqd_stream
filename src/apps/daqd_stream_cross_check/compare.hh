//
// Created by jonathan.hanks on 2/5/21.
//

#ifndef DAQD_STREAM_TEST_COMPARE_HH
#define DAQD_STREAM_TEST_COMPARE_HH

#include <algorithm>
#include <deque>
#include <functional>
#include <iostream>
#include <mutex>
#include <sstream>
#include <string>
#include <vector>

#include <boost/endian/conversion.hpp>

enum class ComparisonType
{
    INT16,
    INT32,
    INT64,
    FLOAT32,
    FLOAT64,
    UINT32,
};

inline std::ostream&
operator<<( std::ostream& os, ComparisonType ct )
{
    switch ( ct )
    {
    case ComparisonType::INT16:
        os << "<int16>";
        break;
    case ComparisonType::INT32:
        os << "<int32>";
        break;
    case ComparisonType::INT64:
        os << "<int64>";
        break;
    case ComparisonType::FLOAT32:
        os << "<float32>";
        break;
    case ComparisonType::FLOAT64:
        os << "<float64>";
        break;
    case ComparisonType::UINT32:
        os << "<uint32>";
        break;
    default:
        os << "unknown";
        break;
    }
    return os;
}

template < typename T >
void
flip( char* start, const char* end )
{
    for ( auto cur = start; cur < end; cur += sizeof( T ) )
    {
        *reinterpret_cast< T* >( cur ) =
            boost::endian::endian_reverse( *reinterpret_cast< T* >( cur ) );
    }
}

struct ComparisonBuffer
{
    ComparisonType      type;
    bool                data_good{ true };
    std::vector< char > data;

    ComparisonBuffer( ComparisonType comp_type,
                      bool is_data_good,
                      std::vector<char> buffer) noexcept:
    type{ comp_type }, data_good{is_data_good}, data{std::move(buffer)}
    {}

    bool
    operator==( const ComparisonBuffer& other ) const noexcept
    {
        if ( type != other.type || data.size( ) != other.data.size( ) )
        {
            return false;
        }
        return std::equal( data.begin( ), data.end( ), other.data.begin( ) );
    }
    bool
    operator!=( const ComparisonBuffer& other ) const noexcept
    {
        return !operator==( other );
    }

    std::vector< char >
    flip_endianness( ) const
    {
        std::vector< char > out( data );
        char*               begin = out.data( );
        char*               end = begin + out.size( );
        switch ( type )
        {
        case ComparisonType::INT16:
            flip< std::int16_t >( begin, end );
            break;
        case ComparisonType::INT32:
        case ComparisonType::FLOAT32:
        case ComparisonType::UINT32:
            flip< std::int32_t >( begin, end );
            break;
        case ComparisonType::INT64:
        case ComparisonType::FLOAT64:
            flip< std::int64_t >( begin, end );
            break;
            //            default:
            //                throw std::runtime_error("Unknown data type,
            //                cannot flip endianness");
        }
        return out;
    }

    template < typename T >
    void
    print_some( std::ostream& os ) const
    {
        auto cur = reinterpret_cast< const T* >( data.data( ) );
        auto end = reinterpret_cast< const T* >( data.data( ) + data.size( ) );

        auto max_num = std::max< int >( std::distance( cur, end ), 1024 );

        for ( auto i = 0; cur < end && i < max_num; ++cur, ++i )
        {
            os << ( i > 0 ? " " : "" ) << *cur;
        }
    }

    void
    print_sample( std::ostream& os ) const
    {
        os << type << " [";
        switch ( type )
        {
        case ComparisonType::INT16:
            print_some< std::int16_t >( os );
            break;
        case ComparisonType::INT32:
            print_some< std::int32_t >( os );
            break;
        case ComparisonType::INT64:
            print_some< std::int64_t >( os );
            break;
        case ComparisonType::FLOAT32:
            print_some< float >( os );
            break;
        case ComparisonType::FLOAT64:
            print_some< double >( os );
            break;
        case ComparisonType::UINT32:
            print_some< std::uint32_t >( os );
            break;
        default:
            os << "hmmm, don't know how to show this data";
            break;
        }
        os << "]";
    }
};

class Comparator
{
    struct Samples
    {
        Samples( std::int64_t                    in_gps,
                 std::int64_t                    in_nano,
                 std::vector< ComparisonBuffer > buf )
            : gps{ in_gps }, nano{ in_nano }, buffer{ std::move( buf ) }
        {
        }

        Samples( const Samples& ) = default;
        Samples( Samples&& ) = default;

        Samples& operator=( const Samples& ) = default;
        Samples& operator=( Samples&& ) = default;

        std::int64_t                    gps;
        std::int64_t                    nano;
        std::vector< ComparisonBuffer > buffer;
    };

    static std::vector< std::deque< Samples > >
    create_sources( int slots )
    {
        std::vector< std::deque< Samples > > d;
        for ( auto i = 0; i >= 0 && i < slots; ++i )
        {
            d.emplace_back( );
        }
        return d;
    }

public:
    enum class stride_type
    {
        FAST_STRIDE,
        SLOW_STRIDE,
    };

    Comparator( int                        slots,
                std::vector< std::string > channel_names,
                stride_type                stride )
        : m_{ }, sources_{ create_sources( slots ) },
          channel_names_{ std::move( channel_names ) }, stride_{ stride }
    {
    }

    void
    input_cycle( int                             slot,
                 std::int64_t                    gps,
                 std::int64_t                    nano,
                 std::vector< ComparisonBuffer > buffers )
    {
        if ( slot < 0 || slot >= sources_.size( ) )
        {
            throw std::range_error( "Invalid slot value" );
        }
        std::unique_lock< std::mutex > l_{ m_ };
        sources_[ slot ].emplace_back( gps, nano, std::move( buffers ) );
        if ( sources_[ slot ].size( ) > 16 )
        {
            sources_[ slot ].pop_front( );
        }

        auto available_sources = std::vector< const Samples* >{ };
        available_sources.reserve( sources_.size( ) );

        for ( const auto& source : sources_ )
        {
            auto it =
                std::find_if( source.begin( ),
                              source.end( ),
                              [ gps, nano ]( const Samples& s ) -> bool {
                                  return ( s.gps == gps && s.nano == nano );
                              } );
            if ( it != source.end( ) )
            {
                available_sources.emplace_back( &( *it ) );
            }
        }

        if ( available_sources.size( ) != sources_.size( ) )
        {
            return;
        }
        bool good = true;
        int  slot_num = -1;
        for ( const auto src_ptr : available_sources )
        {
            ++slot_num;
            const Samples& s0 = *available_sources.front( );

            if ( s0.buffer.size( ) != src_ptr->buffer.size( ) )
            {
                std::cout << "buffer size mismatch, cannot compare\n";
            }

            for ( auto i = 0; i < s0.buffer.size( ); ++i )
            {
                const auto& a = s0.buffer[ i ];
                const auto& b = src_ptr->buffer[ i ];
                if ( a.type != b.type )
                {
                    std::cerr << "type mismatch - buffer 0 & " << i << "\n";
                    good = false;
                }
                if ( a.data.size( ) != b.data.size( ) )
                {
                    std::cerr << "data length mismatch 0 & " << i << "\n";
                    good = false;
                }
                else
                {
                    if ( !std::equal(
                             a.data.begin( ), a.data.end( ), b.data.begin( ) ) )
                    {
                        good = false;
                        auto flipped = b.flip_endianness( );
                        auto need_flip = false;
                        if ( std::equal( a.data.begin( ),
                                         a.data.end( ),
                                         flipped.begin( ) ) )
                        {
                            need_flip = true;
                        }
                        std::cerr
                            << "data mismatch slot 0 & " << slot_num
                            << " on channel " << i << " " << channel_names_[ i ]
                            << ( need_flip ? " needs byte swap" : "" )
                            << ( !a.data_good
                                     ? " 0 marks data bad "
                                     : ( !b.data_good ? " other marks data bad "
                                                      : "" ) )
                            << "\n";
                        a.print_sample( std::cerr );
                        std::cerr << "\n";
                        b.print_sample( std::cerr );
                        std::cerr << "\n";
                    }
                }
            }
        }
        if ( good )
        {
            if ( stride_ == stride_type::FAST_STRIDE )
            {
                if ( good_matches_ % 16 == 0 )
                {
                    std::cout << good_matches_ / 16 << "\n";
                }
            }
            else
            {
                std::cout << good_matches_ << "\n";
            }
            ++good_matches_;
        }
    }

private:
    std::mutex m_;

    std::vector< std::deque< Samples > > sources_;
    int                                  good_matches_{ 0 };
    stride_type                          stride_;
    std::vector< std::string >           channel_names_{ };
};
#endif // DAQD_STREAM_TEST_COMPARE_HH
