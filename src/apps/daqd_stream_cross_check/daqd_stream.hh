//
// Created by jonathan.hanks on 1/25/21.
//

#ifndef DAQD_STREAM_TEST_DAQD_STREAM_HH
#define DAQD_STREAM_TEST_DAQD_STREAM_HH

#include <atomic>
#include <vector>

#include <daqd_stream/daqd_stream.hh>

#include "compare.hh"

extern bool debug_daqd;

void daqd_stream_main_loop( Comparator&                       comp,
                            int                               slot,
                            std::atomic< bool >&              finished,
                            daqd_stream::client&              client,
                            const std::vector< std::string >& selections,
                            int                               stride );

#endif // DAQD_STREAM_TEST_DAQD_STREAM_HH
