/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <random>

#include "daqd_stream.hh"

#include "internal.hh"

#include "fe_stream_generator.hh"

struct verify_stats_t
{
    using int_vect = std::vector< unsigned int >;

    verify_stats_t( ) : skipped{ }, ok{ }, error{ }
    {
    }
    verify_stats_t( const verify_stats_t& other ) = default;
    verify_stats_t( verify_stats_t&& other ) = default;

    verify_stats_t& operator=( const verify_stats_t& other ) = default;
    verify_stats_t& operator=( verify_stats_t&& other ) = default;

    int_vect skipped;
    int_vect ok;
    int_vect error;
};

using dcu_map_t =
    std::map< unsigned int, std::vector< daqd_stream::online_channel > >;

bool
less_by_offset( const daqd_stream::online_channel& a,
                const daqd_stream::online_channel& b )
{
    return a.data_offset < b.data_offset;
}

dcu_map_t
create_dcu_map( const std::vector< daqd_stream::online_channel >& chans )
{
    dcu_map_t results;

    for ( unsigned int i = 0; i < daqd_stream::detail::max_dcu( ); ++i )
    {
        results.insert( std::make_pair(
            i, std::vector< daqd_stream::online_channel >{ } ) );
    }
    std::for_each( chans.begin( ),
                   chans.end( ),
                   [ &results ]( const daqd_stream::online_channel& chan ) {
                       results[ chan.dcuid ].emplace_back( chan );
                   } );
    for ( auto it = results.begin( ); it != results.end( ); ++it )
    {
        std::sort( it->second.begin( ), it->second.end( ), less_by_offset );
    }
    return results;
}

daqd_stream::detail::data_block
load_block( const std::string& fname )
{
    daqd_stream::detail::data_block results{ };

    std::ifstream input( fname, std::ios::binary );
    input.read( reinterpret_cast< char* >( &results ), sizeof( results ) );
    auto bytes_read = input.gcount( );
    if ( bytes_read != sizeof( results ) )
    {
        throw std::runtime_error( "Unable to load data block" );
    }
    return results;
}

void
verify_dcu( unsigned int                                      dcuid,
            const std::vector< daqd_stream::online_channel >& chans,
            const daqd_stream::detail::data_block&            dblock,
            verify_stats_t&                                   stats )
{
    const int NANOSTEP = 1000000000 / 16;
    auto      status = dblock.dcu_status[ dcuid ];
    if ( status == 0xbad )
    {
        stats.skipped.emplace_back( dcuid );
        return;
    }
    for ( int i = 0; i < chans.size( ) && i < 5; ++i )
    {
        std::cout << "\t" << chans[ i ].name.data( ) << std::endl;
    }

    const char* data = dblock.data;
    auto        offset = dblock.dcu_offsets[ dcuid ];
    data += offset;
    std::vector< char > buf{ };
    for ( const auto& chan : chans )
    {
        std::string  name = chan.name.data( );
        GeneratorPtr gen = create_generator( name );
        buf.resize( gen->bytes_per_sec( ) / 16 );

        auto end = gen->generate(
            dblock.gps_second, dblock.cycle * NANOSTEP, buf.data( ) );
        if ( !std::equal( buf.data( ), end, data ) )
        {
            const int* bufi = reinterpret_cast< const int* >( buf.data( ) );
            const int* datai = reinterpret_cast< const int* >( data );

            std::cout << "GPS: " << dblock.gps_second << " : " << dblock.cycle
                      << "\n";
            std::cout << "Expected: " << *bufi << "\nGot: " << *datai << "\n";
            stats.error.emplace_back( dcuid );
            return;
        }
        data += end - buf.data( );
    }
    stats.ok.emplace_back( dcuid );
}

void
verify_online( daqd_stream::client&                        daq_client,
               std::vector< daqd_stream::online_channel >& chan_list )
{
    int                errors = 0;
    std::random_device rd;
    std::mt19937       gen( rd( ) );

    std::uniform_int_distribution<> dis( 0, chan_list.size( ) - 1 );
    daqd_stream::channel_name_list  online_list{
        "mod5-0--gpssoff1p--5--2--2048", "mod5-1--gpssoff1p--6--2--2048"
    };
    while ( online_list.size( ) < 25 )
    {
        std::string name = chan_list.at( dis( gen ) ).name.data( );
        auto it = std::find( online_list.begin( ), online_list.end( ), name );
        if ( it == online_list.end( ) )
        {
            online_list.emplace_back( std::move( name ) );
        }
    }
    auto plan = daq_client.plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH,
                                         online_list );
    std::vector< char >      dest( plan.required_size_16th( ) );
    daqd_stream::data_status status_buf;
    daq_client.get_16th_data( plan, dest.data( ), dest.size( ), status_buf );
    char*               cur = dest.data( );
    std::vector< char > buf;
    std::for_each(
        online_list.begin( ),
        online_list.end( ),
        [ status_buf, &buf, &cur, &errors ]( const std::string& name ) {
            auto generator = create_generator( name );
            buf.resize( generator->bytes_per_sec( ) / 16 );
            auto end = generator->generate(
                status_buf.gps_seconds, status_buf.gps_nano, buf.data( ) );
            auto size = end - buf.data( );
            if ( !std::equal( buf.data( ), end, cur ) )
            {
                std::cout << "Error on " << name << "\t"
                          << status_buf.gps_seconds << " : "
                          << status_buf.gps_nano << " (" << status_buf.cycle
                          << ")\n\tExpected: ";
                std::copy(
                    (int*)buf.data( ),
                    (int*)end,
                    std::ostream_iterator< std::int32_t >( std::cout, " " ) );
                std::cout << "\n\tGot: ";
                std::copy(
                    (int*)cur,
                    (int*)( cur + size ),
                    std::ostream_iterator< std::int32_t >( std::cout, " " ) );
                std::cout << "\n";
                ++errors;
            }
            cur += size;
        } );
    std::cout << "Found " << errors << " errors\n";

    if ( errors > 0 )
    {
        std::cout << "\nChannels:\n";
        for ( const auto& name : online_list )
        {
            auto it = std::find_if(
                chan_list.begin( ),
                chan_list.end( ),
                [ &name ]( const daqd_stream::online_channel& chan ) -> bool {
                    return chan.name == name.c_str( );
                } );
            std::cout << "\t" << it->name.data( ) << " dcu: " << it->dcuid
                      << " doffset: " << it->data_offset
                      << " len: " << it->bytes_per_16th << "\n";
        }
        std::cout << "Plan:\n";
        plan.dump( std::cout );
    }
}

int
main( )
{
    std::cout << "Verify daqd_lite data...\n";
    daqd_stream::client daq_client( "daqd_stream" );

    auto chan_list = daq_client.channels( );
    auto dcu_mapping = create_dcu_map( chan_list );
    daq_client.dump_16th( "out.bin" );

    auto dblock = load_block( "out.bin" );

    verify_stats_t stats{ };
    std::for_each(
        dcu_mapping.begin( ),
        dcu_mapping.end( ),
        [ &stats, &dblock ](
            const std::pair< unsigned int,
                             std::vector< daqd_stream::online_channel > >&
                mapping ) {
            unsigned int dcu = mapping.first;
            auto&        chans = mapping.second;

            verify_dcu( dcu, chans, dblock, stats );
        } );
    std::cout << "Verify data block completed:\n\tok:\t" << stats.ok.size( )
              << "\n\tskipped:\t" << stats.skipped.size( ) << "\n\terror:\t"
              << stats.error.size( ) << std::endl;

    std::cout << "\n";

    verify_online( daq_client, chan_list );

    return 0;
}