/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include <daqd_stream/daqd_stream_server.hh>
#include <daqd_stream/daqd_stream.hh>
#include <daqd_stream/plugins/sub_delay_filter_plugin.hh>
#include <daqd_stream/plugins/sub_dump_filter_plugin.hh>
#include <deque>
#include <csignal>
#include <cstdio>
#include <functional>
#include <iostream>
#include <iomanip>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

struct Config
{
    std::string input_name{
        "tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9010"
    };
    std::string                shmem_name{ "daqd_stream" };
    std::string                config_name{ "" };
    int                        lookback_sec{ 5 };
    int                        shmem_size_mb{ 1024 + 800 };
    int                        permissions{ 0640 };
    std::vector< std::string > addition_plugins{ };
};

struct ApplicationConfig
{
    std::string                                   name{ };
    std::vector< daqd_stream::source_parameters > sources{ };
};

std::unique_ptr< daqd_stream::daqd_stream > daq( nullptr );

class argument_error : public std::runtime_error
{
public:
    explicit argument_error( const char* msg ) : std::runtime_error( msg )
    {
    }
};

ApplicationConfig
load_config( const std::string& fname )
{
    ApplicationConfig cfg;

    char                      buffer[ 1024 ];
    FILE*                     fp = fopen( fname.c_str( ), "rt" );
    rapidjson::FileReadStream input( fp, buffer, sizeof( buffer ) );
    rapidjson::Document       d;
    d.ParseStream( input );
    fclose( fp );

    cfg.name = d[ "name" ].GetString( );
    auto& sources = d[ "sources" ];
    for ( int i = 0; i < sources.Size( ); ++i )
    {
        auto&       cur = sources[ i ];
        std::string source = cur[ "source" ].GetString( );
        std::string label = cur[ "label" ].GetString( );

        auto&        params_doc = cur[ "parameters" ];
        std::int64_t size =
            static_cast< std::int64_t >( params_doc[ "size_mb" ].GetInt( ) ) *
            1024 * 1024;
        int         seconds = params_doc[ "seconds" ].GetInt( );
        std::string perms_source = params_doc[ "permissions" ].GetString( );
        int         perms = std::stoi( perms_source, nullptr, 8 );
        if ( perms <= 0 )
        {
            throw std::runtime_error(
                "invalid permissions setting in config file" );
        }

        daqd_stream::buffer_parameters params( seconds, size, perms );

        cfg.sources.emplace_back( source, label, params );
    }
    return cfg;
}

void
intHandler( int )
{
    if ( daq )
    {
        daq->stop( );
    }
}

std::vector< std::string >
split( const std::string& input, char sep )
{
    std::vector< std::string > out;

    std::string::size_type prev_pos = 0;
    std::string::size_type cur_pos = input.find( sep );
    while ( cur_pos != std::string::npos )
    {
        out.emplace_back( input.substr( prev_pos, cur_pos - prev_pos ) );
        prev_pos = cur_pos + 1;
    }
    out.emplace_back( input.substr( prev_pos ) );
    return out;
}

void
show_help( const char* error_msg )
{
    if ( error_msg )
    {
        std::cerr << error_msg << "\n";
    }
    std::cout
        << "Options:\n"
        << "--help,-h\t\t\tThis help\n"
        << "--additional-plugins <names>\tAdditional (debugging) plugins to "
           "load\n"

        << "\nSingle source specific options:\n"
        << "--data-source,-d <source>\tThe name of the input source "
           "[tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9010]\n"
        << "--permissions <perms>\t\tThe unix permissions for access [0660]\n"
        << "--lookback,-l <secs>\t\tThe length of the lookback in s [5]\n"
        << "--size,-s <mb>\t\t\tThe size of the shared buffer in mb [1824]\n"
        << "--name,-n <name>\t\tThe name of the shmem buffer [daqd_stream]\n"
        << "\nMulti source specific options:\n"
        << "--config,-c <filename>\tThe json source config file when doing a "
           "multi source input.\n"
        << "\n"
        << "Multi source inputs require configuration via json file.\n"
        << "The use of a config file overrides all other config directives.\n"
        << "The source config file is a json file structured as follows:\n"
        << "{\n"
        << "\t\"name\": \"string name of the main shared memory header\",\n"
        << "\t\"sources\": [\n"
        << "\t\t{\n"
        << "\t\t\t\"source\": "
           "\"tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9010\",\n"
        << "\t\t\t\"label\": \"main_data\",\n"
        << "\t\t\t\"parameters\": {\n"
        << "\t\t\t\t\"size_mb\": 1024,\n"
        << "\t\t\t\t\"seconds\": 15,\n"
        << "\t\t\t\t\"permissions\": \"0640\"\n"
        << "\t\t\t}\n"
        << "\t\t}\n"
        << "\t]\n"
        << "}\n";
}

Config
parse_arguments( int argc, char* argv[] )
{
    Config                    cfg;
    std::deque< std::string > args;
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }

    while ( !args.empty( ) )
    {
        std::string arg = args.front( );
        args.pop_front( );

        if ( arg == "--config" || arg == "-c" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--config|-c requires an argument" );
            }
            cfg.config_name = args.front( );
            args.pop_front( );
        }
        else if ( arg == "--data-source" || arg == "-d" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--data-source/-d requires an argument" );
            }
            cfg.input_name = args.front( );
            args.pop_front( );
        }
        else if ( arg == "--permissions" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--permissions an argument" );
            }
            std::stringstream input( args.front( ) );
            input >> std::setbase( 8 ) >> cfg.permissions;
            args.pop_front( );
        }
        else if ( arg == "--lookback" || arg == "-l" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--lookback requires an argument" );
            }
            std::stringstream input( args.front( ) );
            input >> cfg.lookback_sec;
            args.pop_front( );
        }
        else if ( arg == "--size" || arg == "-s" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--size requires an argument" );
            }
            std::stringstream input( args.front( ) );
            input >> cfg.shmem_size_mb;
            args.pop_front( );
        }
        else if ( arg == "--name" || arg == "-n" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--name|-n requires an argument" );
            }
            cfg.shmem_name = args.front( );
            args.pop_front( );
        }
        else if ( arg == "--additional-plugins" )
        {
            if ( args.empty( ) )
            {
                throw argument_error(
                    "--additional-plugins requires an argument" );
            }
            cfg.addition_plugins = split( args.front( ), ',' );
            args.pop_front( );
        }
        else if ( arg == "--help" || arg == "-h" )
        {
            throw argument_error( "" );
        }
        else
        {
            throw argument_error( "unexpected argument" );
        }
    }
    return cfg;
}

int
main( int argc, char* argv[] )
{
    Config cfg;
    try
    {
        cfg = parse_arguments( argc, argv );
    }
    catch ( argument_error& err )
    {
        show_help( err.what( ) );
        std::exit( 1 );
    }

    daqd_stream::daqd_stream::plugin_list additional_plugins{ };
    additional_plugins.emplace_back( plugins::get_dump_filter_sub_plugin() );
    for ( const auto& name : cfg.addition_plugins )
    {
        if ( name == "delay" )
        {
            additional_plugins.emplace_back(
                plugins::get_delay_filter_sub_plugin( ) );
        }
        else
        {
            std::cerr << "Unknown plugin requested '" << name << "'\n";
        }
    }

    if ( cfg.config_name.empty( ) )
    {
        daq = std::make_unique< daqd_stream::daqd_stream >(
            cfg.input_name,
            cfg.shmem_name,
            daqd_stream::buffer_parameters( cfg.lookback_sec,
                                            cfg.shmem_size_mb * 1024 * 1024,
                                            cfg.permissions ),
            additional_plugins );
    }
    else
    {
        auto app_config = load_config( cfg.config_name );
        daq = std::make_unique< daqd_stream::daqd_stream >(
            app_config.name, app_config.sources, additional_plugins );
    }

    std::signal( SIGINT, intHandler );

    // daqd_stream::client daq_client = daq->get_client( );

    daq->run( );

    return 0;
}
