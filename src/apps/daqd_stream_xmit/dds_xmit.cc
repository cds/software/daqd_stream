//
// Created by jonathan.hanks on 1/31/22.
//
#include <chrono>
#include <deque>
#include <iostream>
#include <string>
#include <thread>

#include <daqd_stream/plugins/sub_delay_filter_plugin.hh>
#include <daqd_stream/plugins/sub_frame_plugin.hh>
#include <daqd_stream/daqd_stream.hh>
#include <cds-pubsub/pub.hh>
#include <cds-pubsub/sub.hh>

struct Config
{
    std::string input_name{ "/dev/shm/frames" };
    std::string output_name{ "tcp://0.0.0.0:9000" };
};

void
show_help( const char* error_msg )
{
    if ( error_msg )
    {
        std::cerr << error_msg << "\n";
    }
    std::cout
        << "Options:\n"
        << "--help,-h\t\t\tThis help.\n"
        << "--input,-i\t\t\tThe plugin connection string for the input stream\n"
        << "--output,-o\t\t\tThe name plugin connection string for the output "
           "stream\n";
}

class argument_error : public std::runtime_error
{
public:
    explicit argument_error( const char* msg ) : std::runtime_error( msg )
    {
    }
};

Config
parse_arguments( int argc, char* argv[] )
{
    Config                    cfg;
    std::deque< std::string > args;
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }

    while ( !args.empty( ) )
    {
        std::string arg = args.front( );
        args.pop_front( );

        if ( arg == "-i" || arg == "--input" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--input/-i requires an argument" );
            }
            cfg.input_name = args.front( );
            args.pop_front( );
        }
        else if ( arg == "-o" || arg == "--output" )
        {
            if ( args.empty( ) )
            {
                throw argument_error( "--output/-o requires an argument" );
            }
            cfg.output_name = args.front( );
            args.pop_front( );
        }
        else if ( arg == "-h" || arg == "--help" )
        {
            throw argument_error( "" );
        }
        else
        {
            throw argument_error( "Unexpected option" );
        }
    }
    return cfg;
}

int
main( int argc, char* argv[] )
{
    Config cfg;
    try
    {
        cfg = parse_arguments( argc, argv );
    }
    catch ( argument_error& err )
    {
        show_help( err.what( ) );
        std::exit( 1 );
    }
    pub_sub::Publisher output{ };
    output.add_destination( cfg.output_name );

    pub_sub::Subscriber input{ };
    input.load_plugin( plugins::get_delay_filter_sub_plugin( ) );
    input.load_plugin( daqd_stream::plugins::get_frame_sub_plugin( ) );

    auto sub_id = input.subscribe(
        cfg.input_name, [ &output ]( pub_sub::SubMessage sub_message ) {
            output.publish( sub_message.key( ), sub_message.message( ) );
        } );
    while ( true )
    {
        std::this_thread::sleep_for( std::chrono::seconds( 10 ) );
    }

    return 0;
}
