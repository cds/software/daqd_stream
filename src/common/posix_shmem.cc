//
// Created by jonathan.hanks on 2/11/21.
//
#include "posix_shmem.hh"

#include <cstring>
#include <iostream>
#include <stdexcept>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

namespace
{
    class Fd_closer
    {
    public:
        explicit Fd_closer( int fd ) : fd_{ fd }
        {
        }
        ~Fd_closer( )
        {
            if ( fd_ >= 0 )
            {
                ::close( fd_ );
            }
        }

        int
        get( ) const noexcept
        {
            return fd_;
        }

    private:
        int fd_;
    };

    std::string
    build_segment_name( const std::string& name )
    {
        return "/" + name;
    }
} // namespace

namespace daqd_stream
{
    namespace detail
    {
        void
        Posix_shared_memory::remove( const std::string& name )
        {
            auto segment_name = build_segment_name( name );
            ::shm_unlink( segment_name.c_str( ) );
        }

        Posix_shared_memory::Posix_shared_memory( const std::string& name,
                                                  std::size_t        size,
                                                  int                mode )
            : size_{ size }
        {
            auto      segment_name = build_segment_name( name );
            Fd_closer fd{ shm_open(
                segment_name.c_str( ), O_CREAT | O_RDWR, mode ) };
            if ( fd.get( ) < 0 )
            {
                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
            auto rc = ::ftruncate( fd.get( ), static_cast< off_t >( size ) );
            if ( rc != 0 )
            {

                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
            data_ = ::mmap( nullptr,
                            size,
                            PROT_READ | PROT_WRITE,
                            MAP_SHARED,
                            fd.get( ),
                            0 );
            if ( data_ == MAP_FAILED )
            {
                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
        }

        Posix_shared_memory::Posix_shared_memory( const std::string& name )
            : size_{ 0 }
        {
            auto      segment_name = build_segment_name( name );
            Fd_closer fd{ shm_open( segment_name.c_str( ), O_RDONLY, 0640 ) };
            if ( fd.get( ) < 0 )
            {
                std::cerr << "error on shm_open" << std::endl;
                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
            struct stat finfo;
            auto        rc = fstat( fd.get( ), &finfo );
            if ( rc != 0 )
            {
                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
            size_ = finfo.st_size;
            data_ =
                ::mmap( nullptr, size_, PROT_READ, MAP_SHARED, fd.get( ), 0 );
            if ( data_ == MAP_FAILED )
            {
                throw std::runtime_error(
                    std::string( std::strerror( errno ) ) );
            }
        }

        Posix_shared_memory::Posix_shared_memory(
            Posix_shared_memory&& other ) noexcept : data_{ other.data_ },
                                                     size_{ other.size( ) }
        {
            other.release( );
        }

        Posix_shared_memory::~Posix_shared_memory( )
        {
            if ( data_ != nullptr && data_ != MAP_FAILED )
            {
                std::cout << "Closing shmem at " << data_ << " of " << size_
                          << " bytes\n";
                ::munmap( data_, size_ );
            }
        }

        Posix_shared_memory&
        Posix_shared_memory::operator=( Posix_shared_memory&& other ) noexcept
        {
            if ( data_ != nullptr && data_ != MAP_FAILED )
            {
                ::munmap( data_, size_ );
            }
            data_ = other.data_;
            size_ = other.size_;
            other.release( );
            return *this;
        }

        void
        Posix_shared_memory::release( )
        {
            data_ = MAP_FAILED;
            size_ = 0;
        }
    } // namespace detail
} // namespace daqd_stream