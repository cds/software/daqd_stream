//
// Created by jonathan.hanks on 2/11/21.
//

#ifndef DAQD_STREAM_SHARED_TYPES_HH
#define DAQD_STREAM_SHARED_TYPES_HH

#include "offset_ptr.hh"

#include <iostream>

#include <type_traits>

namespace daqd_stream
{
    namespace detail
    {
        template < typename T >
        class shared_span
        {
        public:
            using pointer = T*;
            using iterator = pointer;
            using const_iterator = typename std::add_const< pointer >::type;
            using value_type = T;

            shared_span( ) noexcept : start_{ nullptr }, end_{ nullptr }
            {
            }
            shared_span( T* p, std::size_t count )
                : start_{ p }, end_{ p + count }
            {
            }
            shared_span( T* p1, T* p2 ) noexcept : start_{ p1 }, end_{ p2 }
            {
            }
            shared_span( const shared_span& other ) noexcept
                : start_{ other.start_.get( ) },
                  end_{ other.end_.get( ) }
            {
            }

            shared_span&
            operator=( const shared_span& other ) noexcept
            {
                start_ = other.start_.get( );
                end_ = other.end_.get( );
                return *this;
            }

            iterator
            begin( ) noexcept
            {
                auto tmp = start_.get( );
                return tmp;
            }

            const iterator
            begin( ) const noexcept
            {
                auto tmp = start_.get( );
                return tmp;
            }

            const_iterator
            cbegin( ) const noexcept
            {
                return start_.get( );
            }

            iterator
            end( ) noexcept
            {
                return end_.get( );
            }

            const_iterator
            end( ) const noexcept
            {
                return end_.get( );
            }

            const_iterator
            cend( ) const noexcept
            {
                return end_.get( );
            }

            std::size_t
            size( ) const
            {
                return end_.get( ) - start_.get( );
            }

        private:
            offset_ptr< T > start_;
            offset_ptr< T > end_;
        };

    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_SHARED_TYPES_HH
