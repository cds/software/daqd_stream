//
// Created by jonathan.hanks on 2/11/21.
//

#ifndef DAQD_STREAM_POSIX_SHMEM_HH
#define DAQD_STREAM_POSIX_SHMEM_HH

#include <string>

namespace daqd_stream
{
    namespace detail
    {
        class Posix_shared_memory
        {
        public:
            static void remove( const std::string& name );

            Posix_shared_memory( void* data, std::size_t size )
                : data_{ data }, size_{ size }
            {
            }

            Posix_shared_memory( const std::string& name,
                                 std::size_t        size,
                                 int                mode = 0640 );
            explicit Posix_shared_memory( const std::string& name );
            Posix_shared_memory( Posix_shared_memory&& other ) noexcept;
            ~Posix_shared_memory( );

            Posix_shared_memory&
            operator=( Posix_shared_memory&& other ) noexcept;

            void*
            get( ) noexcept
            {
                return data_;
            }

            template < typename T >
            T*
            get_as( ) noexcept
            {
                return reinterpret_cast< T* >( get( ) );
            }

            const void*
            get( ) const noexcept
            {
                return data_;
            }

            template < typename T >
            const T*
            get_as( ) const noexcept
            {
                return reinterpret_cast< const T* >( get( ) );
            }

            std::size_t
            size( ) const noexcept
            {
                return size_;
            }

        private:
            void release( );

            void*       data_{ nullptr };
            std::size_t size_;
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_POSIX_SHMEM_HH
