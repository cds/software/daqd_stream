//
// Created by jonathan.hanks on 2/3/21.
//

#ifndef DAQD_STREAM_TEST_SIGNALING_HH
#define DAQD_STREAM_TEST_SIGNALING_HH

#include <atomic>
#include <cstdint>
#include <climits>
#include <memory>
#include <mutex>
#include <string>

#include <functional>

#include <unistd.h>
#include <syscall.h>
#include <linux/futex.h>

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief the data structure needed in the shared memory block
         * in order to signal data arrival.
         */
        struct signaling_data_block
        {
            std::uint32_t futex_data_sec;
            std::uint32_t futex_data_16th;
        };

        /*!
         * @brief the logic to signal a 16th or sec update
         */
        class signaler_api
        {
        public:
            explicit signaler_api( signaling_data_block& data_block )
                : block_{ data_block }
            {
            }

            void
            signal_sec( )
            {
                do_signal( counts_sec++, &block_.futex_data_sec );
            }
            void
            signal_16th( )
            {
                do_signal( counts_16th++, &block_.futex_data_16th );
            }

        private:
            static void
            do_signal( std::uint32_t  lock_val,
                       std::uint32_t* raw_futex ) noexcept
            {
                auto futex = reinterpret_cast< std::atomic< std::uint32_t >* >(
                    raw_futex );
                *futex = lock_val;
                syscall( SYS_futex, futex, FUTEX_WAKE, INT_MAX );
            }
            signaling_data_block& block_;
            std::uint32_t         counts_sec{ 0 };
            std::uint32_t         counts_16th{ 0 };
        };

        class locking_signaler
        {
        public:
            explicit locking_signaler( signaler_api* api ) : api_{ api }
            {
            }
            locking_signaler( const locking_signaler& ) = delete;
            locking_signaler( locking_signaler&& ) = delete;
            locking_signaler& operator=( const locking_signaler& ) = delete;

            locking_signaler& operator=( locking_signaler&& ) = delete;
            void
            signal_sec( )
            {
                if ( api_ )
                {
                    std::lock_guard< std::mutex > l_{ m_sec_ };
                    api_->signal_sec( );
                }
            }
            void
            signal_16th( )
            {
                if ( api_ )
                {
                    std::lock_guard< std::mutex > l_{ m_16th_ };
                    api_->signal_16th( );
                }
            }

        private:
            signaler_api* api_;
            std::mutex    m_sec_{ };
            std::mutex    m_16th_{ };
        };

        /*!
         * @brief the logic to wait for a signal on a 16th or sec update
         */
        class waiting_state
        {
        public:
            class waiting_api
            {
                friend class waiting_state;
                waiting_api( waiting_state&        state,
                             signaling_data_block& data_block )
                    : block_{ data_block }, state_{ state }
                {
                }

            public:
                void
                wait_sec( std::function< bool( void ) > until )
                {
                    while ( !until( ) )
                    {
                        state_.prev_sec_val = do_wait( state_.prev_sec_val,
                                                       &block_.futex_data_sec );
                    }
                }

                void
                wait_16th( std::function< bool( void ) > until )
                {
                    while ( !until( ) )
                    {
                        state_.prev_16th_val = do_wait(
                            state_.prev_16th_val, &block_.futex_data_16th );
                    }
                }

            private:
                static std::uint32_t
                do_wait( std::uint32_t  prev_val,
                         std::uint32_t* raw_futex ) noexcept
                {
                    auto futex =
                        reinterpret_cast< std::atomic< std::uint32_t >* >(
                            raw_futex );
                    std::uint32_t cur_val = *futex;
                    while ( cur_val == prev_val )
                    {
                        auto rc = syscall(
                            SYS_futex, futex, FUTEX_WAIT, prev_val, nullptr );
                        if ( rc < 0 )
                        {
                            auto err = errno;
                        }
                        cur_val = *futex;
                    }
                    return cur_val;
                }

                signaling_data_block& block_;
                waiting_state&        state_;
            };

            waiting_api
            api( signaling_data_block& data_block )
            {
                return waiting_api( *this, data_block );
            }

        private:
            std::uint32_t prev_sec_val{ 0 };
            std::uint32_t prev_16th_val{ 0 };
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_TEST_SIGNALING_HH
