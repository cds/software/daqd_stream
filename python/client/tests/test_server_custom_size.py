import contextlib
import integration
import os
import os.path

import daqd_stream

integration.Executable(name="daqd_stream_srv", hints=['../../src/apps/daqd_stream_server'], description="daqd stream server")
integration.Executable(name="fe_simulated_streams",
                       hints=["/usr/bin", ],
                       description="data server")
integration.Executable(name="cps_xmit",
                       hints=["/usr/bin", ],
                       description="data xmit")
integration.Executable(name="cds_metadata_server", hints=["/usr/bin"], description="channel metadata server")

ini_dir = integration.state.temp_dir('inis')
master_file = os.path.join(ini_dir, 'master')

generator = integration.Process("fe_simulated_streams",
                                ["-b", "local_dc",
                                 "-m", "100",
                                 "-k", "100",
                                 "-R", "5",
                                 "-i", ini_dir,
                                 "-M", master_file,
                                 "-admin", "127.0.0.1:10000",
                                 "-s", "shm://",
                                 ])
xmit = integration.Process("cps_xmit",
                           ["-b", "shm://local_dc",
                            "-m", "100",
                            "-p", "tcp://127.0.0.1:9000",],)
metadata = integration.Process("cds_metadata_server",
                               ["-listen", "127.0.0.1:9100",
                                "-master", master_file, ])
server = integration.Process("daqd_stream_srv", [
    "-d", "tcp://127.0.0.1:9000|meta://ini://" + master_file,
    "-l", "10",
    "-s", "1724",
])


def fail():
    raise RuntimeError("Fail here")


def check_size():
    info = os.stat("/dev/shm/daqd_stream")
    print("segment size is {0}".format(info.st_size))
    assert(info.st_size == 1724*1024*1024)


def check_lookback():
    client = daqd_stream.client("daqd_stream")
    print("segment lookback is {0}".format(client.info().safe_lookback_sec))
    assert(client.info().safe_lookback_sec == 10-1)
    del client


@contextlib.contextmanager
def cleanup_shmem(segments=('local_dc', 'daqd_stream',)):
    yield
    for entry in segments:
        path = os.path.join('/dev/shm', entry)
        try:
            os.remove(path)
        except:
            pass


with cleanup_shmem():
    integration.Sequence(
        [
            # integration.state.preserve_files,
            generator.run,
            integration.wait_tcp_server(port=10000, timeout=5),
            xmit.run,
            integration.wait_tcp_server(port=9000, timeout=5),
            metadata.run,
            integration.wait_tcp_server(port=9100, timeout=5),
            server.run,
            integration.wait(2),
            check_size,
            server.ignore,
            metadata.ignore,
            xmit.ignore,
            generator.ignore,
         ]
    )
print("done!")
