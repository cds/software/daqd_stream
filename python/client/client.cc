/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <daqd_stream/daqd_stream.hh>

// include auto generated doc strings
#include "daqd_stream_doc_strings.inc"

namespace py = pybind11;

namespace
{
    struct signal_units
    {
        signal_units( double      signal_offset,
                      double      signal_slope,
                      std::string signal_units )
            : offset( signal_offset ), slope( signal_slope ),
              units( std::move( signal_units ) )
        {
        }

        double      offset;
        double      slope;
        std::string units;
    };

    std::string
    read_name( const daqd_stream::online_channel& chan )
    {
        return std::string( chan.name.data( ) );
    }

    void
    write_name( daqd_stream::online_channel& chan, const std::string& s )
    {
        chan.name = s.c_str( );
    }

    std::string
    read_units( const daqd_stream::online_channel& chan )
    {
        return std::string( chan.units.data( ) );
    }

    void
    write_units( daqd_stream::online_channel& chan, const std::string& s )
    {
        chan.units = s.c_str( );
    }

    std::string
    repr( const daqd_stream::online_channel& chan )
    {
        return "<" + read_name( chan ) + ">";
    }

    PyObject*
    online_channel_getitem( const daqd_stream::online_channel& channel,
                            const std::string                  index )
    {
        if ( index == "channel_type" )
        {
            return PyLong_FromLong( 1 );
        }
        else if ( index == "data_type" )
        {
            return PyLong_FromLong( static_cast< long >( channel.datatype ) );
        }
        else if ( index == "rate" )
        {
            return PyFloat_FromDouble( channel.datarate );
        }
        else if ( index == "units" )
        {
        }
        throw std::out_of_range( index );
    }

    std::vector< daqd_stream::output_metadata >
    plan_metadata( const daqd_stream::data_plan& plan )
    {
        return plan.metadata();
    }

    std::string
    read_units_loc( const daqd_stream::output_location& loc )
    {
        return std::string( loc.units.data( ) );
    }

    void
    write_units_loc( daqd_stream::output_location& loc, const std::string& s )
    {
        loc.units = s.c_str( );
    }

    daqd_stream::data_status
    get_16th_data(
        daqd_stream::client&          client,
        daqd_stream::data_plan&       plan,
        const daqd_stream::time_point last_time_seen,
        py::array_t< char, py::array::c_style | py::array::forcecast >& buffer,
        daqd_stream::byte_order output_byte_order )
    {
        if ( buffer.ndim( ) != 1 )
        {
            throw std::runtime_error( "Expecting a 1 dimensional array" );
        }

        auto                   buf_info = buffer.request( );
        auto                   status = daqd_stream::data_status{ };
        py::gil_scoped_release release;
        client.get_16th_data(
            plan,
            last_time_seen,
            const_cast< char* >(
                reinterpret_cast< const char* >( buf_info.ptr ) ),
            buf_info.size,
            status,
            output_byte_order );
        return status;
    }

    daqd_stream::sec_data_status
    get_sec_data(
        daqd_stream::client&    client,
        daqd_stream::data_plan& plan,
        std::int64_t            start_second,
        std::size_t             stride,
        py::array_t< char, py::array::c_style | py::array::forcecast >& buffer,
        daqd_stream::byte_order output_byte_order )
    {
        if ( buffer.ndim( ) != 1 )
        {
            throw std::runtime_error( "Expecting a 1 dimensional array" );
        }
        auto                   buf_info = buffer.request( );
        auto                   status = daqd_stream::sec_data_status{ };
        py::gil_scoped_release release;
        client.get_sec_data(
            plan,
            start_second,
            stride,
            const_cast< char* >(
                reinterpret_cast< const char* >( buf_info.ptr ) ),
            buf_info.size,
            status,
            output_byte_order );
        return status;
    }

    static const char*
    dtype_to_string( daqd_stream::DATA_TYPE data_type )
    {
        using daqd_stream::DATA_TYPE;

        switch ( data_type )
        {
        case DATA_TYPE::INT16:
            return "int_2";
        case DATA_TYPE::INT32:
            return "int_4";
        case DATA_TYPE::INT64:
            return "int_8";
        case DATA_TYPE::FLOAT32:
            return "real_4";
        case DATA_TYPE::FLOAT64:
            return "real_8";
        case DATA_TYPE::UINT32:
            return "uint_4";
        default:
            return "unknown";
        }
    }
    // clang-format off
    const char* doc_string_get_16th_data = R"docstring(Retrieve a 1/16s block of data
         parameter plan - The data plan object describing the request
         parameter last_time_seen - The last timepoint that was seen
         parameter buffer - The output buffer (an array of bytes)
         parameter  output_byte_order - requested byte ordering for the data [byte_order.NATIVE]

         This function will serve the next buffer following the given
         last_time_seen value.  For a new request the last_time_seen should be
         set to a zero time.  In typical use cases it does return the next
         value, however there may be gaps in the data if there was a disruption
         in the data stream, or it has been a significant amount of time since
         the last call to get_16th_data and the requested data has aged out
         of the buffers.

         This returns a data_status object that contains the gps time and status
         of the request.
)docstring";

    const char* doc_string_get_sec_data = R"docstring(Retrieve one or more 1s blocks of data
         plan - The data plan object describing the request
         start_second - The second to retrieve
         stride - the number of seconds of data to read (typically 1)
         buffer - the output buffer
         output_byte_order - requested byte ordering of the output data

         NOTE: This is very different from get_16th_data.  This function
         returns the requested time (or the latest if start_second is 0), NOT
         the next second of data.  Put the requested time in, not the previous
         time.

         This returns a data_status object that contains the gps time and status
         of the request.)docstring";
    // clang-format on
} // namespace

PYBIND11_MODULE( daqd_stream, m )
{
    m.doc( ) = "A lightweight daq data stream client";

    py::enum_< daqd_stream::PLAN_TYPE >(
        m, "PLAN_TYPE", docs::doc_string_PLAN_STATUS )
        .value( "PLAN_16TH", daqd_stream::PLAN_TYPE::PLAN_16TH )
        .value( "PLAN_SEC", daqd_stream::PLAN_TYPE::PLAN_SEC );

    py::enum_< daqd_stream::PLAN_STATUS >(
        m, "PLAN_STATUS", docs::doc_string_PLAN_STATUS )
        .value( "OK", daqd_stream::PLAN_STATUS::OK )
        .value( "UPDATE_PLAN", daqd_stream::PLAN_STATUS::UPDATE_PLAN );

    py::enum_< daqd_stream::DATA_TYPE >(
        m, "DATA_TYPE", docs::doc_string_DATA_TYPE )
        .value( "UNDEFINED", daqd_stream::DATA_TYPE::UNDEFINED )
        .value( "INT16", daqd_stream::DATA_TYPE::INT16 )
        .value( "INT32", daqd_stream::DATA_TYPE::INT32 )
        .value( "INT64", daqd_stream::DATA_TYPE::INT64 )
        .value( "FLOAT32", daqd_stream::DATA_TYPE::FLOAT32 )
        .value( "FLOAT64", daqd_stream::DATA_TYPE::FLOAT64 )
        .value( "COMPLEX32", daqd_stream::DATA_TYPE::COMPLEX32 )
        .value( "UINT32", daqd_stream::DATA_TYPE::UINT32 );

    py::class_< daqd_stream::time_point >(
        m, "time_point", docs::doc_string_time_point )
        .def( py::init( &daqd_stream::make_time_point ),
              py::arg( "seconds" ) = 0,
              py::arg( "nano" ) = 0 )
        .def_readwrite( "seconds", &daqd_stream::time_point::seconds )
        .def_readwrite( "nano", &daqd_stream::time_point::nano );

    py::class_< signal_units >( m, "signal_units" )
        .def( py::init< double, double, std::string >( ) )
        .def_readwrite( "offset", &signal_units::offset )
        .def_readwrite( "slope", &signal_units::slope )
        .def_readwrite( "units", &signal_units::units );

    py::class_< daqd_stream::online_channel >(
        m, "online_channel", docs::doc_string_online_channel )
        .def( py::init<>( ) )
        .def_property( "name", read_name, write_name )
        .def_readwrite( "dcuid", &daqd_stream::online_channel::dcuid )
        .def_readwrite( "datatype", &daqd_stream::online_channel::datatype )
        .def_readwrite( "datarate", &daqd_stream::online_channel::datarate )
        .def_readwrite( "bytes_per_16th",
                        &daqd_stream::online_channel::bytes_per_16th )
        .def_readwrite( "data_offset",
                        &daqd_stream::online_channel::data_offset )
        .def_readwrite( "signal_gain",
                        &daqd_stream::online_channel::signal_gain )
        .def_readwrite( "signal_slope",
                        &daqd_stream::online_channel::signal_slope )
        .def_readwrite( "signal_offset",
                        &daqd_stream::online_channel::signal_offset )
        .def_property( "units", read_units, write_units )
        .def( "__repr__", repr )
        .def( "__getitem__",
              []( const daqd_stream::online_channel& channel,
                  const std::string                  index ) -> py::object {
                  if ( index == "channel_type" )
                  {
                      return py::cast( "online" );
                  }
                  else if ( index == "data_type" )
                  {
                      return py::cast( dtype_to_string( channel.datatype ) );
                  }
                  else if ( index == "rate" )
                  {
                      return py::cast( channel.datarate );
                  }
                  else if ( index == "units" )
                  {
                      signal_units units( channel.signal_offset,
                                          channel.signal_slope,
                                          channel.units.data( ) );
                      return py::cast( units );
                  }
                  throw std::out_of_range( index );
              } );

    py::class_< daqd_stream::output_location >(
        m, "output_location", docs::doc_string_output_location )
        .def( py::init<>( ) )
        .def_readwrite( "datatype", &daqd_stream::output_location::datatype )
        .def_readwrite( "datarate", &daqd_stream::output_location::datarate )
        .def_readwrite( "bytes_per_16th",
                        &daqd_stream::output_location::bytes_per_16th )
        .def_readwrite( "data_offset",
                        &daqd_stream::output_location::data_offset )
        .def_readwrite( "signal_gain",
                        &daqd_stream::output_location::signal_gain )
        .def_readwrite( "signal_slope",
                        &daqd_stream::output_location::signal_slope )
        .def_readwrite( "signal_offset",
                        &daqd_stream::output_location::signal_offset )
        .def_property( "units", read_units_loc, write_units_loc );

    py::class_< daqd_stream::output_metadata >(
        m, "output_metadata", docs::doc_string_output_metadata )
        .def( py::init<>( ) )
        .def_readwrite( "name", &daqd_stream::output_metadata::name )
        .def_readwrite( "location", &daqd_stream::output_metadata::location );

    py::class_< daqd_stream::data_plan >(
        m, "data_plan", docs::doc_string_data_plan )
        .def( "required_size",
              &daqd_stream::data_plan::required_size,
              docs::doc_string_data_plan__required_size )
        .def( "metadata",
              &plan_metadata,
              docs::doc_string_data_plan__metadata )
        .def_property_readonly( "type",
                                &daqd_stream::data_plan::type,
                                docs::doc_string_data_plan__type )
        .def( "multi_source",
              &daqd_stream::data_plan::multi_source,
              docs::doc_string_data_plan__multi_source );

    py::class_< daqd_stream::data_status >( m, "data_status" )
        .def_readwrite( "gps", &daqd_stream::data_status::gps )
        .def_readwrite( "cycle", &daqd_stream::data_status::cycle )
        .def_readwrite( "plan_status", &daqd_stream::data_status::plan_status )
        .def_readwrite( "channel_status",
                        &daqd_stream::data_status::channel_status );

    py::class_< daqd_stream::sec_data_status >( m, "sec_data_status" )
        .def_readwrite( "gps", &daqd_stream::sec_data_status::gps )
        .def_readwrite( "cycle", &daqd_stream::sec_data_status::cycle )
        .def_readwrite( "plan_status",
                        &daqd_stream::sec_data_status::plan_status )
        .def_readwrite( "channel_status",
                        &daqd_stream::sec_data_status::channel_status );

    py::enum_< daqd_stream::byte_order >( m, "byte_order" )
        .value( "NATIVE", daqd_stream::byte_order::NATIVE )
        .value( "LITTLE", daqd_stream::byte_order::LITTLE )
        .value( "BIG", daqd_stream::byte_order::BIG );

    //    py::class_< daqd_stream::client::info_block >( m, "info_block" )
    //        .def_readonly( "safe_lookback_sec",
    //                       &daqd_stream::client::info_block::safe_lookback_sec
    //                       );

    py::class_< daqd_stream::client >( m, "client", docs::doc_string_client )
        .def( py::init( &daqd_stream::client::create ),
              docs::doc_string_client__client )
        .def( "plan_request",
              &daqd_stream::client::plan_request,
              docs::doc_string_client__plan_request )
        .def( "config_checksum",
              &daqd_stream::client::config_checksum,
              docs::doc_string_client__config_checksum )
        .def( "channels",
              &daqd_stream::client::channels,
              docs::doc_string_client__channels )
        .def( "update_plan",
              &daqd_stream::client::update_plan,
              docs::doc_string_client__update_plan )
        .def( "get_16th_data",
              &get_16th_data,
              py::arg( "plan" ),
              py::arg( "last_time_seen" ),
              py::arg( "buffer" ),
              py::arg( "output_byte_order" ) = daqd_stream::byte_order::NATIVE,
              doc_string_get_16th_data )
        .def( "get_sec_data",
              &get_sec_data,
              py::arg( "plan" ),
              py::arg( "start_second" ),
              py::arg( "stride" ),
              py::arg( "buffer" ),
              py::arg( "output_byte_order" ) = daqd_stream::byte_order::NATIVE,
              doc_string_get_sec_data );
}