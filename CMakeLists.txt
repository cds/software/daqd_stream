cmake_minimum_required(VERSION 3.13)
project(daqd_stream)

set(CMAKE_CXX_STANDARD 14)
SET( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/config" )

set_property(GLOBAL PROPERTY FIND_LIBRARY_USE_LIB64_PATHS TRUE)

include(Integration)
include(GNUInstallDirs)

find_package(pybind11 REQUIRED)
find_package(CURL)
find_package(RapidJSON)
find_package(libcds-pubsub)

find_package(Python3 COMPONENTS Interpreter Development)

set (PYTHON_EXECUTABLE "${Python3_EXECUTABLE}")

find_package(Boost COMPONENTS system filesystem)
find_package(Threads)
find_package(nds2-client)
find_package(FrameCPP)
find_package(advLigoRTS)

enable_testing()

#add_compile_options("-fsanitize=thread")
#add_link_options("-fsanitize=thread")
#add_compile_options("-fsanitize=address")
#add_link_options("-fsanitize=address")

add_subdirectory(external)
add_subdirectory(src)
add_subdirectory(support)
add_subdirectory(python)

# https://stackoverflow.com/questions/5378528/install-export-problem-for-shared-lib-with-dependencies
install( EXPORT daqd_stream_install
        DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/daqd_stream
        NAMESPACE daqd_stream::
        FILE daqd_streamConfig.cmake
        )