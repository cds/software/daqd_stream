# Daq_lite

Testing out a method to allow external processes to consume a CDS data stream.

The idea is to have a single process which consumes the CDS data stream + metadata and exports a combiled meta-data data buffer set via shared memory.

Library interfaces are provided to consume this, doing channel listing and data access.

The goal is to provide a simple interface such that a process does not need to know about the CDS data layout, only
the channels that are requested.  The application would only need to know the channels, and then wait for
signals for data being delivered.